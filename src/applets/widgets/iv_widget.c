/* Applet ImageView Widget
 *
 * Note: Is a "stripped down" ImageView Widget based on :
 *	 ImageV.c v 1.1 1995/02/22 05:24:26 cwikla
 *
 * Original Notes:
 *	Someone on the net said they wanted to see a widget that could scale
 *	an image.  I threw this together in about 3 hours.  You need
 *	to either the XtNpixmap or XtNximage resource and the XtNimageType
 *	resource.  XtNsaveMemory means not to use an intermediate pixmap for
 *	blitting operations.  That's it...
 *
 *	John L. Cwikla
 *	cwikla@wri.com
 */
#include <ctype.h>

#include <X11/IntrinsicP.h>
#include <X11/StringDefs.h>

#include "iv_widget.h"

struct IVPicture {
	unsigned char type;
	Pixmap pixmap;
	XImage *ximage;
};

/* Image View part of the widget */
struct iv_part {
	GC gc;
	struct IVPicture picture;		/* Picture */
	struct IVPicture internalPicture;	/* Internal Picture */
	Boolean		 saveMemory;
	Boolean		 internalMemorySave;
	unsigned int	 imageWidth;
	unsigned int	 imageHeight;
	double		 wScale;
	double		 hScale;
};

/* Image View Widget */
struct ivw {
	CorePart	core;
	struct iv_part	iv;
};

struct ImageViewClassPart {
	int empty;
};

/* Image View Class Record */
struct ImageViewClassRec {
	CoreClassPart core_class;
	struct ImageViewClassPart image_view_class;
};

extern struct ImageViewClassRec image_viewClassRec;

/* For widget internals */
static void initialize(struct ivw *request, struct ivw *new);
static void realize(struct ivw *w, XtValueMask *xvm, XSetWindowAttributes *xswa);
static void resize(struct ivw *w);
static void destroy(struct ivw *w);
static void redisplay(struct ivw *w, XEvent *e, Region r);
static Boolean setValues(struct ivw *_current, struct ivw *request, struct ivw *new);
static Boolean cvtStringToImageType(Display *display, XrmValuePtr args, Cardinal *numArgs, XrmValuePtr from, XrmValuePtr to, XtPointer *data);

static XtResource imageViewResources[] = {
	{
		XtNsaveMemory, XtCSaveMemory, XtRBoolean, sizeof(Boolean),
		XtOffset(struct ivw *, iv.saveMemory),
		XtRImmediate, (XtPointer) FALSE
	},
	{
		XtNximage, XtCXImage, XtRPointer, sizeof(XImage *),
		XtOffset(struct ivw *, iv.picture.ximage),
		XtRImmediate, (XtPointer) NULL
	},
	{
		XtNpixmap, XtCPixmap, XtRPixmap, sizeof(Pixmap),
		XtOffset(struct ivw *, iv.picture.pixmap),
		XtRImmediate, (XtPointer) 0
	},
	{
		XtNimageType, XtCImageType, XtRImageType, sizeof(unsigned char),
		XtOffset(struct ivw *, iv.picture.type),
		XtRImmediate, (XtPointer) USE_UNSET
	},
};

struct ImageViewClassRec iv_class_rec = {
	{	/* CoreClassPart */
		(WidgetClass) &widgetClassRec,	/* superclass */
		"ImageView",			/* class_name */
		sizeof(struct ivw),		/* widget_size */
		NULL,				/* class_initialize */
		NULL,				/* class_part_initialize */
		FALSE,				/* class_init */
		(XtInitProc)initialize,		/* initialize */
		NULL,				/* initialize_hook */
		(XtRealizeProc)realize,		/* realize */
		NULL,				/* imageViewActions, */		 /* actions */
		0,				/* XtNumber(imageViewActions), */	 /* num_actions */
		imageViewResources,		/* resources */
		XtNumber(imageViewResources),	/* num_resources */
		NULLQUARK,			/* xrm_class */
		TRUE,				/* compress_motion */
		XtExposeCompressMultiple | XtExposeGraphicsExposeMerged, /* compress_exposure */
		TRUE,				/* compress_enterleave */
		TRUE,				/* visible_intress */
		(XtWidgetProc) destroy,		/* destroy */
		(XtWidgetProc) resize,		/* resize */
		(XtExposeProc) redisplay,	/* expose */
		(XtSetValuesFunc) setValues,	/* set_values */
		NULL,				/* set_values_hook */
		XtInheritSetValuesAlmost,	/* set_values_almost */
		NULL,				/* get_values_hook */
		NULL,				/* accept_focus */
		XtVersion,			/* version */
		NULL,				/* callback_private */
		XtInheritTranslations,		/* tm_translations */
		NULL,
		NULL,
		NULL,
	},
	{
		0,	/* empty */
	}
};

WidgetClass imageViewWidgetClass = (WidgetClass) &iv_class_rec;

/* This function may be unused */
static void classInitialize(void) __attribute__((unused));
static void classInitialize(void)
{
	XtSetTypeConverter(XtRString, XtRImageType, cvtStringToImageType, NULL, 0, XtCacheAll, NULL);
}

static void initialize(struct ivw *request, struct ivw *new)
{
	Cardinal numParams = 0;

	new->iv.internalPicture.pixmap = 0;
	new->iv.internalPicture.type = USE_PIXMAP;

	new->iv.wScale = 0.0;
	new->iv.hScale = 0.0;

	if (new->iv.picture.type == USE_UNSET) {
		XtAppErrorMsg(XtWidgetToApplicationContext((Widget) new),
			XtName((Widget) new), "ximage", "NoneSpecified",
			"XtNimageType must be set to USE_PIXMAP or USE_XIMAGE", NULL, &numParams);
	}

	if (new->iv.picture.type == USE_XIMAGE) {
		if (new->iv.picture.ximage == NULL) {
			XtAppErrorMsg(XtWidgetToApplicationContext((Widget) new),
				XtName((Widget) new), "ximage", "NoneSpecified",
				"XtNximage must be non-NULL", NULL, &numParams);
		} else {
			new->iv.imageWidth  = new->iv.picture.ximage->width;
			new->iv.imageHeight = new->iv.picture.ximage->height;
		}
	} else {
		if (new->iv.picture.pixmap == 0) {
			XtAppErrorMsg(XtWidgetToApplicationContext((Widget) new),
				XtName((Widget) new), "pixmap", "NoneSpecified",
				"XtNpixmap must be non-zero", NULL, &numParams);
		} else {
			Window root;
			int x, y;
			unsigned int borderWidth, depth;
			XGetGeometry(XtDisplay(new), new->iv.picture.pixmap, &root, &x, &y,
					&new->iv.imageWidth,
					&new->iv.imageHeight,
					&borderWidth, &depth);
		}
	}

	if (request->core.width == 0)
		new->core.width = new->iv.imageWidth;

	if (request->core.height == 0)
		new->core.height = new->iv.imageHeight;

	new->iv.gc = XtGetGC((Widget) new, 0, NULL);
}

/*
** NOT THREAD SAFE!!!!!
*/
static Boolean PixmapAllocError = False;

static int pixmapErrorHandler(__attribute__((unused)) Display *d,
			      __attribute__((unused)) XErrorEvent *e)
{
	PixmapAllocError = True;
	return 0;
}

Pixmap _createPixmap(Display *dpy, Drawable drawable, unsigned int width, unsigned int height, unsigned int depth)
{
	Pixmap pix;

	Drawable root;

	int x;
	int y;

	unsigned int rw;
	unsigned int rh;
	unsigned int bw;
	unsigned int rd;

	XErrorHandler oldHandler = NULL;

	PixmapAllocError = False;

	/* Install our error handler */
	oldHandler = XSetErrorHandler(pixmapErrorHandler);

	/* Attempt to create pixmap */
	pix = XCreatePixmap(dpy, drawable, width, height, depth);
	XGetGeometry(dpy, pix, &root, &x, &y, &rw, &rh, &bw, &rd);

	/* Wait to see if server allocated pixmap */
	XSync(dpy, False);

	/* Restore standard error handler */
	XSetErrorHandler(oldHandler);

	return	PixmapAllocError ? (Pixmap) NULL : pix;
}

static void check_window_size(struct ivw *w)
{
	unsigned int height;

	w->iv.wScale = (double) w->iv.imageWidth  / (double) w->core.width;
	w->iv.hScale = (double) w->iv.imageHeight / (double) w->core.height;

	if (w->iv.internalPicture.pixmap != 0)
		XFreePixmap(XtDisplay(w), w->iv.internalPicture.pixmap);

	w->iv.internalPicture.pixmap = 0;

	if (!w->iv.saveMemory &&
	   (w->iv.imageWidth != w->core.width) &&
	   (w->iv.imageHeight != w->core.height)) {
		height = (w->iv.imageHeight > w->core.height) ? w->iv.imageHeight : w->core.height;
		w->iv.internalPicture.type = USE_PIXMAP;
		w->iv.internalPicture.pixmap = _createPixmap(XtDisplay(w), XtWindow(w), w->core.width, height, w->core.depth);
	}

	if (w->iv.internalPicture.pixmap == 0)
		w->iv.internalMemorySave = TRUE;
	else
		w->iv.internalMemorySave = FALSE;
}

static void realize(struct ivw *w, XtValueMask *xvm, XSetWindowAttributes *xswa)
{
	/* Call the Realize procedure (XtInheritRealize) */
	(*imageViewWidgetClass->core_class.superclass->core_class.realize)((Widget) w, xvm, xswa);

	check_window_size(w);
}

static void resize(struct ivw *w)
{
	if (XtIsRealized((Widget) w)) {
		check_window_size(w);
		XClearArea(XtDisplay(w), XtWindow(w), 0, 0, w->core.width, w->core.height, FALSE);
	}
}

static void destroy(struct ivw *w)
{
	if (w->iv.internalPicture.pixmap != 0)
		XFreePixmap(XtDisplay(w), w->iv.internalPicture.pixmap);

	XtReleaseGC((Widget) w, w->iv.gc);
}

#if 0
void PCOPY(Display *dpy, struct IVPicture pic, Drawable d, GC gc,
	   int srcX, int srcY, int destX, int destY,
	   unsigned int width, unsigned int height)
{
	if (pic.type == USE_XIMAGE)
		XPutImage(dpy, d, gc, pic.ximage, srcX, srcY, destX, destY, width, height);
	else
		XCopyArea(dpy, pic.pixmap, d, gc, srcX, srcY, width, height, destX, destY);
}
#else
#define PCOPY(dpy, pic, draw, _gc, srcX, srcY, destX, destY, width, height)		\
	do {										\
		if ((pic).type == USE_XIMAGE)						\
			XPutImage(dpy, draw, _gc, (pic).ximage,				\
				  (int) (srcX), (int) (srcY),				\
				  (int) (destX), (int) (destY),				\
				  (unsigned int)(width), (unsigned int)(height));	\
		else									\
			XCopyArea(dpy, (pic).pixmap, draw, _gc,				\
				  (int) (srcX), (int) (srcY),				\
				  (unsigned int)(width), (unsigned int)(height),	\
				  (int)(destX), (int)(destY));				\
	} while (0)
#endif

static void draw_image(struct ivw *w)
{
	unsigned int i;
	unsigned int j;

	Dimension width;
	Dimension height;

	Position x;
	Position y;

	width  = w->core.width;
	height = w->core.height;

	x = 0;
	y = 0;

	if (!width || !height)
		return;

	if ((width == w->iv.imageWidth) && (height == w->iv.imageHeight)) {
		PCOPY(XtDisplay(w), w->iv.picture, XtWindow(w), w->iv.gc,
			0, 0, x, y, w->iv.imageWidth, w->iv.imageHeight);
		return;
	}

	if (width == w->iv.imageWidth) {
		for (j=0; j < height; j++) {
			PCOPY(XtDisplay(w), w->iv.picture, XtWindow(w), w->iv.gc,
				0, (int)(j * w->iv.hScale), x, j+y, width, 1);
		}
		return;
	}

	if (height == w->iv.imageHeight) {
		for (i=0; i < width; i++) {
			PCOPY(XtDisplay(w), w->iv.picture, XtWindow(w), w->iv.gc,
				(int)(i * w->iv.wScale), 0, i+x, y, 1, height);
		}
		return;
	}

	if (!w->iv.internalMemorySave) {
		for (i=0; i < width; i++)
			PCOPY(XtDisplay(w), w->iv.picture, w->iv.internalPicture.pixmap, w->iv.gc,
				(int)(i * w->iv.wScale), 0, i, 0, 1, w->iv.imageHeight);

		height = (w->iv.imageHeight > height) ? w->iv.imageHeight : height;
		for (j = 0; j < height; j++)
			PCOPY(XtDisplay(w), w->iv.internalPicture, XtWindow(w), w->iv.gc,
				0, (int)(j * w->iv.hScale), x, j+y, width, 1);
	} else {
		for (j = 0; j < height; j++)
			for (i = 0; i < width; i++) {
				PCOPY(XtDisplay(w), w->iv.picture, XtWindow(w), w->iv.gc,
					(int)(i * w->iv.wScale), (int)(j * w->iv.hScale), i+x, j+y, 1, 1);
			}
	}
}

static void redisplay(struct ivw *w,
		      __attribute__((unused)) XEvent *e,
		      __attribute__((unused)) Region r)
{
	draw_image(w);
}

static Boolean setValues(__attribute__((unused)) struct ivw *_current,
			 __attribute__((unused)) struct ivw *request,
			 struct ivw *new)
{
	check_window_size(new);
	return TRUE;
}

static void Lower(char *st1, char *st2, int length)
{
	int i;
	char *ptr;

	for (ptr = st1, i = 0; (ptr != NULL) && (i < length); ptr++, i++)
		*(st2 + i) = tolower(*ptr);
}

static Boolean cvtStringToImageType(Display *display,
				    __attribute__((unused)) XrmValuePtr args,
				    Cardinal *numArgs,
				    XrmValuePtr from,
				    XrmValuePtr to,
				    __attribute__((unused)) XtPointer *data)
{
	char *lower;
	static unsigned char imageType;
	Boolean badConversion = FALSE;

	if (*numArgs != 0) {
		XtErrorMsg("cvtStringToImageType", "wrongParamaters",
		"ImageView",
		"cvtStringToImageType needs no arguments.",
		(String *) NULL, (Cardinal *) NULL);
	}

	lower = XtNewString(from->addr);
	Lower(from->addr, lower, strlen(from->addr));

	imageType = USE_UNSET;
	if (!strcmp(lower, "use_ximage"))
		imageType = USE_XIMAGE;
	else if (!strcmp(lower, "ximage"))
		imageType = USE_XIMAGE;
	else if (!strcmp(lower, "use_pixmap"))
		imageType = USE_PIXMAP;
	else if (!strcmp(lower, "pixmap"))
		imageType = USE_PIXMAP;
	else
		badConversion = TRUE;

	XtFree(lower);

	if (badConversion) {
		XtDisplayStringConversionWarning(display, from->addr, XtRImageType);
	} else {
		if (to->addr == NULL)
			to->addr = (caddr_t) &imageType;
		else
		if (to->size < sizeof(unsigned char))
			badConversion = TRUE;
		else
			*(unsigned char *) to->addr = imageType;
		to->size = sizeof(unsigned char);
	}

	return !badConversion;
}
