#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdint.h>

#include <X11/Xlib.h>
#include <X11/keysym.h>

#include <jpeglib.h>
#include <jerror.h>

static void jpeg_error_exit(j_common_ptr cinfo)
{
	cinfo->err->output_message (cinfo);
	exit(EXIT_FAILURE);
}

/* This returns an array for a 24 bit image.*/
static uint8_t *decode_jpeg_file(const char *filename, int *out_width, int *out_height)
{
	FILE *fp;

	register JSAMPARRAY lineBuf;

	struct jpeg_decompress_struct cinfo;
	struct jpeg_error_mgr err_mgr;

	int bpp;	/* byte per pixel */

	int x;
	unsigned int y;

	int lineOffset;

	uint8_t *buf;

	fp = fopen(filename, "rb");
	if (NULL == fp) {
		perror(NULL);
		return NULL;
	}

	cinfo.err = jpeg_std_error(&err_mgr);
	err_mgr.error_exit = jpeg_error_exit;

	jpeg_create_decompress(&cinfo);
	jpeg_stdio_src(&cinfo, fp);
	jpeg_read_header(&cinfo, 1);

	cinfo.do_fancy_upsampling = 0;
	cinfo.do_block_smoothing = 0;

	jpeg_start_decompress(&cinfo);

	*out_width = cinfo.output_width;
	*out_height = cinfo.output_height;

	bpp = cinfo.output_components;

	lineBuf = cinfo.mem->alloc_sarray((j_common_ptr) &cinfo, JPOOL_IMAGE, (*out_width * bpp), 1);

	buf = malloc(3 * (*out_width * *out_height));

	if (NULL == buf) {
		perror (NULL);
		return NULL;
	}

	if (3 == bpp) {
		lineOffset = (*out_width * 3);

		for (y = 0; y < cinfo.output_height; ++y) {
			jpeg_read_scanlines (&cinfo, lineBuf, 1);

			for (x = 0; x < lineOffset; ++x) {
				buf[(lineOffset * y) + x] = lineBuf[0][x];
				++x;
				buf[(lineOffset * y) + x] = lineBuf[0][x];
				++x;
				buf[(lineOffset * y) + x] = lineBuf[0][x];
			}
		}
	} else if (1 == bpp) {
		unsigned int col;
		int lineBufIndex;

		lineOffset = (*out_width * 3);

		for (y = 0; y < cinfo.output_height; ++y) {
			jpeg_read_scanlines(&cinfo, lineBuf, 1);

			lineBufIndex = 0;
			for (x = 0; x < lineOffset; ++x) {
				col = lineBuf[0][lineBufIndex];

				buf[(lineOffset * y) + x] = col;
				++x;
				buf[(lineOffset * y) + x] = col;
				++x;
				buf[(lineOffset * y) + x] = col;

				++lineBufIndex;
			}
		}
	} else {
		fprintf(stderr, "Error: the number of color channels is %d."
				"This program only handles 1 or 3\n", bpp);
		return NULL;
	}

	jpeg_finish_decompress(&cinfo);
	jpeg_destroy_decompress(&cinfo);

	fclose(fp);

	return buf;
}

XImage *create_ximage_from_buffer(Display *dis, int screen, uint8_t *in_buf, int width, int height)
{
	int depth;
	size_t numNewBufBytes;
	XImage *img = NULL;
	Visual *visual;
	double rRatio;
	double gRatio;
	double bRatio;
	unsigned int r, g, b;
	int j = 0;
	int i;
	int numBufBytes = (3 * (width * height));

	int bitmap_pad;

	depth = DefaultDepth(dis, screen);
	visual = DefaultVisual(dis, screen);

	rRatio = visual->red_mask / 255.0;
	gRatio = visual->green_mask / 255.0;
	bRatio = visual->blue_mask / 255.0;

	if (depth >= 24) {
		bitmap_pad = 32;
		numNewBufBytes = (4 * (width * height));
		uint32_t *newBuf = malloc(numNewBufBytes);

		for (i = 0; i < numBufBytes; ++i) {
			r = (in_buf[i] * rRatio);
			++i;
			g = (in_buf[i] * gRatio);
			++i;
			b = (in_buf[i] * bRatio);

			r &= visual->red_mask;
			g &= visual->green_mask;
			b &= visual->blue_mask;

			newBuf[j] = r | g | b;
			++j;
		}
		img = XCreateImage(dis, CopyFromParent, depth, ZPixmap, 0,
			  (char *) newBuf, width, height, bitmap_pad, 0);

	} else if (depth >= 15) {
		bitmap_pad = 16;
		numNewBufBytes = (2 * (width * height));
#if 0
		uint16_t *newBuf = malloc(numNewBufBytes);
#else
		uint32_t *newBuf = malloc(numNewBufBytes);
#endif

		for (i = 0; i < numBufBytes; ++i) {
			r = (in_buf[i] * rRatio);
			++i;
			g = (in_buf[i] * gRatio);
			++i;
			b = (in_buf[i] * bRatio);

			r &= visual->red_mask;
			g &= visual->green_mask;
			b &= visual->blue_mask;

			newBuf[j] = r | g | b;
			++j;
		}
		img = XCreateImage(dis, CopyFromParent, depth, ZPixmap, 0,
			  (char *) newBuf, width, height, bitmap_pad, 0);

	} else {
		fprintf(stderr, "This program does not support displays with a depth less than 15.");
		return NULL;
	}

	XInitImage(img);

#ifdef XIMG_DEBUG
	fprintf(stderr, "XImage:\n");
	fprintf(stderr, "  size: w=%d h=%d\n", img->width, img->height);
	fprintf(stderr, "  n. of pixels offset in X direction:%d\n", img->xoffset);
	fprintf(stderr, "  Format:%d\n", img->format);
	fprintf(stderr, "  data at 0x%p\n", img->data);
	fprintf(stderr, "  byte order:%s\n", (img->byte_order == LSBFirst) ? "LSBFirst" : "MSBFirst");
	fprintf(stderr, "  Quant. of scanline:%d\n", img->bitmap_unit);
	fprintf(stderr, "  bitmap bit order:%s\n", (img->bitmap_bit_order == LSBFirst) ? "LSBFirst" : "MSBFirst");
	fprintf(stderr, "  bitmap pad:%d\n", img->bitmap_pad);
	fprintf(stderr, "  depth:%d\n", img->depth);
	fprintf(stderr, "  bytes_per_line:%d\n", img->bytes_per_line);
	fprintf(stderr, "  bits_per_pixel:%d\n", img->bits_per_pixel);
	fprintf(stderr, "  Red mask:%ld Green mask:%ld Blue mask:%ld\n", img->red_mask, img->green_mask, img->blue_mask);
	fprintf(stderr, "  obdata: 0x%p\n", img->obdata);
	fprintf(stderr, "  Funcs:\n");
	fprintf(stderr, "    create_image: 0x%p\n", img->f.create_image);
	fprintf(stderr, "    destroy_image: 0x%p\n", img->f.destroy_image);
	fprintf(stderr, "    get_pixel: 0x%p\n", img->f.get_pixel);
	fprintf(stderr, "    put_pixel: 0x%p\n", img->f.put_pixel);
	fprintf(stderr, "    sub_image: 0x%p\n", img->f.sub_image);
	fprintf(stderr, "    add_pixel: 0x%p\n", img->f.add_pixel);
#endif

	/* Set the client's byte order, so that XPutImage knows what to do
	 * with the data.  The default in a new X image is the server's format,
	 * which may not be what we want.
	 */
#if 0
	if (LSBFirst == get_byte_order()) {
		img->byte_order = LSBFirst;
	} else {
		img->byte_order = MSBFirst;
	}
#else
	/*The bitmap_bit_order doesn't matter with ZPixmap images.*/
	img->bitmap_bit_order = MSBFirst;
#endif

	return img;
}

#ifdef TEST_JPEG_MODULE

int main(int argc, char **argv)
{
	int running = 1;
	int imageWidth;
	int imageHeight;
	XImage *img;
	Window mainWin;
	int screen;
	Display *dis;
	uint8_t *buf;
	GC copyGC;

	unsigned long windowMask;
	XSetWindowAttributes winAttrib;


	Atom wm_protocols;
	Atom wm_delete_window;

	if (2 != argc) {
		fprintf (stderr, "please specify a filename to %s\n", argv[0]);
		exit(EXIT_FAILURE);
	}

	buf = decode_jpeg_file(argv[1], &imageWidth, &imageHeight);
	if (NULL == buf) {
		fprintf (stderr, "Unable to decode JPEG");
		exit(EXIT_FAILURE);
	}

	dis = XOpenDisplay(NULL);
	screen = DefaultScreen(dis);

	img = create_ximage_from_buffer(dis, screen, buf, imageWidth, imageHeight);

	if (NULL == img) {
		exit(EXIT_FAILURE);
	}

	/* create_image_from_buffer creates a new buffer after translation,
	 * so we can free. */
	free(buf);

	/* Create main window */

	windowMask = CWBackPixel | CWBorderPixel;
	winAttrib.border_pixel = BlackPixel(dis, screen);
	winAttrib.background_pixel = BlackPixel(dis, screen);
	winAttrib.override_redirect = 0;

	mainWin = XCreateWindow(dis, DefaultRootWindow(dis),
				20, 20,			/* x , y */
				imageWidth, imageHeight,
				0, DefaultDepth(dis, screen),
				InputOutput, CopyFromParent,
				windowMask, &winAttrib);

	copyGC = XCreateGC(dis, mainWin, 0, NULL);

	XMapRaised(dis, mainWin);

	/* Tell window system we're willing to handle window-delete messages */
	wm_protocols = XInternAtom(dis, "WM_PROTOCOLS", False);
	wm_delete_window = XInternAtom(dis, "WM_DELETE_WINDOW", False);
	if (XSetWMProtocols(dis, mainWin, &wm_delete_window, 1) == 0) {
		printf("%s: Cannot set ATOM\n", __func__);
	}

	XSelectInput(dis, mainWin, ExposureMask | KeyPressMask);

	while (running) {
		XEvent event;
		XNextEvent(dis, &event);
		switch (event.type) {
		case Expose:
			XPutImage(dis, mainWin, copyGC, img, 0, 0, 0, 0, imageWidth, imageHeight);
			XFlush(dis);
			break;
		case KeyPress:
			if (XK_q == XLookupKeysym(&event.xkey, 0))
				running = 0;
			break;
		/* Process any custom messages. */
		case ClientMessage:
			/* Check if we're going to quit */
			if ((event.xclient.message_type == wm_protocols) &&
			    (event.xclient.data.l[0] == (long) wm_delete_window)) {
				running = 0;
			}
			break;
		}
	}

	return EXIT_SUCCESS;
}

#endif /* TEST_JPEG_MODULE */
