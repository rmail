/*---------------------------------------------------------------------------
   rpng - simple PNG display program                              readpng.c
  ---------------------------------------------------------------------------

Copyright (c) 1998-2000 Greg Roelofs.  All rights reserved.

 This software is provided "as is," without warranty of any kind,
 express or implied.  In no event shall the author or contributors
 be held liable for any damages arising in any way from the use of
 this software.

 Permission is granted to anyone to use this software for any purpose,
 including commercial applications, and to alter it and redistribute
 it freely, subject to the following restrictions:

 1. Redistributions of source code must retain the above copyright
    notice, disclaimer, and this list of conditions.
 2. Redistributions in binary form must reproduce the above copyright
    notice, disclaimer, and this list of conditions in the documenta-
    tion and/or other materials provided with the distribution.
 3. All advertising materials mentioning features or use of this
    software must display the following acknowledgment:

 This product includes software developed by Greg Roelofs
 and contributors for the book, "PNG: The Definitive Guide,"
 published by O'Reilly and Associates.
---------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>

#include <X11/Xlib.h>

#include <png.h>        /* libpng header; includes zlib.h */

static int readpng_init(FILE *infile, unsigned long *pWidth, unsigned long *pHeight);
static int readpng_get_bgcolor(unsigned char *bg_red, unsigned char *bg_green, unsigned char *bg_blue);
static unsigned char *readpng_get_image(double display_exponent, int *pChannels, unsigned long *pRowbytes);
static void readpng_cleanup(int free_image_data);

/* future versions of libpng will provide this macro: */
#ifndef png_jmpbuf
#  define png_jmpbuf(png_ptr)   ((png_ptr)->jmpbuf)
#endif

static png_structp png_ptr = NULL;
static png_infop info_ptr = NULL;

png_uint_32 width;
png_uint_32 height;
int color_type;
int bit_depth;
unsigned char *image_data = NULL;

#ifndef TRUE
#  define TRUE 1
#  define FALSE 0
#endif

#define NO_24BIT_MASKS

/* could just include png.h, but this macro is the only thing we need
 * (name and typedefs changed to local versions); note that side effects
 * only happen with alpha (which could easily be avoided with
 * "unsigned short acopy = (alpha);")
 */

#define alpha_composite(composite, fg, alpha, bg) {				  \
	unsigned short temp = ((unsigned short) (fg) * (unsigned short) (alpha) + \
			       (unsigned short) (bg) * (unsigned short) (255 - (unsigned short) (alpha)) + (unsigned short) 128);  \
       (composite) = (unsigned char)((temp + (temp >> 8)) >> 8);		  \
}

static unsigned char bg_red   = 0;
static unsigned char bg_green = 0;
static unsigned char bg_blue  = 0;

static int rpng_x_msb(unsigned long u32val)
{
	int i;

	for (i = 31;  i >= 0;  --i) {
		if (u32val & 0x80000000L)
			break;
		u32val <<= 1;
	}
	return i;
}

XImage *create_ximage_from_PNG(Display *d, Visual *v, int depth, const char *filename, XColor *bg)
{
	XImage *ximg;
	int rc;
	FILE *fh;
	unsigned long iw;   /* image width */
	unsigned long ih;   /* image height */

	unsigned char *image_data;

	int image_channels;

	unsigned long image_rowbytes;

	int ximage_rowbytes;
	unsigned char *xdata;

	unsigned char *src;
	char *dest;

	unsigned char r;
	unsigned char g;
	unsigned char b;
	unsigned char a;

	unsigned long red;
	unsigned long green;
	unsigned long blue;

	unsigned long i;
	unsigned long row;

	unsigned long pixel;

	int RShift;
	int GShift;
	int BShift;

	unsigned long RMask;
	unsigned long GMask;
	unsigned long BMask;

	if (depth < 24) {
		/* <RB> support only true-color */
		fprintf(stderr, "screen depth %d not supported (24- or 32-bit TrueColor)\n", depth);
		return NULL;
	}

	fh = fopen(filename, "rb");
	if (!fh) {
		fprintf(stderr, "Can't open PNG file [%s]\n", filename);
		return NULL;
	}

	rc = readpng_init(fh, &iw, &ih);
	if (rc != 0) {
		fprintf(stderr, "Read PNG error [%d]\n", rc);
		fclose(fh);
		return NULL;
	}

#if 0
	fprintf(stderr, "visual bits x RGB [%d]\n", v->bits_per_rgb);
#endif

	/* if the user didn't specify a background color on the command line,
	 * check for one in the PNG file--if not, the initialized values of 0
	 * (black) will be used */
	if (bg != NULL) {
		bg_red   = (unsigned char) bg->red;
		bg_green = (unsigned char) bg->green;
		bg_blue  = (unsigned char) bg->blue;
	} else if (readpng_get_bgcolor(&bg_red, &bg_green, &bg_blue) > 1) {
		readpng_cleanup(TRUE);
		fprintf(stderr, ":  libpng error while checking for background color\n");
		return NULL;
	}

	/*---------------------------------------------------------------------------
	Allocate memory for the X- and display-specific version of the image.
	---------------------------------------------------------------------------*/
	xdata = (unsigned char *) malloc(4 * iw * ih);
	if (!xdata) {
		fprintf(stderr, ":  unable to allocate image memory\n");
		return NULL;
	}

	ximg = XCreateImage(d, v, depth, ZPixmap, 0, (char *) xdata, iw, ih, 32, 0);
	if (!ximg) {
		fprintf(stderr, ":  XCreateImage() failed\n");
		free(xdata);
		return NULL;
	}

	/* To avoid testing the byte order every pixel (or doubling the size
	 * of the drawing routine with a giant if-test), we arbitrarily set
	 * the byte order to MSBFirst and let Xlib worry about inverting things
	 * on little-endian machines.
	 * This is not the most efficient approach (the giant if-test would be
	 * better), but in the interest of clarity, we take the easy way out.
	 */
	ximg->byte_order = MSBFirst;

	/* decode the image, all at once */

	/* First set the default value for our display-system exponent, i.e.,
	 * the product of the CRT exponent and the exponent corresponding to
	 * the frame-buffer's lookup table (LUT), if any.  This is not an
	 * exhaustive list of LUT values (e.g., OpenStep has a lot of weird
	 * ones), but it should cover 99% of the current possibilities. */
#if 0
	LUT_exponent = 1.0;		   /* Assume no LUT:  most PCs */
	double CRT_exponent = 2.2;         /* Just the monitor */

	/* the defaults above give 1.0, 1.3, 1.5 and 2.2, respectively: */
	display_exponent = LUT_exponent * CRT_exponent;
#endif
	image_data = readpng_get_image(1.0 * 2.2, &image_channels, &image_rowbytes);

	/* Done with PNG file, so clean up to minimize memory usage
	 * (but do NOT nuke image_data!) */
	readpng_cleanup(FALSE);

	/* It's now safe to close file stream. */
	fclose(fh);

	if (!image_data) {
		fprintf(stderr,":  unable to decode PNG image\n");
		free(xdata);
		return NULL;
	}

	if (image_channels != 4) {
		free(xdata);
		free(image_data);
		image_data = NULL;
		fprintf(stderr,":  only 4 channel PNG\n");
		return NULL;
	}

	ximage_rowbytes = ximg->bytes_per_line;

#if 0
	fprintf(stderr, "beginning display loop (image_channels == %d)\n", image_channels);
	fprintf(stderr, "   (width = %ld, rowbytes = %ld, ximage_rowbytes = %d)\n", iw, image_rowbytes, ximage_rowbytes);
	fprintf(stderr, "   (ximage.width = %d, ximage.height = %d)\n", ximg->width, ximg->height);
	fprintf(stderr, "   (bpp = %d)\n", ximg->bits_per_pixel);
	fprintf(stderr, "   (byte_order = %s)\n", ximg->byte_order == MSBFirst? "MSBFirst" : (ximg->byte_order == LSBFirst? "LSBFirst" : "unknown"));
#endif
	RMask = v->red_mask;
	GMask = v->green_mask;
	BMask = v->blue_mask;

#ifdef NO_24BIT_MASKS
	RShift = rpng_x_msb(RMask) - 7;     /* these are left-shifts */
	GShift = rpng_x_msb(GMask) - 7;
	BShift = rpng_x_msb(BMask) - 7;
#else
	RShift = 7 - rpng_x_msb(RMask);     /* these are right-shifts, too */
	GShift = 7 - rpng_x_msb(GMask);
	BShift = 7 - rpng_x_msb(BMask);
#endif

	if (RShift < 0 || GShift < 0 || BShift < 0) {
		fprintf(stderr, "rpng internal logic error:  negative X shift(s)!\n");
		return NULL;
	}

	for (row = 0; row < ih; ++row) {
		src = image_data + (row * image_rowbytes);
		dest = ximg->data + (row * ximage_rowbytes);
		/* image_channels == 4 */
		for (i = iw; i > 0; --i) {
			r = *src++;
			g = *src++;
			b = *src++;
			a = *src++;
			if (a == 255) {
				red   = r;
				green = g;
				blue  = b;
			} else if (a == 0) {
				red   = bg_red;
				green = bg_green;
				blue  = bg_blue;
			} else {
				/* This macro (from png.h) composites the
				 * foreground and background values and puts
				 * the result into the first argument */
				alpha_composite(red,   r, a, bg_red);
				alpha_composite(green, g, a, bg_green);
				alpha_composite(blue,  b, a, bg_blue);
			}
			pixel = (red << RShift) | (green << GShift) | (blue  << BShift);
			/* recall that we set ximage->byte_order = MSBFirst above */
			*dest++ = (char)((pixel >> 24) & 0xff);
			*dest++ = (char)((pixel >> 16) & 0xff);
			*dest++ = (char)((pixel >>  8) & 0xff);
			*dest++ = (char)( pixel        & 0xff);
		}
	}

	if (image_data) {
		free(image_data);
		image_data = NULL;
	}

	return ximg;
}

/* return value = 0 for success, 1 for bad sig, 2 for bad IHDR, 4 for no mem */
static int readpng_init(FILE *infile, unsigned long *pWidth, unsigned long *pHeight)
{
	unsigned char sig[8];

	/* first do a quick check that the file really is a PNG image; could
	 * have used slightly more general png_sig_cmp() function instead */
	fread(sig, 1, 8, infile);
	if (!png_check_sig(sig, 8))
		return 1;   /* bad signature */

	/* could pass pointers to user-defined error handlers instead of NULLs: */
	png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
	if (!png_ptr)
		return 4;   /* out of memory */

	info_ptr = png_create_info_struct(png_ptr);
	if (!info_ptr) {
		png_destroy_read_struct(&png_ptr, NULL, NULL);
		return 4;   /* out of memory */
	}

	/* we could create a second info struct here (end_info), but it's only
	 * useful if we want to keep pre- and post-IDAT chunk info separated
	 * (mainly for PNG-aware image editors and converters) */

	/* setjmp() must be called in every function that calls a PNG-reading
	 * libpng function */

	if (setjmp(png_jmpbuf(png_ptr))) {
		png_destroy_read_struct(&png_ptr, &info_ptr, NULL);
		return 2;
	}

	png_init_io(png_ptr, infile);
	png_set_sig_bytes(png_ptr, 8);	/* we already read the 8 signature bytes */

	png_read_info(png_ptr, info_ptr);	/* read all PNG info up to image data */

	/* alternatively, could make separate calls to png_get_image_width(),
	 * etc., but want bit_depth and color_type for later [don't care about
	 * compression_type and filter_type => NULLs] */

	png_get_IHDR(png_ptr, info_ptr, &width, &height, &bit_depth, &color_type, NULL, NULL, NULL);
	*pWidth = width;
	*pHeight = height;

	/* OK, that's all we need for now; return happy */
	return 0;
}

/* returns	0 if succeeds,
 *		1 if fails due to no bKGD chunk,
 *		2 if libpng error.
 *
 * scales values to 8-bit if necessary */
static int readpng_get_bgcolor(unsigned char *red, unsigned char *green, unsigned char *blue)
{
	png_color_16p pBackground;

	/* setjmp() must be called in every function that calls a PNG-reading
	 * libpng function */
	if (setjmp(png_jmpbuf(png_ptr))) {
		png_destroy_read_struct(&png_ptr, &info_ptr, NULL);
		return 2;
	}

	if (!png_get_valid(png_ptr, info_ptr, PNG_INFO_bKGD))
		return 1;

	/* it is not obvious from the libpng documentation, but this function
	 * takes a pointer to a pointer, and it always returns valid red, green
	 * and blue values, regardless of color_type: */

	png_get_bKGD(png_ptr, info_ptr, &pBackground);

	/* however, it always returns the raw bKGD data, regardless of any
	 * bit-depth transformations, so check depth and adjust if necessary */
	if (bit_depth == 16) {
		*red   = pBackground->red   >> 8;
		*green = pBackground->green >> 8;
		*blue  = pBackground->blue  >> 8;
	} else if (color_type == PNG_COLOR_TYPE_GRAY && bit_depth < 8) {
		if (bit_depth == 1)
			*red = *green = *blue = pBackground->gray? 255 : 0;
		else if (bit_depth == 2)
			*red = *green = *blue = (255/3) * pBackground->gray;
		else /* bit_depth == 4 */
			*red = *green = *blue = (255/15) * pBackground->gray;
	} else {
		*red   = (unsigned char) pBackground->red;
		*green = (unsigned char) pBackground->green;
		*blue  = (unsigned char) pBackground->blue;
	}
	return 0;
}

/* display_exponent == LUT_exponent * CRT_exponent */
static unsigned char *readpng_get_image(double display_exponent, int *pChannels, unsigned long *pRowbytes)
{
	double  gamma;
	png_uint_32  i, rowbytes;
	png_bytepp  row_pointers = NULL;

	/* setjmp() must be called in every function that calls a PNG-reading
	 * libpng function */
	if (setjmp(png_jmpbuf(png_ptr))) {
		png_destroy_read_struct(&png_ptr, &info_ptr, NULL);
		return NULL;
	}

	/* expand palette images to RGB, low-bit-depth grayscale images to 8 bits,
	 * transparency chunks to full alpha channel; strip 16-bit-per-sample
	 * images to 8 bits per sample; and convert grayscale to RGB[A] */
	if (color_type == PNG_COLOR_TYPE_PALETTE)
		png_set_expand(png_ptr);
	if (color_type == PNG_COLOR_TYPE_GRAY && bit_depth < 8)
		png_set_expand(png_ptr);
	if (png_get_valid(png_ptr, info_ptr, PNG_INFO_tRNS))
		png_set_expand(png_ptr);
	if (bit_depth == 16)
		png_set_strip_16(png_ptr);
	if (color_type == PNG_COLOR_TYPE_GRAY ||
	    color_type == PNG_COLOR_TYPE_GRAY_ALPHA)
		png_set_gray_to_rgb(png_ptr);

	/* unlike the example in the libpng documentation, we have *no* idea where
	 * this file may have come from--so if it doesn't have a file gamma, don't
	 * do any correction ("do no harm") */
	if (png_get_gAMA(png_ptr, info_ptr, &gamma))
		png_set_gamma(png_ptr, display_exponent, gamma);

	/* all transformations have been registered; now update info_ptr data,
	 * get rowbytes and channels, and allocate image memory */
	png_read_update_info(png_ptr, info_ptr);

	*pRowbytes = rowbytes = png_get_rowbytes(png_ptr, info_ptr);
	*pChannels = (int)png_get_channels(png_ptr, info_ptr);

	image_data = (unsigned char *) malloc(rowbytes * height);
	if (image_data == NULL) {
		png_destroy_read_struct(&png_ptr, &info_ptr, NULL);
		return NULL;
	}

	row_pointers = (png_bytepp) malloc(height * sizeof(png_bytep));
	if (row_pointers == NULL) {
		png_destroy_read_struct(&png_ptr, &info_ptr, NULL);
		free(image_data);
		image_data = NULL;
		return NULL;
	}
#if 0
	fprintf(stderr, "readpng_get_image:  rowbytes = %ld, height = %ld\n", rowbytes, height);
#endif

	/* set the individual row_pointers to point at the correct offsets */
	for (i = 0;  i < height;  ++i)
		row_pointers[i] = image_data + i*rowbytes;

	/* now we can go ahead and just read the whole image */
#if 0
	fprintf(stderr, "png_ptr: %p, row_pointers:%p\n", (void *) png_ptr, (void *) row_pointers);
#endif
	png_read_image(png_ptr, row_pointers);

	/* and we're done!  (png_read_end() can be omitted if no processing of
	 * post-IDAT text/time/etc. is desired) */
	free(row_pointers);
	row_pointers = NULL;

	png_read_end(png_ptr, NULL);

	return image_data;
}

static void readpng_cleanup(int free_image_data)
{
	if (free_image_data && image_data) {
		free(image_data);
		image_data = NULL;
	}

	if (png_ptr && info_ptr) {
		png_destroy_read_struct(&png_ptr, &info_ptr, NULL);
		png_ptr = NULL;
		info_ptr = NULL;
	}
}
