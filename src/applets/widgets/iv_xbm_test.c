/* $Id: ImageVT.c,v 1.1 1995/02/22 05:24:30 cwikla Exp $ */

#include <stdio.h>
#include <stdlib.h>

#include <X11/IntrinsicP.h>
#include <X11/StringDefs.h>
#include <X11/CoreP.h>
#include <X11/ShellP.h>

#include "iv_widget.h"


#define APPNAME "ImageViewTest"
#define APPCLASS "ImageViewTest"

static void QuitIt(__attribute__((unused)) Widget w,
		   __attribute__((unused)) caddr_t nil,
		   XEvent *event)
{
	if (event->type == ButtonPress)
			exit(1);
}

int main(int argc, char **argv)
{
	Widget imageViewWidget, toplevel;
	XtAppContext app;
	Display *theDisplay;
	int theScreenNumber;
	Arg warg[3];
	int n;
	unsigned int width, height;
	Pixmap pixmap, pixmap1bit;
	GC gc;
	int xhot, yhot;

	XtToolkitInitialize();
	app = XtCreateApplicationContext();

	theDisplay = XtOpenDisplay(app, NULL, APPNAME, APPCLASS, NULL, 0, &argc, argv);

	if (!theDisplay) {
		printf("%s: can't open display, exiting...\n", APPNAME);
		return 1;
	}

	theScreenNumber = DefaultScreen(theDisplay);

	toplevel = XtAppCreateShell(APPNAME, APPCLASS, applicationShellWidgetClass, theDisplay, NULL, 0);


	if (XReadBitmapFile(theDisplay, RootWindow(theDisplay, theScreenNumber),
		"test.xbm", &width, &height, &pixmap1bit, &xhot, &yhot) != Success) {
		printf("%s: can't open test.xbm!\n", APPNAME);
		return 1;
	}

	gc = DefaultGC(theDisplay, theScreenNumber);

	if (DefaultDepth(theDisplay, theScreenNumber) != 1) {
		pixmap = XCreatePixmap(theDisplay, RootWindow(theDisplay, theScreenNumber),
			width, height, DefaultDepth(theDisplay, theScreenNumber));

		XCopyPlane(theDisplay, pixmap1bit, pixmap, gc, 0, 0, width, height, 0, 0, 1);
		XFreePixmap(theDisplay, pixmap1bit);
	} else
		pixmap = pixmap1bit;

	n = 0;
	XtSetArg(warg[n], XtNpixmap, pixmap);
	n++;
	XtSetArg(warg[n], XtNimageType, 1);
	n++;
	imageViewWidget = XtCreateManagedWidget("ImageView", imageViewWidgetClass, toplevel, warg, n);

	XtRealizeWidget(toplevel);

	XtAddEventHandler(imageViewWidget, ButtonPressMask, FALSE,
			  (XtEventHandler) QuitIt, NULL);

	printf("Press mouse button to exit.\n");

	XtAppMainLoop(app);

	return 0;
}
