#ifndef IMAGEVIEW_WIDGET_H
#define IMAGEVIEW_WIDGET_H

extern WidgetClass imageViewWidgetClass;

typedef struct ImageViewClassRec *ImageViewWidgetClass;
typedef struct iww	 *ImageViewWidget;

#ifndef XtIsImageView
#define XtIsImageView(w)	XtIsSubclass((w), imageViewWidgetClass)
#endif

#define XtNsaveMemory	"saveMemory"
#define XtCSaveMemory	"SaveMemory"

#define XtNximage	"ximage"
#define XtCXImage	"XImage"
#define XtRXimage	"XImage"

#define XtNimageType	"imageType"
#define XtCImageType	"ImageType"
#define XtRImageType	"ImageType"

#define USE_XIMAGE	0
#define USE_PIXMAP	1

#define USE_UNSET	0xFF

#endif	/* IMAGEVIEW_WIDGET_H */
