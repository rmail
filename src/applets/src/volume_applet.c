/* An example of how to create a applet popup dialog box.  */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <limits.h>
#include <fcntl.h>
#include <string.h>
#include <errno.h>
#include <sys/ioctl.h>

#include <X11/Intrinsic.h>
#include <X11/StringDefs.h>
#include <X11/Shell.h>

#include <X11/Xaw/Box.h>
#include <X11/Xaw/Cardinals.h>
#include <X11/Xaw/Command.h>

#include <X11/Xaw/Scrollbar.h>

#include "iv_widget.h"
#include "volume_mixer.h"

#include "applet.h"

#define APPLET_NAME		"Applet_Volume"
#define APPLET_VERSION		"1.00"

#define PNG_VOLUME_LOW		"audio-volume-low.png"
#define PNG_VOLUME_MID		"audio-volume-medium.png"
#define PNG_VOLUME_MAX		"audio-volume-high.png"
#define PNG_VOLUME_MUTE		"audio-volume-mute.png"

/* Window manager messages */
static Atom wm_protocols;
static Atom wm_delete_window;

/* Colours :
 *
 * #4682B4 steelblue
 * #9ACD32 Yellow green
 */
/* Fallback Resources */
static String fallback_res[] = {
	"*Applet.height: 32",

	"*Slider.width: 22",
	"*Slider.height: 100",
	"*Slider.foreground: #4682B4",
	"*Slider.borderWidth: 1",
	"*Slider.orientation: Vertical",
	"*Slider.thickness: 1",

	"*VolumePopup.foreground: #4682B4",

	"*PopupBox.foreground: #4682B4",

	"*Label.foreground: #9ACD32",

	"*Button.foreground: #4682B4",

	/* Defaults if not set */
	"*width: 32",
	"*resize: False",
	"*borderWidth: 0",
	"*borderColor: #4682B4",	/* steelblue */
	"*highlightThickness 0",
	"*background: black",

	NULL,
};

/* Application resources data structure */
typedef struct {
	Boolean   debug;	/* Enable/Disable debug */
	Boolean   help;		/* Show applet commandline help */
	String    dpath;	/* Data path (icons, ...) */
} AppResourcesRec;

static AppResourcesRec appRes;

/* Application resources specification */
static XtResource appResourcesSpec[] = {
	{
		"debug", "Debug", XtRBoolean, sizeof(Boolean),
		XtOffsetOf(AppResourcesRec, debug),
		XtRImmediate, (XtPointer) False
	},{
		"help", "Help", XtRBoolean, sizeof(Boolean),
		XtOffsetOf(AppResourcesRec, help),
		XtRImmediate, (XtPointer) False
	},{
		/* Icons path */
		"dpath", "Dpath", XtRString, sizeof(String),
		XtOffsetOf(AppResourcesRec, dpath),
		XtRString, (XtPointer) ICON_DEFAULT_PATH
	}
};

static Widget app;
static Widget app_button;
static Widget box;		/* Popup box */
static Widget vol_label;	/* Label for % Volume */

static XImage *vol_pix_mute;
static XImage *vol_pix_low;
static XImage *vol_pix_mid;
static XImage *vol_pix_max;
static XImage *pix_applet_status;

static float scrollValue;

static int Volume = 50;		/* Test default */
static int PopUpPresent = 0;

void applet_set_icon(void)
{
	if (Volume == 0)
		pix_applet_status = vol_pix_mute;
	else if (Volume > 75)
		pix_applet_status = vol_pix_max;
	else if (Volume > 30)
		pix_applet_status = vol_pix_mid;
	else if (Volume > 0)
		pix_applet_status = vol_pix_low;

#if 0
	XtVaSetValues(app_button, XtNbitmap, pix_applet_status, NULL);
#else
	XtVaSetValues(app_button, XtNximage, pix_applet_status, XtNimageType, 0, NULL);
#endif
}

/* destroy_popup: Destroys the popup box widget.
 * Arguments:
 *	w - *** UNUSED ***.
 *	box - the box widget.  This widget is a direct child of the popup shell to destroy.
 *	p - *** UNUSED **.
 */
static void destroy_popup(UNUSED Widget w, XtPointer box, UNUSED XtPointer p)
{
	Widget popup = XtParent((Widget) box);

	XtDestroyWidget(popup);
	PopUpPresent = 0;

	/* Update the icon */
	applet_set_icon();
}

/* Called when user selects scroll up or scroll down */
static void ScrollCB(Widget w, UNUSED XtPointer client, XtPointer pos)
{
	char buf[8];

	/* topOfThumb. The location of the top of the thumb,
	 * as a percentage (0.0 - 1.0)
	 * of the length of the scrollbar.
	 */
	XtVaGetValues(w, XtNtopOfThumb, &scrollValue, NULL);

	if ((int) pos < 0) {
		scrollValue -= 0.01;
	} else {
		scrollValue += 0.01;
	}

	Volume = (int) (100.0 * (1.0 - scrollValue));
	mixer_set_volume(Volume);

	/* Update % label */
	sprintf(buf, "%2d%%", Volume);
	XtVaSetValues(vol_label, XtNlabel, buf, NULL);

	/* Update thumb position */
	XawScrollbarSetThumb(w, scrollValue, 0.0);
}

/* Called when user drags slider */
static void JumpCB(UNUSED Widget w, UNUSED XtPointer client, XtPointer posn)
{
	char buf[8];

	scrollValue = *(float *) posn;
	Volume = (int) (100.0 - scrollValue * 100.0);
	mixer_set_volume(Volume);

	/* Update % label */
	sprintf(buf, "%2d%%", Volume);
	XtVaSetValues(vol_label, XtNlabel, buf, NULL);
}

static void create_slider(Widget w, UNUSED XtPointer p1, UNUSED XtPointer p2)
{
	char buf[8];

	Widget popup;
	Widget slider;

	Arg args[8];
	Cardinal n;

	/* x,y,h of the caller widget */
	Position wx;
	Position wy;
	Dimension wh;

	/* Scrollbar translation */
	String slider_trans = "#override "
		"<Btn1Down>: StartScroll(Forward)\n"		/* Left button */
		"<Btn2Down>: \n"				/* Middle button */
		"<Btn3Down>: StartScroll(Backward)\n"		/* Right button */
		"<Btn4Down>: StartScroll(Backward)\n"		/* Wheel down */
		"<Btn5Down>: StartScroll(Forward)\n"		/* Wheel up */
		"<Btn1Motion>: MoveThumb() NotifyThumb()\n"
		"<BtnUp>: NotifyScroll(Proportional) EndScroll()";

	if (PopUpPresent) {
		destroy_popup(w, (XtPointer) box, NULL);
		return;
	}

	/* Get caller widget geometry */
	n = 0;
	XtSetArg(args[n], XtNheight, &wh);
	n++;
	XtSetArg(args[n], XtNx, &wx);
	n++;
	XtSetArg(args[n], XtNy, &wy);
	n++;
	XtGetValues(w, args, n);

	/* We need only they */
	XtTranslateCoords(w, 0, (Position) wh, &wx, &wy);

	n = 0;
	XtSetArg(args[n], XtNx, wx);
	n++;

	if (wy > wh + 120) {
		XtSetArg(args[n], XtNy, wy - wh - 133);
	} else {
		XtSetArg(args[n], XtNy, wy);
	}

	n++;

	popup = XtCreatePopupShell("VolumePopup", transientShellWidgetClass, w, args, n);

	/* PopupBox contains Label and Slider */
	box = XtCreateManagedWidget("PopupBox", boxWidgetClass, popup, NULL, 0);

	sprintf(buf, "%2d%%", Volume);
	vol_label = XtVaCreateManagedWidget("Label", labelWidgetClass, box,
					     XtNlabel, buf,
					     NULL);
	/* Slider */
	slider = XtCreateManagedWidget("Slider", scrollbarWidgetClass, box, NULL ,0);

	/* Override default translations */
	XtOverrideTranslations(slider, XtParseTranslationTable(slider_trans));

	XtAddCallback(slider, XtNscrollProc, ScrollCB, NULL);
	XtAddCallback(slider, XtNjumpProc, JumpCB, NULL);

	scrollValue = 1.0 - ((float) Volume/100.0);
	XawScrollbarSetThumb(slider, scrollValue, 0.0);

	/* Popup widget */
	XtPopup(popup, XtGrabNone);
	PopUpPresent = 1;
}

static void applet_create(Widget app)
{
	Display *d;
	Visual  *visual;

	int screen;

	/* Depth of the root window */
	int depth;

	/* Base path for icons [POSIX "PATH_MAX" defined in limits.h] */
	char path[PATH_MAX];

	d = XtDisplay(app);

	screen = DefaultScreen(d);
	depth  = DisplayPlanes(d, screen);
	visual = XDefaultVisual(d, screen);

	/* Create status pixmaps */
	snprintf(path, sizeof(path),"%s/%s", appRes.dpath, PNG_VOLUME_MUTE);
	vol_pix_mute = create_ximage_from_PNG(d, visual, depth, path, NULL);

	snprintf(path, sizeof(path),"%s/%s", appRes.dpath, PNG_VOLUME_LOW);
	vol_pix_low  = create_ximage_from_PNG(d, visual, depth, path, NULL);

	snprintf(path, sizeof(path),"%s/%s", appRes.dpath, PNG_VOLUME_MID);
	vol_pix_mid  = create_ximage_from_PNG(d, visual, depth, path, NULL);

	snprintf(path, sizeof(path),"%s/%s", appRes.dpath, PNG_VOLUME_MAX);
	vol_pix_max  = create_ximage_from_PNG(d, visual, depth, path, NULL);

	/* Set initial pixmap */
	pix_applet_status = vol_pix_max;

	app_button = XtVaCreateManagedWidget("IV", imageViewWidgetClass, app,
					     XtNximage,    pix_applet_status,
					     XtNimageType, 0,
					     NULL);

	/* Set handler for mouse button press on status icon */
	XtAddEventHandler(app_button, ButtonPressMask, FALSE,
			  (XtEventHandler) create_slider, (XtPointer) NULL);

	applet_set_icon();

	XtManageChild(app_button);
}

/**************************************************************************/
static void WMProtocols(Widget w, XEvent *ev, UNUSED String *params, UNUSED Cardinal *nparams)
{
	if (ev->type == ClientMessage  &&
	    ev->xclient.message_type == wm_protocols  &&
	    ev->xclient.data.l[0] == (long) wm_delete_window) {
		if (w == app) {
			mixer_close();
			exit(0);
		}
	}
}

static void add_wm_handler(Widget w)
{
	XtActionsRec wm_actions[] = {
		{"WMProtocols", WMProtocols }
	};

	XtAppAddActions(XtWidgetToApplicationContext(w), wm_actions, XtNumber(wm_actions));

	XtOverrideTranslations(w, XtParseTranslationTable("<Message>WM_PROTOCOLS: WMProtocols()"));

	wm_protocols = XInternAtom(XtDisplay(w), "WM_PROTOCOLS", False);
	wm_delete_window = XInternAtom(XtDisplay(w), "WM_DELETE_WINDOW", False);

	XSetWMProtocols(XtDisplay(w), XtWindow(w), &wm_delete_window, 1);
}

/**
 * Print the command-line options for this application.
 *
 * @param[in] argc	Standard argument count
 * @param[in] argv	Standard argument array
 *
 */
static void usage(UNUSED int argc, char **argv)
{
	fprintf(stderr, "%s v. %s\n", APPLET_NAME, APPLET_VERSION);
	fprintf(stderr, "Usage: %s [options]\n\n"
		"Options:\n"
		"  -help   Show this help screen\n"
		"  -debug  Show debug messages\n"
		"  -dpath  Override default pixmap path\n"
		, argv[0]);
}

int main(int argc, char **argv)
{
	XtAppContext ctx;

	/* Command line options specification */
	static XrmOptionDescRec app_cmdline[] = {
		{ "-debug", "*debug", XrmoptionNoArg, "True" },
		{ "-help", "*help", XrmoptionNoArg, "True" },
		{ "-dpath", "*dpath", XrmoptionSepArg, 0 }
	};

	app = XtAppInitialize(&ctx, APPLET_NAME,
			      app_cmdline, XtNumber(app_cmdline),
			      &argc, argv,
			      fallback_res, NULL, 0);

	/* Load application resources */
	XtGetApplicationResources(app, (XtPointer) &appRes, appResourcesSpec, XtNumber(appResourcesSpec), NULL, 0);

	if (appRes.help) {
		usage(argc, argv);
		return -1;
	}

	if (appRes.debug) {
		fprintf(stderr, "%s - %s dpath:%s\n", APPLET_NAME, __func__, appRes.dpath);
	}

	/* Get initial state before create applet widgets */
	if (mixer_open() < 0)
		return -1;

	Volume = mixer_get_volume();

	applet_create(app);

	XtRealizeWidget(app);

	/* Tell Window System we're willing to handle window messages */
	add_wm_handler(app);

	XtAppMainLoop(ctx);

	return mixer_close();
}
