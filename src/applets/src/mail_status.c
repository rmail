#include <stdio.h>
#include <unistd.h>

#include "pop3.h"
#include "pop3_config.h"

#undef STATUS_DEBUG

/* Gets the current applet status */
int applet_get_status(void)
{
	struct pop3_mailbox mb;

	mb.auth.type = POP3_AUTH_USERPASS;

	mb.host.name = POP3_DEFAULT_HOST;
	mb.host.port = POP3_DEFAULT_PORT;
	mb.auth.user = POP3_DEFAULT_USER;
	mb.auth.pass = POP3_DEFAULT_PASSWORD;

#ifdef STATUS_DEBUG
	fprintf(stderr, "Connecting to %s port %s...\n", mb.host.name, mb.host.port);
#endif

	if (pop3_connect(&mb) < 0) {
		return -1;
	}

	if (pop3_authenticate(&mb) < 0) {
		fprintf(stderr, "POP3 Authentication failed\n");
		pop3_disconnect(&mb);
		return -1;
	}

	/* Retrieve the number of messages present on the mailbox */
	if (pop3_stat(&mb) < 0) {
		fprintf(stderr, "Error fetch messages\n");
		pop3_disconnect(&mb);
		return -1;
	}

	pop3_disconnect(&mb);

#ifdef STATUS_DEBUG
	fprintf(stderr, "%u Message in maildrop\n", mb.messages);
#endif

	if (mb.messages == 0)
		return 0;

	return 1;
}
