/*
 * mixer.h
 *
 * Audio Volume Applet -- Soundcard mixer interface
 */

#ifndef VOLUME_MIXER_H
#define VOLUME_MIXER_H

int mixer_open(void);
int mixer_close(void);
int mixer_set_volume(const int value);
int mixer_get_volume(void);

#endif	/* VOLUME_MIXER_H */
