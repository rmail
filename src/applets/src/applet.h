/*
 * Common define for X11 applets
 */
#ifndef APPLET_H
#define APPLET_H

/* GCC attibute */
#define UNUSED __attribute__((unused))

/* Default icons base path */
#define ICON_DEFAULT_PATH	"/usr/share/applets/pixmaps"


/* From png.c */
extern XImage *create_ximage_from_PNG(Display *d, Visual *v, int depth, const char *filename, XColor *bg);

#endif /* APPLET_H */
