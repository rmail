/* Network Applet  */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <limits.h>
#include <fcntl.h>
#include <string.h>
#include <errno.h>
#include <sys/ioctl.h>

#include <X11/Intrinsic.h>
#include <X11/StringDefs.h>
#include <X11/Shell.h>

#include <X11/Xaw/Cardinals.h>
#include <X11/Xaw/Command.h>

#include <X11/Xaw/Scrollbar.h>

#include "iv_widget.h"

#include "if_status.h"

#include "applet.h"

#define APPLET_NAME		"Applet_Net"
#define APPLET_VERSION		"1.00"

#define	PNG_NETWORK_UP		"network_up.png"
#define	PNG_NETWORK_DOWN	"network_down.png"
#define	PNG_NETWORK_ERROR	"network_down.png"

/* Current interface status */
static int applet_status = IFSTATUS_DOWN;

/* Window manager messages */
static Atom wm_protocols;
static Atom wm_delete_window;

/* Colours :
 *
 * #4682B4 steelblue
 * #9ACD32 Yellow green
 */
/* Fallback Resources */
static String fallback_res[] = {
	"*Applet.height: 32",

	"*Button.foreground: #4682B4",

	/* Defaults if not set */
	"*width: 32",
	"*resize: False",
	"*borderWidth: 0",
	"*borderColor: #4682B4",	/* steelblue */
	"*highlightThickness 0",
	"*background: black",

	NULL,
};

/* Application resources data structure */
typedef struct {
	Boolean   debug;	/* Enable/Disable debug */
	Boolean   help;		/* Show applet commandline help */
	String    dpath;	/* Data path (icons, ...) */
	Cardinal  wlan;
	String    name;
} AppResourcesRec;

static AppResourcesRec appRes;

/* Application resources specification */
static XtResource appResourcesSpec[] = {
	/* Common Applet Resources */
	{
		"debug", "Debug", XtRBoolean, sizeof(Boolean),
		XtOffsetOf(AppResourcesRec, debug),
		XtRImmediate, (XtPointer) False
	},{
		"help", "Help", XtRBoolean, sizeof(Boolean),
		XtOffsetOf(AppResourcesRec, help),
		XtRImmediate, (XtPointer) False
	},{
		/* Icons path */
		"dpath", "Dpath", XtRString, sizeof(String),
		XtOffsetOf(AppResourcesRec, dpath),
		XtRString, (XtPointer) ICON_DEFAULT_PATH
	},
	/* Specific Applet Resources */
	{
		/* Monitor wireless interface */
		"wlan", "Wlan", XtRInt, sizeof(int),
		XtOffsetOf(AppResourcesRec, wlan),
		XtRImmediate, (XtPointer) 0
	},{
		/* Network interface names */
		"name", "Name", XtRString, sizeof(String),
		XtOffsetOf(AppResourcesRec, name),
		XtRString, (XtPointer) "eth0"
	}
};

static Widget app;
static Widget app_button;

static XImage *pix_network_down;
static XImage *pix_network_up;
static XImage *pix_network_error;

static XImage *pix_applet_status;

/* Status update timer */
#define INTERVAL_MS 5000		/* Update every x ms */
static XtIntervalId update_timer;	/* Timer ID for state update */

void applet_set_icon(void)
{
	if (applet_status == IFSTATUS_UP) {
		pix_applet_status = pix_network_up;
	} else if (applet_status == IFSTATUS_DOWN) {
		pix_applet_status = pix_network_down;
	} else {
		/*IFSTATUS_ERR*/
		pix_applet_status = pix_network_error;
	}

	XtVaSetValues(app_button, XtNximage, pix_applet_status, XtNimageType, 0, NULL);
}

static void applet_create(Widget app)
{
	Display *d;
	Visual  *visual;

	int screen;

	/* Depth of the root window */
	int depth;

	/* Base path for icons [POSIX "PATH_MAX" defined in limits.h] */
	char path[PATH_MAX];

	d = XtDisplay(app);

	screen = DefaultScreen(d);
	depth  = DisplayPlanes(d, screen);
	visual = XDefaultVisual(d, screen);

	applet_status = net_monitor_interface(appRes.name, appRes.wlan);

	snprintf(path, sizeof(path),"%s/%s", appRes.dpath, PNG_NETWORK_UP);
	pix_network_up = create_ximage_from_PNG(d, visual, depth, path, NULL);

	snprintf(path, sizeof(path),"%s/%s", appRes.dpath, PNG_NETWORK_DOWN);
	pix_network_down = create_ximage_from_PNG(d, visual, depth, path, NULL);

	snprintf(path, sizeof(path),"%s/%s", appRes.dpath, PNG_NETWORK_ERROR);
	pix_network_error = create_ximage_from_PNG(d, visual, depth, path, NULL);

	app_button = XtVaCreateManagedWidget("IV", imageViewWidgetClass, app,
					     XtNximage,    pix_network_error,
					     XtNimageType, 0,
					     NULL);
#if 0
	XtAddEventHandler(app_button, ButtonPressMask, FALSE, create_slider, (XtPointer) NULL);
#endif

	applet_set_icon();

	XtManageChild(app_button);
}

/**************************************************************************/
static void WMProtocols(Widget w, XEvent *ev, UNUSED String *params, UNUSED Cardinal *nparams)
{
	if (ev->type == ClientMessage  &&
	    ev->xclient.message_type == wm_protocols  &&
	    ev->xclient.data.l[0] == (long) wm_delete_window) {
		if (w == app) {
			XtRemoveTimeOut(update_timer);
			exit(0);
		}
	}
}

static void add_wm_handler(Widget w)
{
	XtActionsRec wm_actions[] = {
		{"WMProtocols", WMProtocols }
	};

	XtAppAddActions(XtWidgetToApplicationContext(w), wm_actions, XtNumber(wm_actions));

	XtOverrideTranslations(w, XtParseTranslationTable("<Message>WM_PROTOCOLS: WMProtocols()"));

	wm_protocols     = XInternAtom(XtDisplay(w), "WM_PROTOCOLS", False);
	wm_delete_window = XInternAtom(XtDisplay(w), "WM_DELETE_WINDOW", False);

	XSetWMProtocols(XtDisplay(w), XtWindow(w), &wm_delete_window, 1);
}

/* Timer callback */
void update_status(XtPointer client_data, UNUSED XtIntervalId *timer)
{
	XtAppContext ctx = (XtAppContext) client_data;

	applet_status = net_monitor_interface(appRes.name, appRes.wlan);

	applet_set_icon();

	/* Rearm timer */
	update_timer = XtAppAddTimeOut(ctx, INTERVAL_MS, update_status, (XtPointer) ctx);
}

/**
 * Print the command-line options for this application.
 *
 */
static void usage(UNUSED int argc, char **argv)
{
	fprintf(stderr, "%s v. %s\n", APPLET_NAME, APPLET_VERSION);
	fprintf(stderr, "Usage: %s [options]\n\n"
		"Options:\n"
		"  -help   Show this help screen\n"
		"  -debug  Show debug messages\n"
		"  -dpath  Override default pixmap path\n"
		"  -wlan   Wlan interface name\n"
		"  -name   Cable interface name (default eth0)\n"
		, argv[0]);
}

int main(int argc, char **argv)
{
	XtAppContext ctx;

	/* command line options specification */
	static XrmOptionDescRec app_cmdline[] = {
		{ "-debug", "*debug", XrmoptionNoArg, "True" },
		{ "-help", "*help", XrmoptionNoArg, "True" },
		{ "-dpath", "*dpath", XrmoptionSepArg, 0 },
		{ "-wlan",  "*wlan", XrmoptionSepArg, 0 },
		{ "-name",  "*name", XrmoptionSepArg, 0 }
	};

	app = XtAppInitialize(&ctx, APPLET_NAME,
				app_cmdline, XtNumber(app_cmdline),
				&argc, argv,
				fallback_res, NULL, 0);

	/* Load application resources */
	XtGetApplicationResources(app, (XtPointer) &appRes, appResourcesSpec, XtNumber(appResourcesSpec), NULL, 0);

	if (appRes.help) {
		usage(argc, argv);
		return -1;
	}

	if (appRes.debug) {
		fprintf(stderr, "%s - %s dpath:%s\n", APPLET_NAME, __func__, appRes.dpath);
		fprintf(stderr, "%s - %s if_name: %s wlan:%d\n", APPLET_NAME, __func__, appRes.name, appRes.wlan);
	}

	applet_status = net_monitor_interface(appRes.name, appRes.wlan);

	applet_create(app);

	XtRealizeWidget(app);

	/* Handle WM window messages */
	add_wm_handler(app);

	/* Setup timer for status update */
	update_timer = XtAppAddTimeOut(ctx, INTERVAL_MS, update_status, (XtPointer) ctx);

	XtAppMainLoop(ctx);

	XtRemoveTimeOut(update_timer);

	return 0;
}
