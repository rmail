/* Mail Notify Applet  */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <limits.h>
#include <fcntl.h>
#include <string.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/wait.h>

#include <X11/Intrinsic.h>
#include <X11/StringDefs.h>
#include <X11/Shell.h>

#include <X11/Xaw/Cardinals.h>
#include <X11/Xaw/Command.h>

#include <X11/Xaw/Scrollbar.h>

#include "iv_widget.h"

#include "applet.h"
#include "mail_applet.h"

#define APPLET_NAME	"Applet_Mail"
#define APPLET_VERSION	"1.00"

#define	PNG_MAIL_ERROR	"mail_error.png"
#define	PNG_MAIL_OLD	"mail_old.png"
#define	PNG_MAIL_NEW	"mail_new.png"

/* Current interface status */
static int applet_status = -1;

/* Window manager messages */
static Atom wm_protocols;
static Atom wm_delete_window;

/* Colours :
 *
 * #4682B4 steelblue
 * #9ACD32 Yellow green
 */
/* Fallback Resources */
static String fallback_res[] = {
	"*Applet.height: 32",

	"*Button.foreground: #4682B4",

	/* Defaults if not set */
	"*width: 32",
	"*resize: False",
	"*borderWidth: 0",
	"*borderColor: #4682B4",	/* steelblue */
	"*highlightThickness 0",
	"*background: black",

	NULL,
};

/* Application resources data structure */
typedef struct {
	Boolean   debug;	/* Enable/Disable debug */
	Boolean   help;		/* Show applet commandline help */
	String    dpath;	/* Data path (icons, ...) */
} AppResourcesRec;

static AppResourcesRec appRes;

/* application resources specification */
static XtResource appResourcesSpec[] = {
	{
		"debug", "Debug", XtRBoolean, sizeof(Boolean),
		XtOffsetOf(AppResourcesRec, debug),
		XtRImmediate, (XtPointer) False
	},{
		"help", "Help", XtRBoolean, sizeof(Boolean),
		XtOffsetOf(AppResourcesRec, help),
		XtRImmediate, (XtPointer) False
	},{
		/* Icons path */
		"dpath", "Dpath", XtRString, sizeof(String),
		XtOffsetOf(AppResourcesRec, dpath),
		XtRString, (XtPointer) ICON_DEFAULT_PATH
	}
};

static Widget app;
static Widget app_button;

static XImage *pix_mail_new;
static XImage *pix_mail_old;
static XImage *pix_mail_error;

static XImage *pix_applet_status;

/* Status update timer */
#define INTERVAL_MS 60000		/* Update every x ms */
static XtIntervalId update_timer;	/* Timer ID for state update */

void applet_set_icon(void)
{
	if (applet_status > 0)
		pix_applet_status = pix_mail_new;
	else if (applet_status == 0)
		pix_applet_status = pix_mail_old;
	else
		pix_applet_status = pix_mail_error;

	XtVaSetValues(app_button, XtNximage, pix_applet_status, XtNimageType, 0, NULL);
}

static unsigned int client_alive = 0;

static void run_mail_client(UNUSED Widget w, UNUSED XtPointer p1, UNUSED XtPointer p2)
{
	pid_t pid;
	int status;

	if (client_alive)
		return;

	pid = fork();
	switch (pid) {
	case -1:
		perror("fork()");
		break;
	case 0: /* in the child */
		status = execl("/usr/bin/kmail",
				"--nofork",
				"--caption", "eMail",
				"--geometry", "1530x1016-0-0",
				NULL);

		if (status != -1)
			client_alive = 1;
		break;

	default: /* in parent */
		if (waitpid(pid, &status, 0) < 0) {
			if (appRes.debug) {
				fprintf(stderr, "%s - %s - %s\n", APPLET_NAME, __func__, strerror(errno));
			}
		} else {
			if (WIFEXITED(status)) {
				client_alive = 0;
				/* Set pixmap */
				pix_applet_status = pix_mail_old;
				applet_set_icon();
			}
		}
		break;
	}
}

static void applet_create(Widget app)
{
	Display *d;
	Visual  *visual;

	int screen;

	/* Depth of the root window */
	int depth;

	/* Base path for icons [POSIX "PATH_MAX" defined in limits.h] */
	char path[PATH_MAX];

	d = XtDisplay(app);
	screen = DefaultScreen(d);
	depth  = DisplayPlanes(d, screen);
	visual = XDefaultVisual(d, screen);

	/* Create status pixmaps */
	snprintf(path, sizeof(path),"%s/%s", appRes.dpath, PNG_MAIL_ERROR);
	pix_mail_error = create_ximage_from_PNG(d, visual, depth, path, NULL);

	snprintf(path, sizeof(path),"%s/%s", appRes.dpath, PNG_MAIL_OLD);
	pix_mail_old = create_ximage_from_PNG(d, visual, depth, path, NULL);

	snprintf(path, sizeof(path),"%s/%s", appRes.dpath, PNG_MAIL_NEW);
	pix_mail_new = create_ximage_from_PNG(d, visual, depth, path, NULL);

	/* Set initial pixmap */
	pix_applet_status = pix_mail_error;

	app_button = XtVaCreateManagedWidget("IV", imageViewWidgetClass, app,
						XtNximage, pix_applet_status,
						XtNimageType, 0,
						NULL);
	/* Set handler for mouse button press on status icon */
	XtAddEventHandler(app_button, ButtonPressMask, FALSE,
			  (XtEventHandler) run_mail_client, (XtPointer) NULL);

	applet_set_icon();

	XtManageChild(app_button);
}

/**************************************************************************/
static void WMProtocols(Widget w, XEvent *ev, UNUSED String *params, UNUSED Cardinal *nparams)
{
	if (ev->type == ClientMessage  &&
	    ev->xclient.message_type == wm_protocols  &&
	    ev->xclient.data.l[0] == (long) wm_delete_window) {
		if (w == app) {
			XtRemoveTimeOut(update_timer);
			exit(0);
		}
	}
}

static void add_wm_handler(Widget w)
{
	XtActionsRec wm_actions[] = {
		{"WMProtocols", WMProtocols }
	};

	XtAppAddActions(XtWidgetToApplicationContext(w), wm_actions, XtNumber(wm_actions));

	XtOverrideTranslations(w, XtParseTranslationTable("<Message>WM_PROTOCOLS: WMProtocols()"));

	wm_protocols = XInternAtom(XtDisplay(w), "WM_PROTOCOLS", False);
	wm_delete_window = XInternAtom(XtDisplay(w), "WM_DELETE_WINDOW", False);

	XSetWMProtocols(XtDisplay(w), XtWindow(w), &wm_delete_window, 1);
}

/* Timer callback */
void update_status(XtPointer client_data, UNUSED XtIntervalId *timer)
{
	XtAppContext ctx = (XtAppContext) client_data;

	applet_status = applet_get_status();

	applet_set_icon();

	/* Rearm timer */
	update_timer = XtAppAddTimeOut(ctx, INTERVAL_MS, update_status, (XtPointer) ctx);
}

/**
 * Print the command-line options for this application.
 *
 * @param[in] argc	Standard argument count
 * @param[in] argv	Standard argument array
 */
static void usage(UNUSED int argc, char **argv)
{
	fprintf(stderr, "%s v. %s\n", APPLET_NAME, APPLET_VERSION);
	fprintf(stderr, "Usage: %s [options]\n\n"
		"Options:\n"
		"  -help   Show this help screen\n"
		"  -debug  Show debug messages\n"
		"  -dpath  Override default pixmap path\n"
		, argv[0]);
}

int main(int argc, char **argv)
{
	XtAppContext ctx;

	/* Command line options specification */
	static XrmOptionDescRec app_cmdline[] = {
		{ "-debug", "*debug", XrmoptionNoArg, "True" },
		{ "-help", "*help", XrmoptionNoArg, "True" },
		{ "-dpath", "*dpath", XrmoptionSepArg, 0 }
	};

	app = XtAppInitialize(&ctx, APPLET_NAME,
				app_cmdline, XtNumber(app_cmdline),
				&argc, argv, fallback_res, NULL, 0);

	/* Load application resources */
	XtGetApplicationResources(app, (XtPointer) &appRes, appResourcesSpec, XtNumber(appResourcesSpec), NULL, 0);

	if (appRes.help) {
		usage(argc, argv);
		return -1;
	}

	if (appRes.debug) {
		fprintf(stderr, "%s - %s dpath:%s\n", APPLET_NAME, __func__, appRes.dpath);
	}

	/* Get initial state before create applet widgets */
	applet_status = applet_get_status();

	applet_create(app);

	XtRealizeWidget(app);

	/* Handle WM window messages */
	add_wm_handler(app);

	/* Setup timer for status update */
	update_timer = XtAppAddTimeOut(ctx, INTERVAL_MS, update_status, (XtPointer) ctx);

	XtAppMainLoop(ctx);

	XtRemoveTimeOut(update_timer);

	return 0;
}
