/* An example of how to create a applet popup dialog box.  */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <limits.h>
#include <fcntl.h>
#include <string.h>
#include <errno.h>
#include <sys/ioctl.h>

#include <X11/Intrinsic.h>
#include <X11/StringDefs.h>
#include <X11/Shell.h>

#include <X11/Xaw/Box.h>
#include <X11/Xaw/Cardinals.h>
#include <X11/Xaw/Command.h>

#include <X11/Xaw/Toggle.h>

#include <X11/xpm.h>

#include "iv_widget.h"

#include "applet.h"

#define APPLET_NAME	"Applet_Disks"
#define APPLET_VERSION	"1.00"

#define PNG_DISKS	"disks.png"

/* Window manager messages */
static Atom wm_protocols;
static Atom wm_delete_window;

static Boolean usb_state;
static Boolean mmc_state;
static Boolean dvd_state;

#if 0
static Pixmap usb_pix;
static Pixmap mmc_pix;
static Pixmap dvd_pix;

/* XPM */
static char * usb_xpm[] = {
"32 32 5 1",
"	c None",
".	c #000000",
"+	c #7F7F7F",
"@	c #202020",
"#	c #1C1C1C",
"                                ",
"                                ",
"                                ",
"                                ",
"                                ",
"                                ",
"                                ",
"                                ",
"                                ",
"                  ...           ",
"                 .....          ",
"              ........          ",
"            ..   .....          ",
" +@#+      .      ...           ",
"+....+    .                 +   ",
"......  .+                  ..+ ",
"................................",
"......       .+             ..+ ",
"+....+         .            +   ",
" +..+           .     ++++      ",
"                 ..   ....      ",
"                   .......      ",
"                      ....      ",
"                      ++++      ",
"                                ",
"                                ",
"                                ",
"                                ",
"                                ",
"                                ",
"                                ",
"                                "};
#endif

/* Colours :
 *
 * #4682B4 steelblue
 * #9ACD32 Yellow green
 */
/* Fallback Resources */
static String fallback_res[] = {
	"*Applet_Disk.width: 32",
	"*Applet_Disk.height: 32",

	"*Toggle.width: 32",
	"*Toggle.height: 24",
	"*Toggle.foreground: #9ACD32",
	"*Toggle.borderWidth: 1",
	"*Toggle.thickness: 1",

	"*DiskPopup.foreground: #4682B4",

	"*PopupBox.foreground: #4682B4",

	"*Button.foreground: #4682B4",

	/* Defaults if not set */
	"*width: 32",
	"*resize: False",
	"*borderWidth: 0",
	"*borderColor: #4682B4",	/* steelblue */
	"*highlightThickness 0",
	"*background: black",

	NULL,
};

/* Application resources data structure */
typedef struct {
	Boolean   debug;	/* Enable/Disable debug */
	Boolean   help;		/* Show applet commandline help */
	String    dpath;	/* Data path (icons, ...) */
} AppResourcesRec;

static AppResourcesRec appRes;

/* Application resources specification */
static XtResource appResourcesSpec[] = {
	{
		"debug", "Debug", XtRBoolean, sizeof(Boolean),
		XtOffsetOf(AppResourcesRec, debug),
		XtRImmediate, (XtPointer) False
	},{
		"help", "Help", XtRBoolean, sizeof(Boolean),
		XtOffsetOf(AppResourcesRec, help),
		XtRImmediate, (XtPointer) False
	},{
		/* Icons path */
		"dpath", "Dpath", XtRString, sizeof(String),
		XtOffsetOf(AppResourcesRec, dpath),
		XtRString, (XtPointer) ICON_DEFAULT_PATH
	}
};

static Widget app;
static Widget app_button;
static Widget box;		/* Popup box */

static XImage *applet_pix;

static int PopUpPresent = 0;

/* destroy_popup: Destroys the popup box widget.
 * Arguments:
 *	w - *** UNUSED ***.
 *	box - the box widget.  This widget is a direct child of the popup shell to destroy.
 *	p - *** UNUSED **.
 */
static void destroy_popup(UNUSED Widget w, XtPointer box, UNUSED XtPointer p)
{
	Widget popup = XtParent((Widget) box);

	XtDestroyWidget(popup);
	PopUpPresent = 0;
}

static void toggle_callback(UNUSED Widget w, XtPointer user, XtPointer state)
{
	/* xxx_state = state */
	*(Boolean *) user = (state) ? True : False;

#if 0
	printf("%s state:%p user:%p usb_state addr: %p\n", __func__, state, user, &usb_state);
#endif
}

static void create_popup(Widget w, UNUSED XtPointer p1, UNUSED XtPointer p2)
{
	Widget popup;
	Widget usb_toggle;
	Widget mmc_toggle;
	Widget dvd_toggle;

	Arg args[8];
	Cardinal n;

	/* x,y,h of the caller widget */
	Position wx;
	Position wy;
	Dimension wh;
	Dimension ww;

	if (PopUpPresent) {
		destroy_popup(w, (XtPointer) box, NULL);
		return;
	}

	/* Get caller widget geometry */
	n = 0;
	XtSetArg(args[n], XtNwidth, &ww);
	n++;
	XtSetArg(args[n], XtNheight, &wh);
	n++;
	XtSetArg(args[n], XtNx, &wx);
	n++;
	XtSetArg(args[n], XtNy, &wy);
	n++;
	XtGetValues(w, args, n);

	/* We need only they */
	XtTranslateCoords(w, 0, (Position) wh, &wx, &wy);

	n = 0;
	XtSetArg(args[n], XtNx, wx);
	n++;

	if (wy > wh + 120) {
		XtSetArg(args[n], XtNy, wy - wh - 100);
	} else {
		XtSetArg(args[n], XtNy, wy);
	}
	n++;
	XtSetArg(args[n], XtNwidth, ww + 10);
	n++;

	popup = XtCreatePopupShell("DisksPopup", transientShellWidgetClass, w, args, n);

	/* PopupBox contains Toggles */
	box = XtCreateManagedWidget("PopupBox", boxWidgetClass, popup, NULL, 0);

#if 0
	printf("%s usb_state:%d\n", __func__, usb_state);
	printf("%s mmc_state:%d\n", __func__, mmc_state);
	printf("%s dvd_state:%d\n", __func__, dvd_state);
#endif

	/* Toggles */
	usb_toggle = XtVaCreateManagedWidget("ToggleUSB", toggleWidgetClass, box,
					 XtNlabel, "USB",
					 /*XtNbackgroundPixmap, usb_pix,*/
					 XtNstate, usb_state,
					 NULL);

	mmc_toggle = XtVaCreateManagedWidget("ToggleMMC", toggleWidgetClass, box,
					 XtNlabel, "MMC",
					 XtNstate, mmc_state,
					 NULL);

	dvd_toggle = XtVaCreateManagedWidget("ToggleDVD", toggleWidgetClass, box,
					 XtNlabel, "DVD",
					 XtNstate, dvd_state,
					 NULL);

	XtAddCallback(usb_toggle, XtNcallback, toggle_callback, (XtPointer) &usb_state);
	XtAddCallback(mmc_toggle, XtNcallback, toggle_callback, (XtPointer) &mmc_state);
	XtAddCallback(dvd_toggle, XtNcallback, toggle_callback, (XtPointer) &dvd_state);

	/* Popup widget */
	XtPopup(popup, XtGrabNone);
	PopUpPresent = 1;
}

static void applet_create(Widget app)
{
	Display *d;
	Visual  *visual;

	int screen;

	/* Depth of the root window */
	int depth;

	/* Base path for icons [POSIX "PATH_MAX" defined in limits.h] */
	char path[PATH_MAX];


#if 0
	/* XPM stuff */
	XpmColorSymbol transparent;
	XpmAttributes  attrib;
	Pixel bg;
#endif

	d = XtDisplay(app);

	screen = DefaultScreen(d);
	depth  = DisplayPlanes(d, screen);
	visual = XDefaultVisual(d, screen);

	/* Create status pixmaps */
	snprintf(path, sizeof(path),"%s/%s", appRes.dpath, PNG_DISKS);
	applet_pix = create_ximage_from_PNG(d, visual, depth, path, NULL);

#if 0
	XtVaGetValues(app, XtNbackground, &bg, NULL);
	transparent.name = NULL;
	transparent.value = "#FFFFFFFFFFFF";
	transparent.pixel = bg;
	attrib.colorsymbols = &transparent;
	attrib.valuemask = XpmColorSymbols;
	attrib.numsymbols = 1;

	XpmCreatePixmapFromData(d, DefaultRootWindow(d), usb_xpm, &usb_pix, NULL, NULL /* &attrib*/);
#endif

	/* Set pixmap */
	app_button = XtVaCreateManagedWidget("IV", imageViewWidgetClass, app,
					     XtNximage,    applet_pix,
					     XtNimageType, 0,
					     NULL);

	/* Set handler for mouse button press on status icon */
	XtAddEventHandler(app_button, ButtonPressMask, FALSE,
			  (XtEventHandler) create_popup, (XtPointer) NULL);

	XtManageChild(app_button);
}

/**************************************************************************/
static void WMProtocols(Widget w, XEvent *ev, UNUSED String *params, UNUSED Cardinal *nparams)
{
	if (ev->type == ClientMessage  &&
	    ev->xclient.message_type == wm_protocols  &&
	    ev->xclient.data.l[0] == (long) wm_delete_window) {
		if (w == app) {
			exit(0);
		}
	}
}

static void add_wm_handler(Widget w)
{
	XtActionsRec wm_actions[] = {
		{"WMProtocols", WMProtocols }
	};

	XtAppAddActions(XtWidgetToApplicationContext(w), wm_actions, XtNumber(wm_actions));

	XtOverrideTranslations(w, XtParseTranslationTable("<Message>WM_PROTOCOLS: WMProtocols()"));

	wm_protocols = XInternAtom(XtDisplay(w), "WM_PROTOCOLS", False);
	wm_delete_window = XInternAtom(XtDisplay(w), "WM_DELETE_WINDOW", False);

	XSetWMProtocols(XtDisplay(w), XtWindow(w), &wm_delete_window, 1);
}

/**
 * Print the command-line options for this application.
 *
 * @param[in] argc	Standard argument count
 * @param[in] argv	Standard argument array
 *
 */
static void usage(UNUSED int argc, char **argv)
{
	fprintf(stderr, "%s v. %s\n", APPLET_NAME, APPLET_VERSION);
	fprintf(stderr, "Usage: %s [options]\n\n"
		"Options:\n"
		"  -help   Show this help screen\n"
		"  -debug  Show debug messages\n"
		"  -dpath  Override default pixmap path\n"
		, argv[0]);
}

int main(int argc, char **argv)
{
	XtAppContext ctx;

	/* Command line options specification */
	static XrmOptionDescRec app_cmdline[] = {
		{ "-debug", "*debug", XrmoptionNoArg, "True" },
		{ "-help", "*help", XrmoptionNoArg, "True" },
		{ "-dpath", "*dpath", XrmoptionSepArg, 0 }
	};

	app = XtAppInitialize(&ctx, APPLET_NAME,
			      app_cmdline, XtNumber(app_cmdline),
			      &argc, argv,
			      fallback_res, NULL, 0);

	/* Load application resources */
	XtGetApplicationResources(app, (XtPointer) &appRes, appResourcesSpec, XtNumber(appResourcesSpec), NULL, 0);

	if (appRes.help) {
		usage(argc, argv);
		return -1;
	}

	if (appRes.debug) {
		fprintf(stderr, "%s - %s dpath:%s\n", APPLET_NAME, __func__, appRes.dpath);
	}

	/* Set initial state before create applet widgets */
	usb_state = False;
	mmc_state = False;
	dvd_state = False;

	applet_create(app);

	XtRealizeWidget(app);

	/* Tell Window System we're willing to handle window messages */
	add_wm_handler(app);

	XtAppMainLoop(ctx);

	return 0;
}
