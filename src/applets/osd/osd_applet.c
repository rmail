#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <unistd.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xatom.h>
#include <X11/Xft/Xft.h>

#include "osd.h"

static void usage(const char *appname)
{
	fprintf(stderr, "%s -t <title> -b <body> -i <icon>\n", appname);
}

int main(int argc, char **argv)
{
	struct notify_object no;

	Display *dpy;

	XEvent e;

	int running = 1;
	int opt;

	char *title = NULL;
	char *body = NULL;
	char *icon = NULL;

	while ((opt = getopt(argc, argv, "b:i:t:")) != -1) {
		switch (opt) {
		case 'b' :
			body = optarg;
			break;
		case 'i':
			icon = optarg;
			break;
		case 't':
			title = optarg;
			break;
		default:
			fprintf(stderr, "Unknown parameter.\n");
			usage(argv[0]);
			return -1;
                }
        }

	/* open connection with the server */
	dpy = XOpenDisplay(NULL);
	if (dpy == NULL) {
		fprintf(stderr, "Cannot open display\n");
		return -1;
	}

	osd_init(dpy, &no, title, body, icon);

	/* Create OSD object */
	if (osd_new_notify_object(argc, argv, &no)) {
		XCloseDisplay(no.frame.display);
		return -1;
	}

	/* Select kind of events we are interested in */
	XSelectInput(no.frame.display, no.frame.win,
			ExposureMask | KeyPressMask | ButtonPressMask);

	/* Map (show) the window */
	XMapWindow(no.frame.display, no.frame.win);

	/* event loop */
	while (running) {
		XNextEvent(no.frame.display, &e);

		switch (e.type) {
		case Expose:
			/* Draw or redraw the window */
			osd_show_notify_object(&no);
			break;

		/* Exit on key press or mouse button press */
		case KeyPress:
		case ButtonPress:
			running = 0;
			break;
		}
	}

	osd_delete_notify_object(&no);

	/* Close connection to server */
	XCloseDisplay(no.frame.display);

	return 0;
}
