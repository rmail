#ifndef OSD_H
#define OSD_H

/*
 * Bubble appearance and layout
 *
 * Regardless of type, a bubble should appear as a rectangle of
 * color #131313 (regardless of theme) with opacity 80%, corner
 * roundness 0.375 em, and a drop shadow of #000000 color and 0.5 em spread.
 * The bubble should blur whatever is behind it with a Gaussian blur
 * of 0.125 em.
 *
 * A bubble should be 24 ems wide (not including the drop shadow).
 * (<RB> 24 em, fontsize 16 --> 384 pixels.)
 *
 *
 * Its height depends on the contents, but should be a minimum of 5 ems, and a
 * maximum just tall enough to display 10 lines of body text (which means the
 * maximum height of a bubble with a title is greater than of a bubble without
 * a title).
 *
 * (<RB> 5 em, fontsize 16 --> 80 pixels.)
 *
 * Furthermore, the bottom limit is 6 ems from the top of the bottom
 * panel, if there is one, or otherwise 6 ems from the bottom of the screen
 * (as if there was a 5-em bubble at the bottom plus 0.5 em margin on each
 * side). Regardless of font and screen size, a bubble must not be so tall that
 * it extends beyond the bottom limit.
 *
 * Bubbles feature an icon, and/or some text or a gauge.
 * All text elements should have a drop shadow of #000000 color and 0.125 em
 * spread.
 */

/* Extract RGB component for XRenderColor */
#define COLOR_R(rgb)	(((rgb) & 0x00FF0000) >> 8)
#define COLOR_G(rgb)	((rgb) & 0x0000FF00)
#define COLOR_B(rgb)	(((rgb) & 0x000000FF) << 8)
#define COLOR_A(rgb)	0xFFFF

#define BUBBLE_COLOR	0xFF001300UL
/* XXX Not working ??? */
/*#define BUBBLE_COLOR	0x13131300UL	*/

/* Text layout
 *
 * If there is text, it consists of a title and/or a body. Both of these
 * should use the standard application typeface, aligned to the leading side,
 * and should wrap to multiple lines if necessary. If a word is wider than the
 * line, it should wrap following usual Pango rules for the current interface
 * language.
 *
 * Both title and body text should appear to the trailing side of the icon.
 * If there is no body text, the title text should be vertically centered with
 * the center of the icon. If there is body text, the title text should be
 * top-aligned with the top of the icon, and the body text should begin
 * immediately underneath. This applies even if body text is added to a bubble
 * that previously had only a title: the title text should move from its
 * centered alignment to top alignment as the body text appears.
 *
 */

/*
 * Title text
 *
 * The title should be the standard application font size,
 * of color #ffffff, and bold weight.
 *
 * In the title text, any string of one or more consecutive whitespace
 * characters (U+0020 SPACE, U+0009 CHARACTER TABULATION, U+000A LINE FEED (LF),
 * U+000C FORM FEED (FF), or U+000D CARRIAGE RETURN (CR)), even if a mixture of
 * those, should be treated as a single space. Leading and trailing whitespace
 * should not be presented.
 *
 * If the title would take up longer than three lines, it should be truncated
 * at just enough characters to fit an ellipsis character ... at the end of
 * the third line.
 */

#define TITLE_FONT_SIZE  16		/* Standard font size (1 em) */
#define TITLE_MAX_LINES  3
#define TITLE_COLOR      0xFFFFFF

/* Body text
 *
 * The body should be 0.9xthe standard application font size, of color #EAEAEA,
 * standard weight, and should not contain any extra formatting.
 *
 * In the body text, any string of one or more consecutive whitespace characters
 * that contains at least one newline (U+000D CARRIAGE RETURN (CR),
 * U+000A LINE FEED (LF), or U+000D CARRIAGE RETURN (CR) immediately followed
 * by U+000A LINE FEED (LF)), even if a mixture of those, should be treated
 * as a single newline.
 * Then for each line in the text, any string of one or more consecutive
 * (non-newline) whitespace characters, even if a mixture of them, should be
 * treated as a single space, and leading and trailing whitespace should not
 * be presented.
 *
 */

#define TEXT_FONT_SIZE	14		/* 0.9 * Standard font size	*/
#define TEXT_MAX_LINES	10		/*				*/
#define TEXT_COLOR	0xEAEAEA	/*				*/

/* Icon
 *
 * Any icon should be scaled so that its largest dimension is 3 ems, while
 * retaining the icon's original aspect ratio. This means that vector and
 * bitmap icons should be simple, without fine detail or thin strokes, and
 * all icons should be square or nearly square whenever possible.
 * If the icon is a bitmap, Sinc/Lanczos scaling should be used to minimize
 * jagginess.
 *
 * If the bubble has no text, the icon should be horizontally centered within
 * the window. Otherwise, the icon should be horizontally and vertically
 * centered within a 3-em square that is flush with the leading side, with
 * 1 em margin to the trailing side.
 *
 * Icons representing alerts, warnings or errors should use an x or ? symbol
 * with smooth gradient ranging from light red (#ff6363) at the top to dark
 * red (#cc0000) at the bottom. Other than that, all icons and gauges should
 * use one color theme: a smooth gradient ranging from light gray (#ececec)
 * at the top to medium gray (#9b9b9b) at the bottom. Every icon should also
 * use a black 2px outline with 30% opacity. There should also be a 1px white
 * highlight with 40% opacity on the top and left edges to improve the
 * definition. Different opacity values of this scheme (excluding the black
 * outline) are allowed, for example to depict an inactive/disabled state or
 * different levels of intensity (e.g. brightness). All icons and gauges
 * should use gently rounded corners with the recommended roundness of
 * 0.1 em radius and the roundness should not exceed 0.25 em radius.
 */

#define WIN_X_MARGIN	16	/* 1 em */
#define WIN_Y_MARGIN	16	/* 1 em */

/* Max widht 24 em */
#define WIN_MAX_WIDTH	(24 * TITLE_FONT_SIZE)

/* Min height 5 em */
#define WIN_MIN_HEIGHT	(5 * TITLE_FONT_SIZE)

/* Max icon widht 3 em */
#define ICON_MAX_WIDTH	(3 * TITLE_FONT_SIZE)

struct notify_text {
	char       *data;
	XftFont    *xft_font;
	XftColor   xft_colour;
	XGlyphInfo extents;
};

struct notify_icon {
	char      *name;
	XImage    *ximage;
	GC        gc;
};

struct notify_frame {
	Display *display;
	Window  win;

	int screen;		/* Screen number */
	int depth;		/* Color depth */

	Colormap cmap;		/* Current Colormap */
	Visual *visual;		/* Current visual */

	/* OSD window position & dimension */
	int width;
	int height;
	int x;
	int y;
};

struct notify_object {
	struct notify_frame	frame;
	struct notify_text	title;
	struct notify_text	body;
	struct notify_icon	icon;

	XftDraw *draw;	/* Drawable for text objects */
};

int osd_new_notify_object(int argc, char **argv, struct notify_object *no);

void osd_delete_notify_object(struct notify_object *no);

void osd_show_notify_object(struct notify_object *no);

#if 0
int osd_new_frame(int argc, char **argv, struct notify_object *no);
#endif

void osd_init(Display *dpy, struct notify_object *no, char *title, char *body, char *icon);

#endif	/* OSD_H */
