#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xatom.h>
#include <X11/Xft/Xft.h>

#include "osd.h"

extern XImage *create_ximage_from_PNG(Display *d, Visual *v, int depth, const char *filename, XColor *bg);

int osd_new_frame(int argc, char **argv, struct notify_object *no)
{
	int dpy_width;
	int dpy_height;

	XSizeHints *xsh;
	XWMHints   *xwmh;

	XSetWindowAttributes xwinattrs;

	double transparency;
	unsigned int opacity;
	Atom wm_opacity;

	Atom wm_state;	/* WM state */
	Atom wms[3];

	int desktop;
	Atom wm_desktop;

	if (!no)
		return -1;

	dpy_width  = DisplayWidth(no->frame.display, no->frame.screen);
	dpy_height = DisplayHeight(no->frame.display, no->frame.screen);

#ifdef OSD_DEBUG
	fprintf(stderr, "w:%d h:%d\n", dpy_width, dpy_height);
#endif

	/* create window */
	/* set override-redirect to true, so the window will not be decorated
	 * if you want the window decorated the last two parameters of
	 * XCreateWindow can be: 0, NULL
	 */
	xwinattrs.override_redirect = 1;
	xwinattrs.background_pixel = BUBBLE_COLOR;

	no->frame.win = XCreateWindow(no->frame.display,
					DefaultRootWindow(no->frame.display),
					dpy_width - no->frame.width, 0,
					no->frame.width, no->frame.height,
					0, CopyFromParent, InputOutput, CopyFromParent,
					CWOverrideRedirect | CWBackPixel, &xwinattrs);
/*					0, NULL); */

	/* Adjust opacity */
	transparency = 0.8;
	opacity = (unsigned int) (0xffffffff * transparency);
	wm_opacity = XInternAtom(no->frame.display,
				 "_NET_WM_WINDOW_OPACITY", False);

	XChangeProperty(no->frame.display, no->frame.win,
			wm_opacity, XA_CARDINAL, 32,
			PropModeReplace, (unsigned char *) &opacity, 1);

#ifdef OSD_DEBUG
	XSync(no->frame.display, False);
	fprintf(stderr, "%s - _NET_WM_WINDOW_OPACITY\n", __func__);
#endif

	/* Make window appear on all desktops */
	desktop = 0xffffffff;
	wm_desktop = XInternAtom(no->frame.display, "_NET_WM_DESKTOP", False);
	XChangeProperty(no->frame.display, no->frame.win, wm_desktop,
			XA_CARDINAL, 32, PropModeReplace,
			(unsigned char *) &desktop, 1);


#ifdef OSD_DEBUG
	XSync(no->frame.display, False);
	fprintf(stderr, "%s - _NET_WM_DESKTOP\n", __func__);
#endif

	wm_state = XInternAtom(no->frame.display, "_NET_WM_STATE", False);

	/* Stick on viewpoint scroll */
	wms[0] = XInternAtom(no->frame.display, "_NET_WM_STATE_STICKY", False);

	/* Above all other windows */
	wms[1] = XInternAtom(no->frame.display, "_NET_WM_STATE_ABOVE", True);

	/* Not on taskbar */
	wms[2] = XInternAtom(no->frame.display, "_NET_WM_STATE_SKIP_TASKBAR", True);

	XChangeProperty(no->frame.display, no->frame.win, wm_state, XA_ATOM, 32,
			PropModeReplace, (unsigned char *) wms, 3);

#ifdef OSD_DEBUG
	XSync(no->frame.display, False);
	fprintf(stderr, "%s - _NET_WM_STATE\n", __func__);
#endif

	XSetTransientForHint(no->frame.display,
			     DefaultRootWindow(no->frame.display),
			     no->frame.win);

	/* Setup size and window manager hints */
	xsh = XAllocSizeHints();
	xwmh = XAllocWMHints();

	if ((xsh != NULL) && (xwmh != NULL)) {
		xsh->flags = (PMinSize | PMaxSize | PBaseSize | PWinGravity);

		xsh->win_gravity = NorthEastGravity;

		xsh->min_width = WIN_MAX_WIDTH; /* XXX */
		xsh->min_height = WIN_MIN_HEIGHT;

		xsh->max_width = WIN_MAX_WIDTH;
		xsh->max_height = WIN_MIN_HEIGHT; /* XXX */

		xsh->base_width  = (no->frame.width  > WIN_MAX_WIDTH)  ? WIN_MAX_WIDTH  : no->frame.width;
		xsh->base_height = (no->frame.height < WIN_MIN_HEIGHT) ? WIN_MIN_HEIGHT : no->frame.height;

		/* The UrgencyHint flag, if set in the flags field, indicates
		 * that the client deems the window contents to be urgent,
		 * requiring the timely response of the user.
		 * The window manager will make some effort to draw the user's
		 * attention to this window while this flag is set. The client
		 * must provide some means by which the user can cause the
		 * urgency flag to be cleared (either mitigating the condition
		 * that made the window urgent or merely shutting off the alarm)
		 * or the window to be withdrawn.
		 */
		xwmh->flags = (InputHint | StateHint /* | UrgencyHint*/);
		xwmh->input = False;
		xwmh->initial_state = NormalState;

		XSetWMProperties(no->frame.display, no->frame.win, NULL, NULL,
				 argv, argc, xsh, xwmh, NULL);

		XFree(xsh);
		XFree(xwmh);
	}
	return 0;
}

static void osd_truncate_string(Display *display, XftFont *font, char *string, int max_width)
{
	XGlyphInfo gi;

	unsigned int i;
	unsigned int j;
	char *p;

	p = string;

	i = 0;
	while (p[i] != '\0') {
		XftTextExtents8(display, font, (FcChar8 *) string, i + 1, &gi);
		if (gi.width > max_width) {
			p[i] = '\0';

			/* ellipsis... */
			for (j = 1; j < 4; j++) {
				if ((i - j) > 0) {
					p[i-j] = '.';
				}
			}
			break;
		}
#ifdef OSD_DEBUG
		for (j = 0; j <= i; j++)
			fprintf(stderr, "%c", p[j]);
		fprintf(stderr, " w:%d\n", gi.width);
#endif
		i++;
	}
}

/* Clip title and body texts */
static void osd_clip_text(struct notify_object *no, int max_width)
{
	struct notify_text *nt;

	nt = &no->title;
	if (nt->extents.width > max_width) {
		osd_truncate_string(no->frame.display, nt->xft_font, nt->data, max_width);
	}

	nt = &no->body;
	if (nt->extents.width > max_width) {
		osd_truncate_string(no->frame.display, nt->xft_font, nt->data, max_width);
	}
}

static void osd_dimension(struct notify_object *no)
{
	struct notify_icon *nc;
	int width = 0;

#ifdef OSD_DEBUG
	fprintf(stderr, "%s - TITLE ex:%d ey:%d ew:%d eh:%d\n", __func__,
			no->title.extents.x, no->title.extents.y,
			no->title.extents.width, no->title.extents.height);

	fprintf(stderr, "%s - BODY ex:%d ey:%d ew:%d eh:%d\n", __func__,
			no->body.extents.x, no->body.extents.y,
			no->body.extents.width, no->body.extents.height);
#endif

	/*** Calc window dimesions ***/

	/* Default left and rigth margins */
	no->frame.width  = 2 * WIN_X_MARGIN;
	no->frame.height = 2 * WIN_Y_MARGIN;

	nc = &no->icon;
	if (nc->ximage) {
		/* Add icon width and height */
		no->frame.width  += nc->ximage->width;
		no->frame.height += nc->ximage->height;

#ifdef OSD_DEBUG
		fprintf(stderr, "%s - ICON w:%d h:%d\n", __func__,
			nc->ximage->width, nc->ximage->height);
#endif
	}

	/* Add text height */
	if (no->body.extents.height > 0) {
		no->frame.height += no->body.extents.height;
	}

	if (no->title.extents.height > 0) {
		no->frame.height += no->title.extents.height;
	}

	/* Add longest text width */
	if ((no->body.extents.width > 0) || (no->title.extents.width > 0)) {
		if (nc->ximage) {
			/* Add icon to text margin */
			no->frame.width  += WIN_X_MARGIN;
		}

		/* Save width for clipping */
		width = no->frame.width;

		if (no->body.extents.width > no->title.extents.width) {
			no->frame.width += no->body.extents.width;
		} else {
			no->frame.width += no->title.extents.width;
		}
	}

#ifdef OSD_DEBUG
	fprintf(stderr, "%s - FRAME (request) ew:%d eh:%d\n", __func__,
			no->frame.width, no->frame.height);
#endif

	/*** Check for oversized frame ***/
	if (no->frame.width > WIN_MAX_WIDTH) {
		no->frame.width = WIN_MAX_WIDTH;
		osd_clip_text(no, WIN_MAX_WIDTH - width);
	}
	if (no->frame.height > WIN_MIN_HEIGHT) {
		no->frame.height = WIN_MIN_HEIGHT;
	}

#ifdef OSD_DEBUG
	fprintf(stderr, "%s - FRAME (real) ew:%d eh:%d\n", __func__,
			no->frame.width, no->frame.height);
#endif
}

static void osd_new_title(struct notify_object *no)
{
	struct notify_text *nt;

	XRenderColor title_colour = {
		COLOR_R(TITLE_COLOR),
		COLOR_G(TITLE_COLOR),
		COLOR_B(TITLE_COLOR),
		COLOR_A(TITLE_COLOR)
	};

	nt = &no->title;

	if (!nt->data) {
		nt->xft_font = NULL;
		return;
	}

	/* Standard font size */
	nt->xft_font = XftFontOpen(no->frame.display, no->frame.screen,
			XFT_FAMILY,    XftTypeString,  "dejavu",
			XFT_SIZE,      XftTypeDouble,  16.0,
			XFT_WEIGHT,    XftTypeInteger, XFT_WEIGHT_BOLD,
			XFT_ANTIALIAS, XftTypeBool,    True,
			NULL);

	if (!nt->xft_font) {
		fprintf(stderr, "%s cannot open font!\n", __func__);
		return;
	}

	/* Get text width & height */
	XftTextExtents8(no->frame.display, nt->xft_font, (FcChar8 *) nt->data,
			strlen(nt->data), &nt->extents);

	XftColorAllocValue(no->frame.display, no->frame.visual, no->frame.cmap,
				&title_colour,
				&nt->xft_colour);
}

static void osd_new_body(struct notify_object *no)
{
	struct notify_text *nt;

	XRenderColor text_colour = {
		COLOR_R(TEXT_COLOR),
		COLOR_G(TEXT_COLOR),
		COLOR_B(TEXT_COLOR),
		COLOR_A(TEXT_COLOR)
	};

	nt = &no->body;
	if (!nt->data) {
		fprintf(stderr, "%s NULL body!\n", __func__);
		nt->xft_font = NULL;
		return;
	}

	/* 0.9 standard font size */
	nt->xft_font = XftFontOpen(no->frame.display,
				   no->frame.screen,
				   XFT_FAMILY,    XftTypeString, "dejavu",
				   XFT_SIZE,      XftTypeDouble, 14.0,
				   XFT_ANTIALIAS, XftTypeBool,   True,
				   NULL);

	if (!nt->xft_font) {
		fprintf(stderr, "%s cannot open font!\n", __func__);
		return;
	}

	/* Get text width & height */
	XftTextExtents8(no->frame.display, nt->xft_font, (FcChar8 *) nt->data,
			strlen(nt->data), &nt->extents);

	XftColorAllocValue(no->frame.display, no->frame.visual, no->frame.cmap,
			   &text_colour, &nt->xft_colour);
}

static void osd_new_icon(struct notify_object *no)
{
	struct notify_icon *nc;

	if (!no)
		return;

	nc = &no->icon;

	/* Create icon object */
	if (nc->name) {
		nc->ximage = create_ximage_from_PNG(no->frame.display,
						 no->frame.visual,
						 no->frame.depth,
						 nc->name, NULL);

		if (!nc->ximage) {
			fprintf(stderr, "%s cannot create icon!\n", __func__);
		}
	}
}

int osd_new_notify_object(int argc, char **argv, struct notify_object *no)
{
	XGCValues gcvalues;

	if (no->title.data) {
		osd_new_title(no);
	}

	if (no->body.data) {
		osd_new_body(no);
	}

	osd_new_icon(no);

	/* Set frame dimensions */
	osd_dimension(no);

	/* Create frame */
	if (osd_new_frame(argc, argv, no)) {
		return -1;
	}

	/* Crate XFT draw context only if title or body font was opened */
	if (no->title.xft_font || no->body.xft_font) {
		no->draw = XftDrawCreate(no->frame.display, no->frame.win,
					 no->frame.visual, no->frame.cmap);
		if (!no->draw) {
			fprintf(stderr, "%s cannot create draw context!\n", __func__);
			return -1;
		}
	}

	/* Create icon GC */
	if (no->icon.ximage) {
		no->icon.gc = XCreateGC(no->frame.display, no->frame.win, 0, &gcvalues);
	}

	return 0;
}

void osd_delete_notify_object(struct notify_object *no)
{
	XImage *icon;

	if (!no)
		return;

	/* Clean up all image resources */
	if (no->icon.ximage) {
		icon = no->icon.ximage;
		if (icon->data) {
			free(icon->data);
			icon->data = (char *) NULL;  /*  instead of XDestroyImage() */
		}
		XDestroyImage(icon);
		icon = NULL;

		XFreeGC(no->frame.display, no->icon.gc);
	}

	if (no->draw) {
		XftDrawDestroy(no->draw);

		if (no->title.xft_font) {
			XftFontClose(no->frame.display, no->title.xft_font);
		}
		if (no->body.xft_font) {
			XftFontClose(no->frame.display, no->body.xft_font);
		}
	}
}

void osd_show_notify_object(struct notify_object *no)
{
	XImage *icon;

	/* Offsets in pixel to draw into the notification window */
	int x_offset = WIN_X_MARGIN;
	int y_offset = WIN_Y_MARGIN;

#if 0
	fprintf(stderr, "Icon x:%d y:%d\n", x_offset, y_offset);
#endif
	if (no->icon.ximage) {
		icon = no->icon.ximage;

		XPutImage(no->frame.display, no->frame.win, no->icon.gc,
			  icon, 0, 0, x_offset, y_offset,
			  icon->width, icon->height);

		/* Increment x_offset for text title and body
		 * (icon width + 1 em)
		 */
		x_offset += icon->width + WIN_X_MARGIN;
		y_offset += WIN_Y_MARGIN;
	}
#if 0
	fprintf(stderr, "Title x:%d y:%d\n", x_offset, y_offset);
#endif
	if (no->title.data && no->title.xft_font) {
		XftDrawStringUtf8(no->draw,
				&no->title.xft_colour,
				no->title.xft_font,
				x_offset, y_offset,
				(FcChar8 *) no->title.data,
				(int) strlen(no->title.data));

		/* Increment y_offset for text body */
		y_offset += 24;
	}

	if (no->body.data && no->body.xft_font) {
#if 0
		fprintf(stderr, "Body x:%d y:%d\n", x_offset, y_offset);
#endif
		XftDrawStringUtf8(no->draw,
				&no->body.xft_colour, no->body.xft_font,
				x_offset, y_offset,
				(FcChar8 *) no->body.data,
				(int) strlen(no->body.data));
	}

}

void osd_init(Display *dpy, struct notify_object *no, char *title, char *body, char *icon)
{
	no->frame.display = dpy;
	no->frame.screen  = DefaultScreen(no->frame.display);
	no->frame.cmap    = DefaultColormap(no->frame.display, no->frame.screen);
	no->frame.visual  = DefaultVisual(no->frame.display, no->frame.screen);
	no->frame.depth   = DisplayPlanes(no->frame.display, no->frame.screen);

	/* Init frame data */
	no->frame.width = 384;
	no->frame.height = 80;
	no->draw = NULL;

	/* Init title */
	if (!title) {
		no->title.data = NULL;
		no->title.xft_font = NULL;
		no->title.extents.width = 0;
		no->title.extents.height = 0;
	} else {
		no->title.data = title;
	}

	if (!body) {
		no->body.data  = NULL;
		no->body.xft_font = NULL;
		no->body.extents.width = 0;
		no->body.extents.height = 0;
	} else {
		no->body.data  = body;
	}

	if (!icon) {
		no->icon.name   = NULL;
		no->icon.ximage = NULL;
	} else {
		no->icon.name  = icon;
	}

#ifdef OSD_DEBUG
	fprintf(stderr, "title:%s\nbody:%s\nicon:%s\n",
		no->title.data, no->body.data, no->icon.name);
#endif
}
