/*
 * em2pixel.c
 *
 *
 * Formula:
 *	em = (1 / fontsize_px) * px	[pixel to em]
 *	px = em * fontsize_px		[em to pixel]
 */

#include <stdio.h>
#include <stdlib.h>


unsigned int em_to_pixel(const double em, const unsigned int fontsize)
{
	return (em * fontsize);
}

double pixel_to_em(const unsigned int pixels, const unsigned int fontsize)
{
	double px = (double) pixels;
	double fs = (double) fontsize;

	return px/fs;
}


int main(int argc, char **argv)
{
	unsigned int fs;
	unsigned int px;
	double em;

	if (argc < 4) {
		printf("usage: e fontsize pixels\n");
		printf("       p fontsize em\n");
		return 1;
	}

	if (*argv[1] == 'e') {
		fs = strtol(argv[2], (char **) NULL, 10);
		px = strtol(argv[3], (char **) NULL, 10);
		printf("px=%d fs=%u --> em=%.3f\n", px, fs, pixel_to_em(px, fs));
	} else if(*argv[1] == 'p') {
		fs = strtol(argv[2], (char **) NULL, 10);
		em = strtod(argv[3], (char **) NULL);
		printf("em=%.3f fs=%u --> px=%u\n", em, fs, em_to_pixel(em, fs));
	} else {
		printf("Wrong\n");
		return 2;
	}

	return 0;
}
