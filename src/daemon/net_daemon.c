/*
 * Network interface daemon
 *
 * To use this code, search for 'TODO' and follow the directions.
 *
 * To compile this file:
 *      gcc -o [daemonname] thisfile.c
 *
 */
#define _GNU_SOURCE

#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <syslog.h>
#include <string.h>
#include <assert.h>
#include <signal.h>

#include "if_status.h"

#define DAEMON_NAME	"net_daemon"
#define DAEMON_PID_FILE	"/var/run/net_daemon.pid"

#define IFACE_MAX 4

struct interface {
	char name[16];
	int status;
};

struct interface ifs[IFACE_MAX];

static volatile int running = 1;

/**
 * Print the command-line options for this application.
 *
 * @param[in] argc	Standard argument count
 * @param[in] argv	Standard argument array
 */
static void usage(int argc, char **argv)
{
	if (argc >= 1) {
		fprintf(stderr, "Usage: %s [options]\n"
			"  Options:\n"
			"    -i\tInterface names (separated by comma).\n"
			"    -w\tEnable Wireless extension.\n"
			"    -n\tDon't fork off as a daemon.\n"
			"    -h\tShow this help screen.\n",
			basename(argv[0]));

	}
}

/**
 * This function handles select signals that the application may receive.
 * This function is installed as a signal handler in the 'main()' function.
 *
 * @param[in] sig	The received signal number
 */
void signal_handler(int sig)
{
	switch (sig) {
	case SIGHUP:
		syslog(LOG_WARNING, "Received SIGHUP signal.");
		break;
	case SIGTERM:
		syslog(LOG_WARNING, "Received SIGTERM signal.");
		running = 0;
		break;
	default:
		syslog(LOG_WARNING,
			"Unhandled signal (%d) %s", sig, strsignal(sig));
		break;
	}
}

static void demonizing_process(void)
{
	pid_t pid;	/* Our Process ID */
	pid_t sid;	/* Our Session ID */

	syslog(LOG_INFO, "Starting the daemonizing process");

	/* Fork off the parent process */
	pid = fork();
	if (pid < 0) {
		syslog(LOG_ERR, "%s - FORK: %s", __func__, strerror(errno));
		exit(EXIT_FAILURE);
	}

	/* If we got a good PID, then we can exit the parent process. */
	if (pid > 0) {
		syslog(LOG_DEBUG, "%s - TERM parent process", __func__);
		exit(EXIT_SUCCESS);
	}

	/* Change the file mode mask */
	umask(0);

	/* Create a new SID for the child process */
	sid = setsid();
	if (sid < 0) {
		syslog(LOG_ERR, "%s - SETSID: %s", __func__, strerror(errno));
		exit(EXIT_FAILURE);
	}

	/* Change the current working directory */
	if ((chdir("/")) < 0) {
		syslog(LOG_ERR, "%s - CHDIR: %s", __func__, strerror(errno));
		exit(EXIT_FAILURE);
	}

        /* Close out the standard file descriptors */
        close(STDIN_FILENO);
        close(STDOUT_FILENO);
        close(STDERR_FILENO);
}

static void notify_status_change(const char *if_name, const int status)
{
	switch (status) {
	case IFSTATUS_UP:
		syslog(LOG_INFO, "%s: Link beat detected\n", if_name);
		break;
	case IFSTATUS_DOWN:
		syslog(LOG_INFO, "%s: Unplugged\n", if_name);
		break;
	default:
		syslog(LOG_INFO, "%s: Not supported\n", if_name);
		break;
	}
}

int parse_interfaces(struct interface *ifs, const char *buf, const size_t n)
{
	char *s;
	char *p;
	char *token;
	struct interface *iface;
	unsigned int i;

	i = 0;

	s = strdup(buf);
	if (!s)
		return 0;

	/* copy ptr to free later (strsep modyfy s)*/
	p = s;

	do {
		token = strsep(&s, ",");
		if (token) {
			iface = &ifs[i];
			strncpy(iface->name, token, 16);
			i++;
			if (i >= n)
				break;
		}
	} while (token);

	free(p);

	return i;
}


/**************************************************************************/

int main(int argc, char **argv)
{
	int c;
	int net_status = -1;

	int if_num = 0;

#if defined(DEBUG)
	int daemonize = 0;
#else
	int daemonize = 1;
#endif
	int wlan = 0;	/* monitor wlan */

	/* Setup signal handling before we start */
	signal(SIGHUP,  signal_handler);
	signal(SIGTERM, signal_handler);
	signal(SIGINT,  signal_handler);
	signal(SIGQUIT, signal_handler);

	/* Command line parsing */
	while((c = getopt(argc, argv, "i:wnh|help")) != -1) {
		switch (c) {
		case 'w' :
			wlan = 1;
			break;
		case 'i':
			if_num = parse_interfaces(ifs, optarg, IFACE_MAX);
			/*if_name = optarg;*/
			break;

		case 'h':
			usage(argc, argv);
			exit(0);
			break;
		case 'n':
			daemonize = 0;
			break;
		default:
			usage(argc, argv);
			exit(0);
			break;
		}
	}

	if (if_num <= 0) {
		syslog(LOG_INFO, "No interface specified. Exit");
		exit(-1);
	}

	/* Setup syslog logging - see setlogmask(3) */
#if defined(DEBUG)
	setlogmask(LOG_UPTO(LOG_DEBUG));
	openlog(DAEMON_NAME, LOG_CONS | LOG_NDELAY | LOG_PERROR | LOG_PID, LOG_USER);
#else
	setlogmask(LOG_UPTO(LOG_INFO));
	openlog(DAEMON_NAME, LOG_CONS, LOG_USER);
#endif

	if (daemonize) {
		syslog(LOG_DEBUG, "Daemon mode");
		demonizing_process();
	} else {
		syslog(LOG_INFO, "Monitor mode");
	}

	/*
	 * Start deamon processing code
	 */
	syslog(LOG_INFO, "Wireless extension: %s", (wlan) ? "Enable" : "Disable");

	/* Get current interfaces status */
	for (c = 0; c < if_num; c++) {
		syslog(LOG_INFO, "Monitor: %s\n", ifs[c].name);
		ifs[c].status = net_monitor_interface(ifs[c].name, wlan);
	}

	while (running) {
		for (c = 0; c < if_num; c++) {
			if (ifs[c].name[0] != '\0') {
				net_status = net_monitor_interface(ifs[c].name, wlan);
				if (net_status != ifs[c].status) {
					ifs[c].status = net_status;
					notify_status_change(ifs[c].name, net_status);
				}
			}
		}
		sleep(5);
	}

	/*
	 * TODO: Free any allocated resources (if any) before exiting
	 */

	syslog(LOG_INFO, "Exit");

	exit(0);
}
