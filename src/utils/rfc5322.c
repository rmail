#include <stdio.h>
#include <string.h>
#include <ctype.h>

#include "string_utils.h"
#include "rfc2047.h"

/* Extract address form a From: line in e-mail header */
int rfc5322_extract_address(char *from, const char *buf, size_t n)
{
	char *start = NULL;
	char *end   = NULL;

	start = strchr(buf, (int) '<');
	end = strchr(buf, (int) '>');

	/* Check angle-address presence (e.g <login@host.domain>) */
	if ((start) && (end) && (start < end)) {
		/* Extract the "agle-addr" text */
		start++;
		end--;
		substrncpy(from, start, end, n);
		return 0;
	}

	/* No angle address found.
	 * Try to extract the address searching the 'at' character.
	 */

	/* Get '@' position */
	start = strchr(buf, (int) '@');
	if (!start) {
		/* XXX: Something wrong no valid address */
		strncpy(from, buf, n);
		return 1;
	}

	end = start;
	while ((!isspace(*start)) && (start > buf))
		start--;

	while ((!isspace(*end)) && (end < buf + n))
		end++;

	start++;
	end--;

	substrncpy(from, start, end, n);

	return 0;
}

#if 0
/* Extract the realname from a mail address */
void rfc5322_extract_name(char *from, const char *buf, size_t n)
{
	char *start = NULL;
	char *end   = NULL;


	/* Check old style */
	start = strchr(buf, (int) '(');
	end = strchr(buf, (int) ')');

	/* Check angle-address presence (e.g <login@host.domain>) */
	if ((start) && (end) && (start < end)) {
		/* Remove the "agle-addr" text */
		start++;
		end--;
		substrncpy(from, start, end, n);
		return;
	}

	/* No angle address found. */
}
#endif


/* Extract the Subject: from e-mail header line */
void rfc5322_extract_subject(char *subject, const char *buf, size_t n)
{
	char *start = NULL;
	char *end   = NULL;

	if (!subject || !buf || (n == 0)) {
		return;
	}

	start = strchr(buf, (int) ':');
	if (start) {
		start++;
		while (isspace(*start))
			start++;
	}

	end = strchr(buf, (int) '\n');
	if (end) {
		*end = '\0';
		end--;
		while (isspace(*end))
			end--;
	}

	/* XXX: n should be optimized!*/

	/* Decode plain, quoted-printable or base64 text */
	rfc2047_decode_word(subject, start, n);
}
