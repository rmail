#ifndef MAIL_H
#define MAIL_H

#include <stdio.h>
#include <time.h>

#include "mime.h"

#define LINE_LENGTH		1024

struct mail_header {
	char filename[1024];		/* Filename */
	char from[LINE_LENGTH];		/* From or Reply-To address */
	char subject[LINE_LENGTH];	/* Subject */
	unsigned char addr_md5[16];	/* MD5 sum of 'from' field */

	time_t  date;			/* Mail date time */

	long	body_offset;		/* File offset of start of msg body */

	struct mime_header mime;	/* MIME informations */
};


void init_mail_header(struct mail_header *hdr);
void parse_mail_header(FILE *fh, struct mail_header *hdr);


void print_mail_header(struct mail_header *hdr);

#endif /* MAIL_H */
