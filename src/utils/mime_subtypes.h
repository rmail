#ifndef MIME_SUBTYPES_H
#define MIME_SUBTYPES_H
/* MIME subtypes */

enum mime_subtype {

	/*********************************************************************
	 * Subtypes common for all MIME types
	 ********************************************************************/
	MIME_SUBTYPE_UNKNOWN = 0,
	MIME_SUBTYPE_EXAMPLE,		/* example [RFC4735] */

	/*********************************************************************
	 * Subtypes common for Application, Audio, Text, Video
	 ********************************************************************/
	MIME_SUBTYPE_1D_INTERLEAVED_PARITYFEC,	/* 1d-interleaved-parityfec [RFC6015] */
	MIME_SUBTYPE_PARITYFEC,			/* parityfec [RFC5109] */
	MIME_SUBTYPE_ULPFEC,			/* ulpfec [RFC5109] */
	MIME_SUBTYPE_RTX,			/* rtx [RFC4588] */

	/*********************************************************************
	 * Subtypes common for Audio, Text
	 ********************************************************************/
	MIME_SUBTYPE_RED,			/* RED [RFC4102] */

	/*********************************************************************
	 * Subtypes common for Audio, Video
	 ********************************************************************/
	MIME_SUBTYPE_3GPP,		/* 3gpp  [RFC4281][RFC3839] */
	MIME_SUBTYPE_3GPP2,		/* 3gpp2 [RFC4393][RFC4281] */
	MIME_SUBTYPE_MPEG,		/* mpeg  [RFC2045,RFC2046] (video) */
					/*       [RFC3003] (audio)	   */

	/*********************************************************************
	 * Subtypes common for Audio, Text, Video
	 ********************************************************************/
	MIME_SUBTYPE_RTP_ENC_AESCM128,		/* rtp-enc-aescm128 [3GPP] */

	/*********************************************************************
	 * Subtypes common for Application, Image
	 ********************************************************************/
	MIME_SUBTYPE_FITS,			/* fits [RFC4047] */

	/*********************************************************************
	 * Subtypes common for Application, Message
	 ********************************************************************/
	MIME_SUBTYPE_HTTP,			/* http [RFC2616] */

	/*********************************************************************
	 * Subtypes common for Application, Model
	 ********************************************************************/
	MIME_SUBTYPE_IGES,			/* iges [Parks] */

	/*********************************************************************
	 * Subtypes common for Application, Text
	 ********************************************************************/
	MIME_SUBTYPE_DNS,				/* dns [RFC4027] */
	MIME_SUBTYPE_ECMASCRIPT,			/* ecmascript [RFC4329] (obsolete for text type) */
	MIME_SUBTYPE_JAVASCRIPT,			/* javascript [RFC4329] (obsolete for text type) */
	MIME_SUBTYPE_RTF,				/* rtf [Lindner] */
	MIME_SUBTYPE_XML,				/* xml [RFC3023] */
	MIME_SUBTYPE_XML_EXTERNAL_PARSED_ENTITY,	/* xml-external-parsed-entity [RFC3023] */

	/*********************************************************************
	 * Subtypes common for Audio, Image
	 ********************************************************************/
	MIME_SUBTYPE_T38,				/* t38 [RFC4612] */

	/*********************************************************************
	 * Subtypes common for Application, Audio, Video
	 ********************************************************************/
	MIME_SUBTYPE_MP4,			/* mp4 [RFC4337] */

	MIME_SUBTYPE_MPEG4_GENERIC,		/* mpeg4-generic
						 * [RFC3640] (Application and Video)
						 * [RFC3640][RFC5691][RFC-ietf-payload-rfc4695-bis-02.txt] (Audio)
						 */

	MIME_SUBTYPE_OGG,			/* ogg [RFC5334] */

	/*********************************************************************
	 * Subtypes common for Image, Video
	 ********************************************************************/
	MIME_SUBTYPE_JPEG,				/* jpeg [RFC2045, RFC2046] (image) */
							/* JPEG [RFC3555] (video) */

	/* Subtypes common for Application, Audio, Image, Message, Model, Text, Video */


	/*********************************************************************
	 * MIME text subtypes
	 ********************************************************************/
	MIME_SUBTYPE_CALENDAR,				/* calendar [RFC5545] */
	MIME_SUBTYPE_CSS,				/* css [RFC2318] */
	MIME_SUBTYPE_CSV,				/* csv [RFC4180] */
	MIME_SUBTYPE_DIRECTORY,				/* directory [RFC2425] */
	MIME_SUBTYPE_ENRICHED,				/* enriched [RFC1896] */
	MIME_SUBTYPE_HTML,				/* html [RFC2854] */
	MIME_SUBTYPE_N3,				/* n3 [Prud'hommeaux][W3C] */
	MIME_SUBTYPE_PLAIN,				/* plain [RFC2046][RFC3676][RFC5147] */
	MIME_SUBTYPE_PRS_FALLENSTEIN_RST,		/* prs.fallenstein.rst [Fallenstein] */
	MIME_SUBTYPE_PRS_LINES_TAG,			/* prs.lines.tag [Lines] */
	MIME_SUBTYPE_RFC822_HEADERS,			/* rfc822-headers [RFC3462] */
	MIME_SUBTYPE_RICHTEXT,				/* richtext [RFC2045][RFC2046] */
	MIME_SUBTYPE_SGML,				/* sgml	 [RFC1874] */
	MIME_SUBTYPE_T140,				/* t140 [RFC4103] */
	MIME_SUBTYPE_TAB_SEPARATED_VALUES,		/* tab-separated-values [Lindner] */
	MIME_SUBTYPE_TROFF,				/* troff [RFC4263] */
	MIME_SUBTYPE_TURTLE,				/* turtle [Prud'hommeaux][W3C] */
	MIME_SUBTYPE_URI_LIST,				/* uri-list [RFC2483] */
	MIME_SUBTYPE_VND_ABC,				/* vnd.abc [Allen] */
	MIME_SUBTYPE_VND_CURL,				/* vnd.curl [Byrnes] */
	MIME_SUBTYPE_VND_DMCLIENTSCRIPT,		/* vnd.DMClientScript [Bradley] */
	MIME_SUBTYPE_VND_ESMERTEC_THEME_DESCRIPTOR,	/* vnd.esmertec.theme-descriptor [Eilemann] */
	MIME_SUBTYPE_VND_FLY,				/* vnd.fly [Gurney] */
	MIME_SUBTYPE_VND_FMI_FLEXSTOR,			/* vnd.fmi.flexstor [Hurtta] */
	MIME_SUBTYPE_VND_GRAPHVIZ,			/* vnd.graphviz [Ellson] */
	MIME_SUBTYPE_VND_IN3D_3DML,			/* vnd.in3d.3dml [Powers] */
	MIME_SUBTYPE_VND_IN3D_SPOT,			/* vnd.in3d.spot [Powers] */
	MIME_SUBTYPE_VND_IPTC_NEWSML,			/* vnd.IPTC.NewsML [IPTC] */
	MIME_SUBTYPE_VND_IPTC_NITF,			/* vnd.IPTC.NITF [IPTC] */
	MIME_SUBTYPE_VND_LATEX_Z,			/* vnd.latex-z [Lubos] */
	MIME_SUBTYPE_VND_MOTOROLA_REFLEX,		/* vnd.motorola.reflex [Patton] */
	MIME_SUBTYPE_VND_MS_MEDIAPACKAGE,		/* vnd.ms-mediapackage [Nelson] */
	MIME_SUBTYPE_VND_NET2PHONE_COMMCENTER_COMMAND,	/* vnd.net2phone.commcenter.command [Xie] */
	MIME_SUBTYPE_VND_RADISYS_MSML_BASIC_LAYOUT,	/* vnd.radisys.msml-basic-layout [RFC5707] */
	MIME_SUBTYPE_VND_SI_URICATALOGUE,		/* vnd.si.uricatalogue [Parks Young] */
	MIME_SUBTYPE_VND_SUN_J2ME_APP_DESCRIPTOR,	/* vnd.sun.j2me.app-descriptor [G.Adams] */
	MIME_SUBTYPE_VND_TROLLTECH_LINGUIST,		/* vnd.trolltech.linguist [D.Lambert] */
	MIME_SUBTYPE_VND_WAP_SI,			/* vnd.wap.si [WAP-Forum] */
	MIME_SUBTYPE_VND_WAP_SL,			/* vnd.wap.sl [WAP-Forum] */
	MIME_SUBTYPE_VND_WAP_WML,			/* vnd.wap.wml [Stark] */
	MIME_SUBTYPE_VND_WAP_WMLSCRIPT,			/* vnd.wap.wmlscript	[Stark] */

	/* multipart */
	MIME_SUBTYPE_ALTERNATIVE,			/* alternative [RFC2045,RFC2046] */
	MIME_SUBTYPE_APPLEDOUBLE,			/* appledouble [Faltstrom] */
	MIME_SUBTYPE_BYTERANGES,			/* byteranges [RFC2616] */
	MIME_SUBTYPE_DIGEST,				/* digest [RFC2045,RFC2046] */
	MIME_SUBTYPE_ENCRYPTED,				/* encrypted [RFC1847] */
	MIME_SUBTYPE_FORM_DATA,				/* form-data [RFC2388] */
	MIME_SUBTYPE_HEADER_SET,			/* header-set [Crocker] */
	MIME_SUBTYPE_MIXED,				/* mixed [RFC2045,RFC2046] */
	MIME_SUBTYPE_PARALLEL,				/* parallel [RFC2045,RFC2046] */
	MIME_SUBTYPE_RELATED,				/* related [RFC2387] */
	MIME_SUBTYPE_REPORT,				/* report [RFC3462] */
	MIME_SUBTYPE_SIGNED,				/* signed [RFC1847] */
	MIME_SUBTYPE_VOICE_MESSAGE,			/* voice-message [RFC2421,RFC2423] */

	/* audio */
	MIME_SUBTYPE_32KADPCM,				/* 32kadpcm [RFC3802][RFC2421] */
	MIME_SUBTYPE_AC3,				/* ac3 [RFC4184] */
	MIME_SUBTYPE_AMR,				/* AMR [RFC4867] */
	MIME_SUBTYPE_AMR_WB,				/* AMR-WB [RFC4867] */
	MIME_SUBTYPE_AMR_WBP,				/* amr-wb+ [RFC4352] */
	MIME_SUBTYPE_ASC,				/* asc [RFC-ietf-payload-rfc4695-bis-02.txt] */
	MIME_SUBTYPE_ATRAC_ADVANCED_LOSSLESS,		/* ATRAC-ADVANCED-LOSSLESS [RFC5584] */
	MIME_SUBTYPE_ATRAC_X,				/* ATRAC-X [RFC5584] */
	MIME_SUBTYPE_ATRAC3,				/* ATRAC3 [RFC5584] */
	MIME_SUBTYPE_BASIC,				/* basic [RFC2045][RFC2046] */
	MIME_SUBTYPE_BV16,				/* BV16 [RFC4298] */
	MIME_SUBTYPE_BV32,				/* BV32 [RFC4298] */
	MIME_SUBTYPE_CLEARMODE,				/* clearmode [RFC4040] */
	MIME_SUBTYPE_CN,				/* CN [RFC3389] */
	MIME_SUBTYPE_DAT12,				/* DAT12 [RFC3190] */
	MIME_SUBTYPE_DLS,				/* dls [RFC4613] */
	MIME_SUBTYPE_DSR_ES201108,			/* dsr-es201108 [RFC3557] */
	MIME_SUBTYPE_DSR_ES202050,			/* dsr-es202050 [RFC4060] */
	MIME_SUBTYPE_DSR_ES202211,			/* dsr-es202211 [RFC4060] */
	MIME_SUBTYPE_DSR_ES202212,			/* dsr-es202212 [RFC4060] */
	MIME_SUBTYPE_EAC3,				/* eac3 [RFC4598] */
	MIME_SUBTYPE_DVI4,				/* DVI4 [RFC4856] */
	MIME_SUBTYPE_EVRC,				/* EVRC [RFC4788] */
	MIME_SUBTYPE_EVRC0,				/* EVRC0 [RFC4788] */
	MIME_SUBTYPE_EVRC1,				/* EVRC1 [RFC4788] */
	MIME_SUBTYPE_EVRCB,				/* EVRCB [RFC5188] */
	MIME_SUBTYPE_EVRCB0,				/* EVRCB0 [RFC5188] */
	MIME_SUBTYPE_EVRCB1,				/* EVRCB1 [RFC4788] */
	MIME_SUBTYPE_EVRC_QCP,				/* EVRC-QCP [RFC3625] */
	MIME_SUBTYPE_EVRCWB,				/* EVRCWB [RFC5188] */
	MIME_SUBTYPE_EVRCWB0,				/* EVRCWB0 [RFC5188] */
	MIME_SUBTYPE_EVRCWB1,				/* EVRCWB1 [RFC5188] */
	MIME_SUBTYPE_G719,				/* G719 [RFC5404] */
	MIME_SUBTYPE_G722,				/* G722 [RFC4856] */
	MIME_SUBTYPE_G7221,				/* G7221 [RFC5577] */
	MIME_SUBTYPE_G723,				/* G723 [RFC4856] */
	MIME_SUBTYPE_G726_16,				/* G726-16 [RFC4856] */
	MIME_SUBTYPE_G726_24,				/* G726-24 [RFC4856] */
	MIME_SUBTYPE_G726_32,				/* G726-32 [RFC4856] */
	MIME_SUBTYPE_G726_40,				/* G726-40 [RFC4856] */
	MIME_SUBTYPE_G728,				/* G728 [RFC4856] */
	MIME_SUBTYPE_G729,				/* G729 [RFC4856] */
	MIME_SUBTYPE_G7291,				/* G7291 [RFC4749][RFC5459] */
	MIME_SUBTYPE_G729D,				/* G729D [RFC4856] */
	MIME_SUBTYPE_G729E,				/* G729E [RFC4856] */
	MIME_SUBTYPE_GSM,				/* GSM [RFC4856] */
	MIME_SUBTYPE_GSM_EFR,				/* GSM-EFR [RFC4856] */
	MIME_SUBTYPE_GSM_HR_08,				/* GSM-HR-08 [RFC5993] */
	MIME_SUBTYPE_ILBC,				/* iLBC [RFC3952] */
	MIME_SUBTYPE_IP_MR_V2_5,			/* ip-mr_v2.5 [RFC-ietf-avt-rtp-ipmr-15.txt] */
	MIME_SUBTYPE_L8,				/* L8 [RFC4856] */
	MIME_SUBTYPE_L16,				/* L16 [RFC4856] */
	MIME_SUBTYPE_L20,				/* L20 [RFC3190] */
	MIME_SUBTYPE_L24,				/* L24 [RFC3190] */
	MIME_SUBTYPE_LPC,				/* LPC [RFC4856] */
	MIME_SUBTYPE_MOBILE_XMF,			/* mobile-xmf [RFC4723] */
	MIME_SUBTYPE_MPA,				/* MPA [RFC3555] */
	MIME_SUBTYPE_MP4A_LATM,				/* MP4A-LATM [RFC3016] */
	MIME_SUBTYPE_MPA_ROBUST,			/* mpa-robust [RFC5219] */
	MIME_SUBTYPE_PCMA,				/* PCMA [RFC4856] */
	MIME_SUBTYPE_PCMA_WB,				/* PCMA-WB [RFC5391] */
	MIME_SUBTYPE_PCMU,				/* PCMU [RFC4856] */
	MIME_SUBTYPE_PCMU_WB,				/* PCMU-WB [RFC5391] */
	MIME_SUBTYPE_PRS_SID,				/* prs.sid [Walleij] */
	MIME_SUBTYPE_QCELP,				/* QCELP [RFC3555][RFC3625] */
	MIME_SUBTYPE_RTP_MIDI,				/* rtp-midi [RFC-ietf-payload-rfc4695-bis-02.txt] */
	MIME_SUBTYPE_SMV,				/* SMV [RFC3558] */
	MIME_SUBTYPE_SMV0,				/* SMV0 [RFC3558] */
	MIME_SUBTYPE_SMV_QCP,				/* SMV-QCP [RFC3625] */
	MIME_SUBTYPE_SP_MIDI,				/* sp-midi [Kosonen][T. White] */
	MIME_SUBTYPE_SPEEX,				/* speex [RFC5574] */
	MIME_SUBTYPE_T140C,				/* t140c [RFC4351] */
	MIME_SUBTYPE_TELEPHONE_EVENT,			/* telephone-event [RFC4733] */
	MIME_SUBTYPE_TONE,				/* tone [RFC4733] */
	MIME_SUBTYPE_UEMCLIP,				/* UEMCLIP [RFC5686] */
	MIME_SUBTYPE_VDVI,				/* VDVI [RFC4856] */
	MIME_SUBTYPE_VMR_WB,				/* VMR-WB [RFC4348][RFC4424] */
	MIME_SUBTYPE_VND_3GPP_IUFP,			/* vnd.3gpp.iufp [Belling] */
	MIME_SUBTYPE_VND_4SB,				/* vnd.4SB [De Jaham] */
	MIME_SUBTYPE_VND_AUDIOKOZ,			/* vnd.audiokoz [DeBarros] */
	MIME_SUBTYPE_VND_CELP,				/* vnd.CELP [De Jaham] */
	MIME_SUBTYPE_VND_CISCO_NSE,			/* vnd.cisco.nse [Kumar] */
	MIME_SUBTYPE_VND_CMLES_RADIO_EVENTS,		/* vnd.cmles.radio-events [Goulet] */
	MIME_SUBTYPE_VND_CNS_ANP1,			/* vnd.cns.anp1 [McLaughlin] */
	MIME_SUBTYPE_VND_CNS_INF1,			/* vnd.cns.inf1 [McLaughlin] */
	MIME_SUBTYPE_VND_DECE_AUDIO,			/* vnd.dece.audio [Dolan] */
	MIME_SUBTYPE_VND_DIGITAL_WINDS,			/* vnd.digital-winds [Strazds] */
	MIME_SUBTYPE_VND_DLNA_ADTS,			/* vnd.dlna.adts [Heredia] */
	MIME_SUBTYPE_VND_DOLBY_HEAAC_1,			/* vnd.dolby.heaac.1 [Hattersley] */
	MIME_SUBTYPE_VND_DOLBY_HEAAC_2,			/* vnd.dolby.heaac.2 [Hattersley] */
	MIME_SUBTYPE_VND_DOLBY_MLP,			/* vnd.dolby.mlp [Ward] */
	MIME_SUBTYPE_VND_DOLBY_MPS,			/* vnd.dolby.mps [Hattersley] */
	MIME_SUBTYPE_VND_DOLBY_PL2,			/* vnd.dolby.pl2 [Hattersley] */
	MIME_SUBTYPE_VND_DOLBY_PL2X,			/* vnd.dolby.pl2x [Hattersley] */
	MIME_SUBTYPE_VND_DOLBY_PL2Z,			/* vnd.dolby.pl2z [Hattersley] */
	MIME_SUBTYPE_VND_DOLBY_PULSE_1,			/* vnd.dolby.pulse.1 [Hattersley] */
	MIME_SUBTYPE_VND_DRA,				/* vnd.dra [Tian] */
	MIME_SUBTYPE_VND_DTS,				/* vnd.dts [Zou] */
	MIME_SUBTYPE_VND_DTS_HD,			/* vnd.dts.hd [Zou] */
	MIME_SUBTYPE_VND_DVB_FILE,			/* vnd.dvb.file [Siebert] */
	MIME_SUBTYPE_VND_EVERAD_PLJ,			/* vnd.everad.plj [Cicelsky] */
	MIME_SUBTYPE_VND_HNS_AUDIO,			/* vnd.hns.audio [Swaminathan] */
	MIME_SUBTYPE_VND_LUCENT_VOICE,			/* vnd.lucent.voice [Vaudreuil] */
	MIME_SUBTYPE_VND_MS_PLAYREADY_MEDIA_PYA,	/* vnd.ms-playready.media.pya [DiAcetis] */
	MIME_SUBTYPE_VND_NOKIA_MOBILE_XMF,		/* vnd.nokia.mobile-xmf [Nokia Corporation] */
	MIME_SUBTYPE_VND_NORTEL_VBK,			/* vnd.nortel.vbk [Parsons] */
	MIME_SUBTYPE_VND_NUERA_ECELP4800,		/* vnd.nuera.ecelp4800 [Fox] */
	MIME_SUBTYPE_VND_NUERA_ECELP7470,		/* vnd.nuera.ecelp7470 [Fox] */
	MIME_SUBTYPE_VND_NUERA_ECELP9600,		/* vnd.nuera.ecelp9600 [Fox] */
	MIME_SUBTYPE_VND_OCTEL_SBC,			/* vnd.octel.sbc [Vaudreuil] */
	MIME_SUBTYPE_VND_QCELP,				/* vnd.qcelp - DEPRECATED - Please use audio/qcelp [RFC3625] */
	MIME_SUBTYPE_VND_RHETOREX_32KADPCM,		/* vnd.rhetorex.32kadpcm [Vaudreuil] */
	MIME_SUBTYPE_VND_RIP,				/* vnd.rip [Dawe] */
	MIME_SUBTYPE_VND_SEALEDMEDIA_SOFTSEAL_MPEG,	/* vnd.sealedmedia.softseal.mpeg [Petersen] */
	MIME_SUBTYPE_VND_VMX_CVSD,			/* vnd.vmx.cvsd [Vaudreuil] */
	MIME_SUBTYPE_VORBIS,				/* vorbis [RFC5215] */
	MIME_SUBTYPE_VORBIS_CONFIG,			/* vorbis-config [RFC5215] */

	/* image */
	MIME_SUBTYPE_CGM,				/* cgm Computer Graphics Metafile [Francis] */
	MIME_SUBTYPE_G3FAX,				/* g3fax [RFC1494] */
	MIME_SUBTYPE_GIF,				/* gif [RFC2045,RFC2046] */
	MIME_SUBTYPE_IEF,				/* ief Image Exchange Format [RFC1314] */
	MIME_SUBTYPE_JP2,				/* jp2 [RFC3745] */
	MIME_SUBTYPE_JPM,				/* jpm [RFC3745] */
	MIME_SUBTYPE_JPX,				/* jpx [RFC3745] */
	MIME_SUBTYPE_KTX,				/* ktx [Callow][Khronos] */
	MIME_SUBTYPE_NAPLPS,				/* naplps [Ferber] */
	MIME_SUBTYPE_PNG,				/* png [Randers-Pehrson] */
	MIME_SUBTYPE_PRS_BTIF,				/* prs.btif [Simon] */
	MIME_SUBTYPE_PRS_PTI,				/* prs.pti [Laun] */
	MIME_SUBTYPE_SVG_XML,				/* svg+xml [W3C] */
	MIME_SUBTYPE_TIFF,				/* tiff [RFC3302] */
	MIME_SUBTYPE_TIFF_FX,				/* tiff-fx [RFC3950] */
	MIME_SUBTYPE_VND_ADOBE_PHOTOSHOP,		/* vnd.adobe.photoshop [Scarborough] */
	MIME_SUBTYPE_VND_CNS_INF2,			/* vnd.cns.inf2 [McLaughlin] */
	MIME_SUBTYPE_VND_DECE_GRAPHIC,			/* vnd.dece.graphic [Dolan] */
	MIME_SUBTYPE_VND_DJVU,				/* vnd.djvu [Bottou] */
	MIME_SUBTYPE_VND_DVB_SUBTITLE,			/* vnd.dvb.subtitle [Siebert] */
	MIME_SUBTYPE_VND_DWG,				/* vnd.dwg [Moline] */
	MIME_SUBTYPE_VND_DXF,				/* vnd.dxf [Moline] */
	MIME_SUBTYPE_VND_FASTBIDSHEET,			/* vnd.fastbidsheet [Becker] */
	MIME_SUBTYPE_VND_FPX,				/* vnd.fpx [Spencer] */
	MIME_SUBTYPE_VND_FST,				/* vnd.fst [Fuldseth] */
	MIME_SUBTYPE_VND_FUJIXEROX_EDMICS_MMR,		/* vnd.fujixerox.edmics-mmr [Onda] */
	MIME_SUBTYPE_VND_FUJIXEROX_EDMICS_RLC,		/* vnd.fujixerox.edmics-rlc [Onda] */
	MIME_SUBTYPE_VND_GLOBALGRAPHICS_PGB,		/* vnd.globalgraphics.pgb [Bailey] */
	MIME_SUBTYPE_VND_MICROSOFT_ICON,		/* vnd.microsoft.icon [Butcher] */
	MIME_SUBTYPE_VND_MIX,				/* vnd.mix [Reddy] */
	MIME_SUBTYPE_VND_MS_MODI,			/* vnd.ms-modi [Vaughan] */
	MIME_SUBTYPE_VND_NET_FPX,			/* vnd.net-fpx [Spencer] */
	MIME_SUBTYPE_VND_RADIANCE,			/* vnd.radiance [Fritz][GWard] */
	MIME_SUBTYPE_VND_SEALED_PNG,			/* vnd.sealed.png [Petersen] */
	MIME_SUBTYPE_VND_SEALEDMEDIA_SOFTSEAL_GIF,	/* vnd.sealedmedia.softseal.gif [Petersen] */
	MIME_SUBTYPE_VND_SEALEDMEDIA_SOFTSEAL_JPG,	/* vnd.sealedmedia.softseal.jpg [Petersen] */
	MIME_SUBTYPE_VND_SVF,				/* vnd.svf [Moline] */
	MIME_SUBTYPE_VND_WAP_WBMP,			/* vnd.wap.wbmp [Stark] */
	MIME_SUBTYPE_VND_XIFF,				/* vnd.xiff [S.Martin] */

	/* message */
	MIME_SUBTYPE_CPIM,				/* CPIM [RFC3862] */
	MIME_SUBTYPE_DELIVERY_STATUS,			/* delivery-status [RFC1894] */
	MIME_SUBTYPE_DISPOSITION_NOTIFICATION,		/* disposition-notification [RFC3798] */
	MIME_SUBTYPE_EXTERNAL_BODY,			/* external-body [RFC2045][RFC2046] */
	MIME_SUBTYPE_FEEDBACK_REPORT,			/* feedback-report [RFC5965] */
	MIME_SUBTYPE_GLOBAL,				/* global [RFC5335] */
	MIME_SUBTYPE_GLOBAL_DELIVERY_STATUS,		/* global-delivery-status [RFC5337] */
	MIME_SUBTYPE_GLOBAL_DISPOSITION_NOTIFICATION,	/* global-disposition-notification [RFC5337] */
	MIME_SUBTYPE_GLOBAL_HEADERS,			/* global-headers [RFC5337] */
	MIME_SUBTYPE_IMDN_XML,				/* imdn+xml [RFC5438] */
	MIME_SUBTYPE_NEWS,				/* news (OBSOLETE) [RFC5537] [H.Spencer] */
	MIME_SUBTYPE_PARTIAL,				/* partial [RFC2045][RFC2046] */
	MIME_SUBTYPE_RFC822,				/* rfc822 [RFC2045][RFC2046] */
	MIME_SUBTYPE_S_HTTP,				/* s-http [RFC2660] */
	MIME_SUBTYPE_SIP,				/* sip [RFC3261] */
	MIME_SUBTYPE_SIPFRAG,				/* sipfrag [RFC3420] */
	MIME_SUBTYPE_TRACKING_STATUS,			/* tracking-status [RFC3886] */
	MIME_SUBTYPE_VND_SI_SIMP,			/* vnd.si.simp [Parks Young] */

	/* model */
	MIME_SUBTYPE_MESH,				/* mesh [RFC2077] */
	MIME_SUBTYPE_VND_COLLADA_XML,			/* vnd.collada+xml [Riordon] */
	MIME_SUBTYPE_VND_DWF,				/* vnd.dwf [Pratt] */
	MIME_SUBTYPE_VND_FLATLAND_3DML,			/* vnd.flatland.3dml [Powers] */
	MIME_SUBTYPE_VND_GDL,				/* vnd.gdl [Babits] */
	MIME_SUBTYPE_VND_GS_GDL,			/* vnd.gs-gdl [Babits] */
	MIME_SUBTYPE_VND_GTW,				/* vnd.gtw [Ozaki] */
	MIME_SUBTYPE_VND_MOML_XML,			/* vnd.moml+xml [Brooks] */
	MIME_SUBTYPE_VND_MTS,				/* vnd.mts */
	MIME_SUBTYPE_VND_PARASOLID_TRANSMIT_BINARY,	/* vnd.parasolid.transmit.binary [Parasolid] */
	MIME_SUBTYPE_VND_PARASOLID_TRANSMIT_TEXT,	/* vnd.parasolid.transmit.text [Parasolid] */
	MIME_SUBTYPE_VND_VTU,				/* vnd.vtu [Rabinovitch] */
	MIME_SUBTYPE_VRML,				/* vrml [RFC2077] */

	/* application */
	MIME_SUBTYPE_3GPP_IMS_XML,			/* 3gpp-ims+xml [Meredith] */
	MIME_SUBTYPE_ACTIVEMESSAGE,			/* activemessage [Shapiro] */
	MIME_SUBTYPE_ANDREW_INSET,			/* andrew-inset [Borenstein] */
	MIME_SUBTYPE_APPLEFILE,				/* applefile [Faltstrom] */
	MIME_SUBTYPE_ATOM_XML,				/* atom+xml [RFC4287][RFC5023] */
	MIME_SUBTYPE_ATOMICMAIL,			/* atomicmail [Borenstein] */
	MIME_SUBTYPE_ATOMCAT_XML,			/* atomcat+xml [RFC5023] */
	MIME_SUBTYPE_ATOMSVC_XML,			/* atomsvc+xml [RFC5023] */
	MIME_SUBTYPE_AUTH_POLICY_XML,			/* auth-policy+xml [RFC4745] */
	MIME_SUBTYPE_BATCH_SMTP,			/* batch-SMTP [RFC2442] */
	MIME_SUBTYPE_BEEP_XML,				/* beep+xml [RFC3080] */
	MIME_SUBTYPE_CALS_1840,				/* cals-1840 [RFC1895] */
	MIME_SUBTYPE_CCXML_XML,				/* ccxml+xml [RFC4267] */
	MIME_SUBTYPE_CDMI_CAPABILITY,			/* cdmi-capability [RFC-cdmi-mediatypes-07.txt] */
	MIME_SUBTYPE_CDMI_CONTAINER,			/* cdmi-container [RFC-cdmi-mediatypes-07.txt] */
	MIME_SUBTYPE_CDMI_DOMAIN,			/* cdmi-domain [RFC-cdmi-mediatypes-07.txt] */
	MIME_SUBTYPE_CDMI_OBJECT,			/* cdmi-object [RFC-cdmi-mediatypes-07.txt] */
	MIME_SUBTYPE_CDMI_QUEUE,			/* cdmi-queue [RFC-cdmi-mediatypes-07.txt] */
	MIME_SUBTYPE_CEA_2018_XML,			/* cea-2018+xml [Zimmermann] */
	MIME_SUBTYPE_CELLML_XML,			/* cellml+xml [RFC4708] */
	MIME_SUBTYPE_CFW,				/* cfw [RFC-ietf-mediactrl-sip-control-framework-12.txt] */
	MIME_SUBTYPE_CNRP_XML,				/* cnrp+xml [RFC3367] */
	MIME_SUBTYPE_COMMONGROUND,			/* commonground [Glazer] */
	MIME_SUBTYPE_CONFERENCE_INFO_XML,		/* conference-info+xml [RFC4575] */
	MIME_SUBTYPE_CPL_XML,				/* cpl+xml [RFC3880] */
	MIME_SUBTYPE_CSTA_XML,				/* csta+xml [Ecma International Helpdesk] */
	MIME_SUBTYPE_CSTADATA_XML,			/* CSTAdata+xml [Ecma International Helpdesk] */
	MIME_SUBTYPE_CYBERCASH,				/* cybercash [Eastlake] */
	MIME_SUBTYPE_DAVMOUNT_XML,			/* davmount+xml [RFC4709] */
	MIME_SUBTYPE_DCA_RFT,				/* dca-rft [Campbell] */
	MIME_SUBTYPE_DEC_DX,				/* dec-dx [Campbell] */
	MIME_SUBTYPE_DIALOG_INFO_XML,			/* dialog-info+xml [RFC4235] */
	MIME_SUBTYPE_DICOM,				/* dicom [RFC3240] */
	MIME_SUBTYPE_DSKPP_XML,				/* dskpp+xml [RFC6063] */
	MIME_SUBTYPE_DSSC_DER,				/* dssc+der [RFC5698] */
	MIME_SUBTYPE_DSSC_XML,				/* dssc+xml [RFC5698] */
	MIME_SUBTYPE_DVCS,				/* dvcs [RFC3029] */
	MIME_SUBTYPE_EDI_CONSENT,			/* EDI-Consent [RFC1767] */
	MIME_SUBTYPE_EDIFACT,				/* EDIFACT [RFC1767] */
	MIME_SUBTYPE_EDI_X12,				/* EDI-X12 [RFC1767] */
	MIME_SUBTYPE_EMMA_XML,				/* emma+xml [W3C] */
	MIME_SUBTYPE_EPP_XML,				/* epp+xml [RFC5730] */
	MIME_SUBTYPE_ESHOP,				/* eshop [Katz] */
	MIME_SUBTYPE_EXI,				/* exi [W3C] */
	MIME_SUBTYPE_FASTINFOSET,			/* fastinfoset [ITU-T ASN.1 Rapporteur] */
	MIME_SUBTYPE_FASTSOAP,				/* fastsoap [ITU-T ASN.1 Rapporteur] */
	MIME_SUBTYPE_FONT_TDPFR,			/* font-tdpfr [RFC3073] */
	MIME_SUBTYPE_FRAMEWORK_ATTRIBUTES_XML,		/* framework-attributes+xml [RFC-ietf-mediactrl-sip-control-framework-12.txt] */
	MIME_SUBTYPE_H224,				/* H224 [RFC4573] */
	MIME_SUBTYPE_HELD_XML,				/* held+xml [RFC5985] */
	MIME_SUBTYPE_HYPERSTUDIO,			/* hyperstudio [Domino] */
	MIME_SUBTYPE_IBE_KEY_REQUEST_XML,		/* ibe-key-request+xml [RFC5408] */
	MIME_SUBTYPE_IBE_PKG_REPLY_XML,			/* ibe-pkg-reply+xml [RFC5408] */
	MIME_SUBTYPE_IBE_PP_DATA,			/* ibe-pp-data [RFC5408] */
	MIME_SUBTYPE_IM_ISCOMPOSING_XML,		/* im-iscomposing+xml [RFC3994] */
	MIME_SUBTYPE_INDEX,				/* index [RFC2652] */
	MIME_SUBTYPE_INDEX_CMD,				/* index.cmd [RFC2652] */
	MIME_SUBTYPE_INDEX_OBJ,				/* index.obj [RFC2652] */
	MIME_SUBTYPE_INDEX_RESPONSE,			/* index.response [RFC2652] */
	MIME_SUBTYPE_INDEX_VND,				/* index.vnd [RFC2652] */
	MIME_SUBTYPE_IOTP,				/* iotp [RFC2935] */
	MIME_SUBTYPE_IPFIX,				/* ipfix [RFC5655] */
	MIME_SUBTYPE_IPP,				/* ipp [RFC2910] */
	MIME_SUBTYPE_ISUP,				/* isup [RFC3204] */
	MIME_SUBTYPE_JSON,				/* json [RFC4627] */
	MIME_SUBTYPE_KPML_REQUEST_XML,			/* kpml-request+xml [RFC4730] */
	MIME_SUBTYPE_KPML_RESPONSE_XML,			/* kpml-response+xml [RFC4730] */
	MIME_SUBTYPE_LOST_XML,				/* lost+xml [RFC5222] */
	MIME_SUBTYPE_MAC_BINHEX40,			/* mac-binhex40 [Faltstrom] */
	MIME_SUBTYPE_MACWRITEII,			/* macwriteii [Lindner] */
	MIME_SUBTYPE_MADS_XML,				/* mads+xml [RFC-denenberg-mods-etc-media-types-03] */
	MIME_SUBTYPE_MARC,				/* marc [RFC2220] */
	MIME_SUBTYPE_MARCXML_XML,			/* marcxml+xml [RFC-denenberg-mods-etc-media-types-03] */
	MIME_SUBTYPE_MATHEMATICA,			/* mathematica [Wolfram] */
	MIME_SUBTYPE_MATHML_CONTENT_XML,		/* mathml-content+xml [W3C] */
	MIME_SUBTYPE_MATHML_PRESENTATION_XML,		/* mathml-presentation+xml [W3C] */
	MIME_SUBTYPE_MATHML_XML,			/* mathml+xml [W3C] */
	MIME_SUBTYPE_MBMS_ASSOCIATED_PROCEDURE_DESCRIPTION_XML,	/* mbms-associated-procedure-description+xml [3GPP] */
	MIME_SUBTYPE_MBMS_DEREGISTER_XML,		/* mbms-deregister+xml [3GPP] */
	MIME_SUBTYPE_MBMS_ENVELOPE_XML,			/* mbms-envelope+xml [3GPP] */
	MIME_SUBTYPE_MBMS_MSK_RESPONSE_XML,		/* mbms-msk-response+xml [3GPP] */
	MIME_SUBTYPE_MBMS_MSK_XML,			/* mbms-msk+xml [3GPP] */
	MIME_SUBTYPE_MBMS_PROTECTION_DESCRIPTION_XML,	/* mbms-protection-description+xml [3GPP] */
	MIME_SUBTYPE_MBMS_RECEPTION_REPORT_XML,		/* mbms-reception-report+xml [3GPP] */
	MIME_SUBTYPE_MBMS_REGISTER_RESPONSE_XML,	/* mbms-register-response+xml [3GPP] */
	MIME_SUBTYPE_MBMS_REGISTER_XML,			/* mbms-register+xml [3GPP] */
	MIME_SUBTYPE_MBMS_USER_SERVICE_DESCRIPTION_XML,	/* mbms-user-service-description+xml [3GPP] */
	MIME_SUBTYPE_MBOX,				/* mbox [RFC4155] */
	MIME_SUBTYPE_MEDIA_CONTROL_XML,			/* media_control+xml [RFC5168] */
	MIME_SUBTYPE_MEDIASERVERCONTROL_XML,		/* mediaservercontrol+xml [RFC5022] */
	MIME_SUBTYPE_METALINK4_XML,			/* metalink4+xml [RFC5854] */
	MIME_SUBTYPE_METS_XML,				/* mets+xml [RFC-denenberg-mods-etc-media-types-03] */
	MIME_SUBTYPE_MIKEY,				/* mikey [RFC3830] */
	MIME_SUBTYPE_MODS_XML,				/* mods+xml [RFC-denenberg-mods-etc-media-types-03] */
	MIME_SUBTYPE_MOSS_KEYS,				/* moss-keys [RFC1848] */
	MIME_SUBTYPE_MOSS_SIGNATURE,			/* moss-signature [RFC1848] */
	MIME_SUBTYPE_MOSSKEY_DATA,			/* mosskey-data [RFC1848] */
	MIME_SUBTYPE_MOSSKEY_REQUEST,			/* mosskey-request [RFC1848] */
	MIME_SUBTYPE_MP21,				/* mp21 [Singer] */
	MIME_SUBTYPE_MPEG4_IOD,				/* mpeg4-iod [RFC4337] */
	MIME_SUBTYPE_MPEG4_IOD_XMT,			/* mpeg4-iod-xmt [RFC4337] */
	MIME_SUBTYPE_MSC_IVR_XML,			/* msc-ivr+xml [RFC-ietf-mediactrl-ivr-control-package-11] */
	MIME_SUBTYPE_MSC_MIXER_XML,			/* msc-mixer+xml [RFC-ietf-mediactrl-mixer-control-package-14] */
	MIME_SUBTYPE_MSWORD,				/* msword [Lindner] */
	MIME_SUBTYPE_MXF,				/* mxf [RFC4539] */
	MIME_SUBTYPE_NASDATA,				/* nasdata [RFC4707] */
	MIME_SUBTYPE_NEWS_CHECKGROUPS,			/* news-checkgroups [RFC5537] */
	MIME_SUBTYPE_NEWS_GROUPINFO,			/* news-groupinfo [RFC5537] */
	MIME_SUBTYPE_NEWS_TRANSMISSION,			/* news-transmission [RFC5537] */
	MIME_SUBTYPE_NSS,				/* nss [Hammer] */
	MIME_SUBTYPE_OCSP_REQUEST,			/* ocsp-request [RFC2560] */
	MIME_SUBTYPE_OCSP_RESPONSE,			/* ocsp-response [RFC2560] */
	MIME_SUBTYPE_OCTET_STREAM,			/* octet-stream [RFC2045][RFC2046] */
	MIME_SUBTYPE_ODA,				/* oda [RFC2045][RFC2046] */
	MIME_SUBTYPE_OEBPS_PACKAGE_XML,			/* oebps-package+xml [RFC4839] */
	MIME_SUBTYPE_PATCH_OPS_ERROR_XML,		/* patch-ops-error+xml [RFC5261] */
	MIME_SUBTYPE_PDF,				/* pdf [RFC3778] */
	MIME_SUBTYPE_PGP_ENCRYPTED,			/* pgp-encrypted [RFC3156] */
	MIME_SUBTYPE_PGP_KEYS,				/* pgp-keys [RFC3156] */
	MIME_SUBTYPE_PGP_SIGNATURE,			/* pgp-signature [RFC3156] */
	MIME_SUBTYPE_PIDF_XML,				/* pidf+xml [RFC3863] */
	MIME_SUBTYPE_PIDF_DIFF_XML,			/* pidf-diff+xml [RFC5262] */
	MIME_SUBTYPE_PKCS10,				/* pkcs10 [RFC5967] */
	MIME_SUBTYPE_PKCS7_MIME,			/* pkcs7-mime [RFC5751] */
	MIME_SUBTYPE_PKCS7_SIGNATURE,			/* pkcs7-signature [RFC5751] */
	MIME_SUBTYPE_PKCS8,				/* pkcs8 [RFC5958] */
	MIME_SUBTYPE_PKIX_ATTR_CERT,			/* pkix-attr-cert [RFC5877] */
	MIME_SUBTYPE_PKIX_CERT,				/* pkix-cert [RFC2585] */
	MIME_SUBTYPE_PKIXCMP,				/* pkixcmp [RFC2510] */
	MIME_SUBTYPE_PKIX_CRL,				/* pkix-crl [RFC2585] */
	MIME_SUBTYPE_PKIX_PKIPATH,			/* pkix-pkipath [RFC6066] */
	MIME_SUBTYPE_PLS_XML,				/* pls+xml [RFC4267] */
	MIME_SUBTYPE_POC_SETTINGS_XML,			/* poc-settings+xml [RFC4354] */
	MIME_SUBTYPE_POSTSCRIPT,			/* postscript [RFC2045][RFC2046] */
	MIME_SUBTYPE_PRS_ALVESTRAND_TITRAX_SHEET,	/* prs.alvestrand.titrax-sheet [Alvestrand] */
	MIME_SUBTYPE_PRS_CWW,				/* prs.cww [Rungchavalnont] */
	MIME_SUBTYPE_PRS_NPREND,			/* prs.nprend [Doggett] */
	MIME_SUBTYPE_PRS_PLUCKER,			/* prs.plucker [Janssen] */
	MIME_SUBTYPE_PRS_RDF_XML_CRYPT,			/* prs.rdf-xml-crypt [Inkster] */
	MIME_SUBTYPE_PRS_XSF_XML,			/* prs.xsf+xml */
	MIME_SUBTYPE_PSKC_xml,				/* pskc+xml [RFC6030] */
	MIME_SUBTYPE_RDF_XML,				/* rdf+xml [RFC3870] */
	MIME_SUBTYPE_QSIG,				/* qsig [RFC3204] */
	MIME_SUBTYPE_REGINFO_XML,			/* reginfo+xml [RFC3680] */
	MIME_SUBTYPE_RELAX_NG_COMPACT_SYNTAX,		/* relax-ng-compact-syntax [ISO/IEC 19757-2:2003/FDAM-1] */
	MIME_SUBTYPE_REMOTE_PRINTING,			/* remote-printing [RFC1486][Rose] */
	MIME_SUBTYPE_RESOURCE_LISTS_DIFF_XML,		/* resource-lists-diff+xml [RFC5362] */
	MIME_SUBTYPE_RESOURCE_LISTS_XML,		/* resource-lists+xml [RFC4826] */
	MIME_SUBTYPE_RISCOS,				/* riscos [Smith] */
	MIME_SUBTYPE_RLMI_XML,				/* rlmi+xml [RFC4662] */
	MIME_SUBTYPE_RLS_SERVICES_XML,			/* rls-services+xml [RFC4826] */
#if 0
	MIME_SUBTYPE_, /* samlassertion+xml [OASIS Security Services Technical Committee (SSTC)] */
	MIME_SUBTYPE_, /* samlmetadata+xml [OASIS Security Services Technical Committee (SSTC)] */
	MIME_SUBTYPE_, /* sbml+xml [RFC3823] */
	MIME_SUBTYPE_, /* scvp-cv-request [RFC5055] */
	MIME_SUBTYPE_, /* scvp-cv-response [RFC5055] */
	MIME_SUBTYPE_, /* scvp-vp-request [RFC5055] */
	MIME_SUBTYPE_, /* scvp-vp-response [RFC5055] */
	MIME_SUBTYPE_, /* sdp [RFC4566] */
	MIME_SUBTYPE_, /* set-payment [Korver] */
	MIME_SUBTYPE_, /* set-payment-initiation [Korver] */
	MIME_SUBTYPE_, /* set-registration [Korver] */
	MIME_SUBTYPE_, /* set-registration-initiation [Korver] */
	MIME_SUBTYPE_, /* sgml [RFC1874] */
	MIME_SUBTYPE_, /* sgml-open-catalog [Grosso] */
	MIME_SUBTYPE_, /* shf+xml [RFC4194] */
	MIME_SUBTYPE_, /* sieve [RFC5228] */
	MIME_SUBTYPE_, /* simple-filter+xml [RFC4661] */
	MIME_SUBTYPE_, /* simple-message-summary [RFC3842] */
	MIME_SUBTYPE_, /* simpleSymbolContainer [3GPP] */
	MIME_SUBTYPE_, /* slate [Crowley] */
	MIME_SUBTYPE_, /* smil (OBSOLETE) [RFC4536] */
	MIME_SUBTYPE_, /* smil+xml [RFC4536] */
	MIME_SUBTYPE_, /* soap+fastinfoset [ITU-T ASN.1 Rapporteur] */
	MIME_SUBTYPE_, /* soap+xml [RFC3902] */
	MIME_SUBTYPE_, /* sparql-query [W3C] */
	MIME_SUBTYPE_, /* sparql-results+xml [W3C] */
	MIME_SUBTYPE_, /* spirits-event+xml [RFC3910] */
	MIME_SUBTYPE_, /* srgs [RFC4267] */
	MIME_SUBTYPE_, /* srgs+xml [RFC4267] */
	MIME_SUBTYPE_, /* sru+xml [RFC-denenberg-mods-etc-media-types-03] */
	MIME_SUBTYPE_, /* ssml+xml [RFC4267] */
	MIME_SUBTYPE_, /* tamp-apex-update [RFC5934] */
	MIME_SUBTYPE_, /* tamp-apex-update-confirm [RFC5934] */
	MIME_SUBTYPE_, /* tamp-community-update [RFC5934] */
	MIME_SUBTYPE_, /* tamp-community-update-confirm [RFC5934] */
	MIME_SUBTYPE_, /* tamp-error [RFC5934] */
	MIME_SUBTYPE_, /* tamp-sequence-adjust [RFC5934] */
	MIME_SUBTYPE_, /* tamp-sequence-adjust-confirm [RFC5934] */
	MIME_SUBTYPE_, /* tamp-status-query [RFC5934] */
	MIME_SUBTYPE_, /* tamp-status-response [RFC5934] */
	MIME_SUBTYPE_, /* tamp-update [RFC5934] */
	MIME_SUBTYPE_, /* tamp-update-confirm [RFC5934] */
	MIME_SUBTYPE_, /* tei+xml [RFC6129] */
	MIME_SUBTYPE_, /* thraud+xml [RFC5941] */
	MIME_SUBTYPE_, /* timestamp-query [RFC3161] */
	MIME_SUBTYPE_, /* timestamp-reply [RFC3161] */
	MIME_SUBTYPE_, /* timestamped-data [RFC5955] */
	MIME_SUBTYPE_, /* tve-trigger [Welsh] */
	MIME_SUBTYPE_, /* vemmi [RFC2122] */
	MIME_SUBTYPE_VND_, /* vnd.3gpp.bsf+xml [Meredith] */
	MIME_SUBTYPE_VND_, /* vnd.3gpp.pic-bw-large [Meredith] */
	MIME_SUBTYPE_VND_, /* vnd.3gpp.pic-bw-small [Meredith] */
	MIME_SUBTYPE_VND_, /* vnd.3gpp.pic-bw-var [Meredith] */
	MIME_SUBTYPE_VND_, /* vnd.3gpp.sms [Meredith] */
	MIME_SUBTYPE_VND_, /* vnd.3gpp2.bcmcsinfo+xml [Dryden] */
	MIME_SUBTYPE_VND_, /* vnd.3gpp2.sms [Mahendran] */
	MIME_SUBTYPE_VND_, /* vnd.3gpp2.tcap [Mahendran] */
	MIME_SUBTYPE_VND_, /* vnd.3M.Post-it-Notes [O'Brien] */
	MIME_SUBTYPE_VND_, /* vnd.accpac.simply.aso [Leow] */
	MIME_SUBTYPE_VND_, /* vnd.accpac.simply.imp [Leow] */
	MIME_SUBTYPE_VND_, /* vnd.acucobol [Lubin] */
	MIME_SUBTYPE_VND_, /* vnd.acucorp [Lubin] */
	MIME_SUBTYPE_VND_, /* vnd.adobe.fxp [Brambley][Heintz] */
	MIME_SUBTYPE_VND_, /* vnd.adobe.partial-upload [Otala] */
	MIME_SUBTYPE_VND_, /* vnd.adobe.xdp+xml [Brinkman] */
	MIME_SUBTYPE_VND_, /* vnd.adobe.xfdf [Perelman] */
	MIME_SUBTYPE_VND_, /* vnd.aether.imp [Moskowitz] */
	MIME_SUBTYPE_VND_, /* vnd.ah-barcode [Ichinose] */
	MIME_SUBTYPE_VND_, /* vnd.ahead.space [Kristensen] */
	MIME_SUBTYPE_VND_, /* vnd.airzip.filesecure.azf [Mould][Clueit] */
	MIME_SUBTYPE_VND_, /* vnd.airzip.filesecure.azs [Mould][Clueit] */
	MIME_SUBTYPE_VND_, /* vnd.americandynamics.acc [Sands] */
	MIME_SUBTYPE_VND_, /* vnd.amiga.ami [Blumberg] */
	MIME_SUBTYPE_VND_, /* vnd.amundsen.maze+xml [Amundsen] */
	MIME_SUBTYPE_VND_, /* vnd.anser-web-certificate-issue-initiation [Mori] */
	MIME_SUBTYPE_VND_, /* vnd.antix.game-component [Shelton] */
	MIME_SUBTYPE_VND_, /* vnd.apple.mpegurl [Singer][Pantos] */
	MIME_SUBTYPE_VND_, /* vnd.apple.installer+xml [Bierman] */
	MIME_SUBTYPE_VND_, /* vnd.arastra.swi (OBSOLETE) [Fenner] */
	MIME_SUBTYPE_VND_, /* vnd.aristanetworks.swi [Fenner] */
	MIME_SUBTYPE_VND_, /* vnd.audiograph [Slusanschi] */
	MIME_SUBTYPE_VND_, /* vnd.autopackage [Hearn] */
	MIME_SUBTYPE_VND_, /* vnd.avistar+xml [Vysotsky] */
	MIME_SUBTYPE_VND_, /* vnd.blueice.multipass [Holmstrom] */
	MIME_SUBTYPE_VND_, /* vnd.bluetooth.ep.oob [Foley] */
	MIME_SUBTYPE_VND_, /* vnd.bmi [Gotoh] */
	MIME_SUBTYPE_VND_, /* vnd.businessobjects [Imoucha] */
	MIME_SUBTYPE_VND_, /* vnd.cab-jscript [Falkenberg] */
	MIME_SUBTYPE_VND_, /* vnd.canon-cpdl [Muto] */
	MIME_SUBTYPE_VND_, /* vnd.canon-lips [Muto] */
	MIME_SUBTYPE_VND_, /* vnd.cendio.thinlinc.clientconf */
	MIME_SUBTYPE_VND_, /* vnd.chemdraw+xml [Howes] */
	MIME_SUBTYPE_VND_, /* vnd.chipnuts.karaoke-mmd [Xiong] */
	MIME_SUBTYPE_VND_, /* vnd.cinderella [Kortenkamp] */
	MIME_SUBTYPE_VND_, /* vnd.cirpack.isdn-ext [Mayeux] */
	MIME_SUBTYPE_VND_, /* vnd.claymore [Simpson] */
	MIME_SUBTYPE_VND_, /* vnd.cloanto.rp9 [Labatt] */
	MIME_SUBTYPE_VND_, /* vnd.clonk.c4group [Brammer] */
	MIME_SUBTYPE_VND_, /* vnd.cluetrust.cartomobile-config [Paulsen] */
	MIME_SUBTYPE_VND_, /* vnd.cluetrust.cartomobile-config-pkg [Paulsen] */
	MIME_SUBTYPE_VND_, /* vnd.commerce-battelle [Applebaum] */
	MIME_SUBTYPE_VND_, /* vnd.commonspace [Chandhok] */
	MIME_SUBTYPE_VND_, /* vnd.cosmocaller [Dellutri] */
	MIME_SUBTYPE_VND_, /* vnd.contact.cmsg [Patz] */
	MIME_SUBTYPE_VND_, /* vnd.crick.clicker [Burt] */
	MIME_SUBTYPE_VND_, /* vnd.crick.clicker.keyboard [Burt] */
	MIME_SUBTYPE_VND_, /* vnd.crick.clicker.palette [Burt] */
	MIME_SUBTYPE_VND_, /* vnd.crick.clicker.template [Burt] */
	MIME_SUBTYPE_VND_, /* vnd.crick.clicker.wordbank [Burt] */
	MIME_SUBTYPE_VND_, /* vnd.criticaltools.wbs+xml [Spiller] */
	MIME_SUBTYPE_VND_, /* vnd.ctc-posml [Kohlhepp] */
	MIME_SUBTYPE_VND_, /* vnd.ctct.ws+xml [Ancona] */
	MIME_SUBTYPE_VND_, /* vnd.cups-pdf [Sweet] */
	MIME_SUBTYPE_VND_, /* vnd.cups-postscript [Sweet] */
	MIME_SUBTYPE_VND_, /* vnd.cups-ppd [Sweet] */
	MIME_SUBTYPE_VND_, /* vnd.cups-raster [Sweet] */
	MIME_SUBTYPE_VND_, /* vnd.cups-raw [Sweet] */
	MIME_SUBTYPE_VND_, /* vnd.curl [Byrnes] */
	MIME_SUBTYPE_VND_, /* vnd.cybank [Helmee] */
	MIME_SUBTYPE_VND_, /* vnd.data-vision.rdz [Fields] */
	MIME_SUBTYPE_VND_, /* vnd.dece.data [Dolan] */
	MIME_SUBTYPE_VND_, /* vnd.dece.ttml+xml [Dolan] */
	MIME_SUBTYPE_VND_, /* vnd.dece.unspecified [Dolan] */
	MIME_SUBTYPE_VND_, /* vnd.denovo.fcselayout-link [Dixon] */
	MIME_SUBTYPE_VND_, /* vnd.dir-bi.plate-dl-nosuffix [Yamanaka] */
	MIME_SUBTYPE_VND_, /* vnd.dna [Searcy] */
	MIME_SUBTYPE_VND_, /* vnd.dolby.mobile.1 [Hattersley] */
	MIME_SUBTYPE_VND_, /* vnd.dolby.mobile.2 [Hattersley] */
	MIME_SUBTYPE_VND_, /* vnd.dpgraph [Parker] */
	MIME_SUBTYPE_VND_, /* vnd.dreamfactory [Appleton] */
	MIME_SUBTYPE_VND_, /* vnd.dvb.ait [Siebert] */
	MIME_SUBTYPE_VND_, /* vnd.dvb.dvbj [Siebert] */
	MIME_SUBTYPE_VND_, /* vnd.dvb.esgcontainer [Heuer] */
	MIME_SUBTYPE_VND_, /* vnd.dvb.ipdcdftnotifaccess [Yue] */
	MIME_SUBTYPE_VND_, /* vnd.dvb.ipdcesgaccess [Heuer] */
	MIME_SUBTYPE_VND_, /* vnd.dvb.ipdcesgaccess2 [Marcon] */
	MIME_SUBTYPE_VND_, /* vnd.dvb.ipdcesgpdd [Marcon] */
	MIME_SUBTYPE_VND_, /* vnd.dvb.ipdcroaming [Xu] */
	MIME_SUBTYPE_VND_, /* vnd.dvb.iptv.alfec-base [Henry] */
	MIME_SUBTYPE_VND_, /* vnd.dvb.iptv.alfec-enhancement [Henry] */
	MIME_SUBTYPE_VND_, /* vnd.dvb.notif-aggregate-root+xml [Yue] */
	MIME_SUBTYPE_VND_, /* vnd.dvb.notif-container+xml [Yue] */
	MIME_SUBTYPE_VND_, /* vnd.dvb.notif-generic+xml [Yue] */
	MIME_SUBTYPE_VND_, /* vnd.dvb.notif-ia-msglist+xml [Yue] */
	MIME_SUBTYPE_VND_, /* vnd.dvb.notif-ia-registration-request+xml [Yue] */
	MIME_SUBTYPE_VND_, /* vnd.dvb.notif-ia-registration-response+xml [Yue] */
	MIME_SUBTYPE_VND_, /* vnd.dvb.notif-init+xml [Yue] */
	MIME_SUBTYPE_VND_, /* vnd.dvb.pfr [Siebert] */
	MIME_SUBTYPE_VND_, /* vnd.dvb.service [Siebert] */
#endif
	MIME_SUBTYPE_VND_DXR,				/* vnd.dxr [Duffy] */
	MIME_SUBTYPE_VND_DYNAGEO,			/* vnd.dynageo [Mechling] */
	MIME_SUBTYPE_VND_EASYKARAOKE_CDGDOWNLOAD,	/* vnd.easykaraoke.cdgdownload [Downs] */
	MIME_SUBTYPE_VND_ECDIS_UPDATE,			/* vnd.ecdis-update [Buettgenbach] */
	MIME_SUBTYPE_VND_ECOWIN_CHART,			/* vnd.ecowin.chart [Olsson] */
	MIME_SUBTYPE_VND_ECOWIN_FILEREQUEST,		/* vnd.ecowin.filerequest [Olsson] */
	MIME_SUBTYPE_VND_ECOWIN_FILEUPDATE,		/* vnd.ecowin.fileupdate [Olsson] */
	MIME_SUBTYPE_VND_ECOWIN_SERIES,			/* vnd.ecowin.series [Olsson] */
	MIME_SUBTYPE_VND_ECOWIN_SERIESREQUEST,		/* vnd.ecowin.seriesrequest [Olsson] */
	MIME_SUBTYPE_VND_ECOWIN_SERIESUPDATE,		/* vnd.ecowin.seriesupdate [Olsson] */
	MIME_SUBTYPE_VND_EMCLIENT_ACCESSREQUEST_XML,	/* vnd.emclient.accessrequest+xml [Navara] */
	MIME_SUBTYPE_VND_ENLIVEN,			/* vnd.enliven [Santinelli] */
	MIME_SUBTYPE_VND_EPSON_ESF,			/* vnd.epson.esf [Hoshina] */
	MIME_SUBTYPE_VND_EPSON_MSF,			/* vnd.epson.msf [Hoshina] */
	MIME_SUBTYPE_VND_EPSON_QUICKANIME,		/* vnd.epson.quickanime [Gu] */
	MIME_SUBTYPE_VND_EPSON_SALT,			/* vnd.epson.salt [Nagatomo] */
	MIME_SUBTYPE_VND_EPSON_SSF,			/* vnd.epson.ssf [Hoshina] */
	MIME_SUBTYPE_VND_ERICSSON_QUICKCALL,		/* vnd.ericsson.quickcall [Tidwell] */
	MIME_SUBTYPE_VND_ESZIGNO3_XML,			/* vnd.eszigno3+xml */
#if 0
	MIME_SUBTYPE_VND_ETSI_, /* vnd.etsi.aoc+xml [Hu] */
	MIME_SUBTYPE_VND_ETSI_, /* vnd.etsi.cug+xml [Hu] */
	MIME_SUBTYPE_VND_ETSI_, /* vnd.etsi.iptvcommand+xml [Hu] */
	MIME_SUBTYPE_VND_ETSI_, /* vnd.etsi.iptvdiscovery+xml [Hu] */
	MIME_SUBTYPE_VND_ETSI_, /* vnd.etsi.iptvprofile+xml [Hu] */
	MIME_SUBTYPE_VND_ETSI_, /* vnd.etsi.iptvsad-bc+xml [Hu] */
	MIME_SUBTYPE_VND_ETSI_, /* vnd.etsi.iptvsad-cod+xml [Hu] */
	MIME_SUBTYPE_VND_ETSI_, /* vnd.etsi.iptvsad-npvr+xml [Hu] */
	MIME_SUBTYPE_VND_ETSI_, /* vnd.etsi.iptvservice+xml [Ortega] */
	MIME_SUBTYPE_VND_ETSI_, /* vnd.etsi.iptvsync+xml [Ortega] */
	MIME_SUBTYPE_VND_ETSI_, /* vnd.etsi.iptvueprofile+xml [Hu] */
	MIME_SUBTYPE_VND_ETSI_, /* vnd.etsi.mcid+xml [Hu] */
	MIME_SUBTYPE_VND_ETSI_, /* vnd.etsi.overload-control-policy-dataset+xml [Ortega] */
	MIME_SUBTYPE_VND_ETSI_, /* vnd.etsi.sci+xml */
	MIME_SUBTYPE_VND_ETSI_, /* vnd.etsi.simservs+xml [Hu] */
	MIME_SUBTYPE_VND_ETSI_, /* vnd.etsi.tsl+xml [Hu] */
	MIME_SUBTYPE_VND_ETSI_, /* vnd.etsi.tsl.der [Hu] */
#endif
	MIME_SUBTYPE_VND_EUDORA_DATA,		/* vnd.eudora.data [Resnick] */
#if 0
	MIME_SUBTYPE_VND_, /* vnd.ezpix-album [Electronic Zombie, Corp.] */
	MIME_SUBTYPE_VND_, /* vnd.ezpix-package [Electronic Zombie, Corp.] */
	MIME_SUBTYPE_VND_, /* vnd.f-secure.mobile [Sarivaara] */
	MIME_SUBTYPE_VND_, /* vnd.fdf [Zilles] */
	MIME_SUBTYPE_VND_, /* vnd.fdsn.mseed [Trabant] */
	MIME_SUBTYPE_VND_, /* vnd.fdsn.seed [Trabant] */
	MIME_SUBTYPE_VND_, /* vnd.ffsns [Holstage] */
	MIME_SUBTYPE_VND_, /* vnd.fints [Hammann] */
	MIME_SUBTYPE_VND_, /* vnd.FloGraphIt [Floersch] */
	MIME_SUBTYPE_VND_, /* vnd.fluxtime.clip [Winter] */
	MIME_SUBTYPE_VND_, /* vnd.font-fontforge-sfd [Williams] */
	MIME_SUBTYPE_VND_, /* vnd.framemaker [Wexler] */
	MIME_SUBTYPE_VND_, /* vnd.frogans.fnc [Tamas] */
	MIME_SUBTYPE_VND_, /* vnd.frogans.ltf [Tamas] */
	MIME_SUBTYPE_VND_, /* vnd.fsc.weblaunch [D.Smith] */
	MIME_SUBTYPE_VND_, /* vnd.fujitsu.oasys [Togashi] */
	MIME_SUBTYPE_VND_, /* vnd.fujitsu.oasys2 [Togashi] */
	MIME_SUBTYPE_VND_, /* vnd.fujitsu.oasys3 [Okudaira] */
	MIME_SUBTYPE_VND_, /* vnd.fujitsu.oasysgp [Sugimoto] */
	MIME_SUBTYPE_VND_, /* vnd.fujitsu.oasysprs [Ogita] */
	MIME_SUBTYPE_VND_, /* vnd.fujixerox.ART4 [Tanabe] */
	MIME_SUBTYPE_VND_, /* vnd.fujixerox.ART-EX [Tanabe] */
	MIME_SUBTYPE_VND_, /* vnd.fujixerox.ddd [Onda] */
	MIME_SUBTYPE_VND_, /* vnd.fujixerox.docuworks [Taguchi] */
	MIME_SUBTYPE_VND_, /* vnd.fujixerox.docuworks.binder [Matsumoto] */
	MIME_SUBTYPE_VND_, /* vnd.fujixerox.HBPL [Tanabe] */
	MIME_SUBTYPE_VND_, /* vnd.fut-misnet [Pruulmann] */
	MIME_SUBTYPE_VND_, /* vnd.fuzzysheet [Birtwistle] */
	MIME_SUBTYPE_VND_, /* vnd.genomatix.tuxedo [Frey] */
	MIME_SUBTYPE_VND_, /* vnd.geocube+xml [Pirsch] */
	MIME_SUBTYPE_VND_, /* vnd.geogebra.file [GeoGebra][Kreis] */
	MIME_SUBTYPE_VND_, /* vnd.geogebra.tool [GeoGebra][Kreis] */
	MIME_SUBTYPE_VND_, /* vnd.geometry-explorer [Hvidsten] */
	MIME_SUBTYPE_VND_, /* vnd.geonext [Ehmann] */
	MIME_SUBTYPE_VND_, /* vnd.geoplan [Mercat] */
	MIME_SUBTYPE_VND_, /* vnd.geospace [Mercat] */
	MIME_SUBTYPE_VND_, /* vnd.globalplatform.card-content-mgt [Bernabeu] */
	MIME_SUBTYPE_VND_, /* vnd.globalplatform.card-content-mgt-response [Bernabeu] */
#endif
	MIME_SUBTYPE_VND_GMX,				/* vnd.gmx (OBSOLETE) [Sciberras] */
	MIME_SUBTYPE_VND_GOOGLE_EARTH_KML_XML,		/* vnd.google-earth.kml+xml [Ashbridge] */
	MIME_SUBTYPE_VND_GOOGLE_EARTH_KMZ,		/* vnd.google-earth.kmz [Ashbridge] */
	MIME_SUBTYPE_VND_GRAFEQ,			/* vnd.grafeq [Tupper] */
#if 0
	MIME_SUBTYPE_VND_, /* vnd.gridmp [Lawson] */
	MIME_SUBTYPE_VND_, /* vnd.groove-account [Joseph] */
	MIME_SUBTYPE_VND_, /* vnd.groove-help [Joseph] */
	MIME_SUBTYPE_VND_, /* vnd.groove-identity-message [Joseph] */
	MIME_SUBTYPE_VND_, /* vnd.groove-injector [Joseph] */
	MIME_SUBTYPE_VND_, /* vnd.groove-tool-message [Joseph] */
	MIME_SUBTYPE_VND_, /* vnd.groove-tool-template [Joseph] */
	MIME_SUBTYPE_VND_, /* vnd.groove-vcard [Joseph] */
	MIME_SUBTYPE_VND_, /* vnd.hal+xml [Kelly] */
	MIME_SUBTYPE_VND_, /* vnd.HandHeld-Entertainment+xml [Hamilton] */
	MIME_SUBTYPE_VND_, /* vnd.hbci [Hammann] */
	MIME_SUBTYPE_VND_, /* vnd.hcl-bireports [Serres] */
	MIME_SUBTYPE_VND_, /* vnd.hhe.lesson-player [Jones] */
	MIME_SUBTYPE_VND_, /* vnd.hp-HPGL [Pentecost] */
	MIME_SUBTYPE_VND_, /* vnd.hp-hpid [Gupta] */
	MIME_SUBTYPE_VND_, /* vnd.hp-hps [Aubrey] */
	MIME_SUBTYPE_VND_, /* vnd.hp-jlyt [Gaash] */
	MIME_SUBTYPE_VND_, /* vnd.hp-PCL [Pentecost] */
	MIME_SUBTYPE_VND_, /* vnd.hp-PCLXL [Pentecost] */
	MIME_SUBTYPE_VND_, /* vnd.httphone [Lefevre] */
	MIME_SUBTYPE_VND_, /* vnd.hydrostatix.sof-data [Gillam] */
	MIME_SUBTYPE_VND_, /* vnd.hzn-3d-crossword [Minnis] */
	MIME_SUBTYPE_VND_, /* vnd.ibm.afplinedata [Buis] */
	MIME_SUBTYPE_VND_, /* vnd.ibm.electronic-media [Tantlinger] */
	MIME_SUBTYPE_VND_, /* vnd.ibm.MiniPay [Herzberg] */
	MIME_SUBTYPE_VND_, /* vnd.ibm.modcap [Hohensee] */
	MIME_SUBTYPE_VND_, /* vnd.ibm.rights-management [Tantlinger] */
	MIME_SUBTYPE_VND_, /* vnd.ibm.secure-container [Tantlinger] */
	MIME_SUBTYPE_VND_, /* vnd.iccprofile [Green] */
	MIME_SUBTYPE_VND_, /* vnd.igloader [Fisher] */
	MIME_SUBTYPE_VND_, /* vnd.immervision-ivp [Villegas] */
	MIME_SUBTYPE_VND_, /* vnd.immervision-ivu [Villegas] */
	MIME_SUBTYPE_VND_, /* vnd.informedcontrol.rms+xml [Wahl] */
	MIME_SUBTYPE_VND_, /* vnd.infotech.project [Engelke] */
	MIME_SUBTYPE_VND_, /* vnd.infotech.project+xml [Engelke] */
	MIME_SUBTYPE_VND_, /* vnd.informix-visionary [Gales] */
	MIME_SUBTYPE_VND_, /* vnd.insors.igm 	[Swanson] */
	MIME_SUBTYPE_VND_, /* vnd.intercon.formnet 	[Gurak] */
	MIME_SUBTYPE_VND_, /* vnd.intergeo 	[Kreis] */
	MIME_SUBTYPE_VND_, /* vnd.intertrust.digibox 	[Tomasello] */
	MIME_SUBTYPE_VND_, /* vnd.intertrust.nncp 	[Tomasello] */
	MIME_SUBTYPE_VND_, /* vnd.intu.qbo 	[Scratchley] */
	MIME_SUBTYPE_VND_, /* vnd.intu.qfx 	[Scratchley] */
	MIME_SUBTYPE_VND_, /* vnd.iptc.g2.conceptitem+xml 	[Steidl] */
	MIME_SUBTYPE_VND_, /* vnd.iptc.g2.knowledgeitem+xml 	[Steidl] */
	MIME_SUBTYPE_VND_, /* vnd.iptc.g2.newsitem+xml 	[Steidl] */
	MIME_SUBTYPE_VND_, /* vnd.iptc.g2.packageitem+xml 	[Steidl] */
	MIME_SUBTYPE_VND_, /* vnd.ipunplugged.rcprofile 	[Ersson] */
	MIME_SUBTYPE_VND_, /* vnd.irepository.package+xml 	[Knowles] */
	MIME_SUBTYPE_VND_, /* vnd.is-xpr 	[Natarajan] */
	MIME_SUBTYPE_VND_, /* vnd.isac.fcs 	[RBrinkman] */
	MIME_SUBTYPE_VND_, /* vnd.jam 	[B.Kumar] */
	MIME_SUBTYPE_VND_, /* vnd.japannet-directory-service 	[Fujii] */
	MIME_SUBTYPE_VND_, /* vnd.japannet-jpnstore-wakeup 	[Yoshitake] */
	MIME_SUBTYPE_VND_, /* vnd.japannet-payment-wakeup 	[Fujii] */
	MIME_SUBTYPE_VND_, /* vnd.japannet-registration 	[Yoshitake] */
	MIME_SUBTYPE_VND_, /* vnd.japannet-registration-wakeup 	[Fujii] */
	MIME_SUBTYPE_VND_, /* vnd.japannet-setstore-wakeup 	[Yoshitake] */
	MIME_SUBTYPE_VND_, /* vnd.japannet-verification 	[Yoshitake] */
	MIME_SUBTYPE_VND_, /* vnd.japannet-verification-wakeup 	[Fujii] */
	MIME_SUBTYPE_VND_, /* vnd.jcp.javame.midlet-rms 	[Gorshenev] */
	MIME_SUBTYPE_VND_, /* vnd.jisp 	[Deckers] */
	MIME_SUBTYPE_VND_, /* vnd.joost.joda-archive 	[Joost] */
	MIME_SUBTYPE_VND_, /* vnd.kahootz 	[Macdonald] */
	MIME_SUBTYPE_VND_, /* vnd.kde.karbon 	[Faure] */
	MIME_SUBTYPE_VND_, /* vnd.kde.kchart 	[Faure] */
	MIME_SUBTYPE_VND_, /* vnd.kde.kformula 	[Faure] */
	MIME_SUBTYPE_VND_, /* vnd.kde.kivio 	[Faure] */
	MIME_SUBTYPE_VND_, /* vnd.kde.kontour 	[Faure] */
	MIME_SUBTYPE_VND_, /* vnd.kde.kpresenter 	[Faure] */
	MIME_SUBTYPE_VND_, /* vnd.kde.kspread 	[Faure] */
	MIME_SUBTYPE_VND_, /* vnd.kde.kword 	[Faure] */
	MIME_SUBTYPE_VND_, /* vnd.kenameaapp 	[DiGiorgio-Haag] */
	MIME_SUBTYPE_VND_, /* vnd.kidspiration 	[Bennett] */
	MIME_SUBTYPE_VND_, /* vnd.Kinar 	[Thakkar] */
	MIME_SUBTYPE_VND_, /* vnd.koan 	[Cole] */
	MIME_SUBTYPE_VND_, /* vnd.kodak-descriptor 	[Donahue] */
	MIME_SUBTYPE_VND_, /* vnd.las.las+xml 	[Bailey] */
	MIME_SUBTYPE_VND_, /* vnd.liberty-request+xml 	[McDowell] */
	MIME_SUBTYPE_VND_, /* vnd.llamagraphics.life-balance.desktop 	[White] */
	MIME_SUBTYPE_VND_, /* vnd.llamagraphics.life-balance.exchange+xml 	[White] */
	MIME_SUBTYPE_VND_, /* vnd.lotus-1-2-3 	[Wattenberger] */
	MIME_SUBTYPE_VND_, /* vnd.lotus-approach 	[Wattenberger] */
	MIME_SUBTYPE_VND_, /* vnd.lotus-freelance 	[Wattenberger] */
	MIME_SUBTYPE_VND_, /* vnd.lotus-notes 	[Laramie] */
	MIME_SUBTYPE_VND_, /* vnd.lotus-organizer 	[Wattenberger] */
	MIME_SUBTYPE_VND_, /* vnd.lotus-screencam 	[Wattenberger] */
	MIME_SUBTYPE_VND_, /* vnd.lotus-wordpro 	[Wattenberger] */
	MIME_SUBTYPE_VND_, /* vnd.macports.portpkg 	[Berry] */
	MIME_SUBTYPE_VND_, /* vnd.marlin.drm.actiontoken+xml 	[Ellison] */
	MIME_SUBTYPE_VND_, /* vnd.marlin.drm.conftoken+xml 	[Ellison] */
	MIME_SUBTYPE_VND_, /* vnd.marlin.drm.license+xml 	[Ellison] */
	MIME_SUBTYPE_VND_, /* vnd.marlin.drm.mdcf 	[Ellison] */
#endif
	MIME_SUBTYPE_VND_MCD,			/* vnd.mcd [Gotoh] */
	MIME_SUBTYPE_VND_MEDCALCDATA,		/* vnd.medcalcdata [Schoonjans] */
	MIME_SUBTYPE_VND_MEDIASTATION_CDKEY,	/* vnd.mediastation.cdkey [Flurry] */
	MIME_SUBTYPE_VND_MERIDIAN_SLINGSHOT,	/* vnd.meridian-slingshot [Wedel] */
	MIME_SUBTYPE_VND_MFER,			/* vnd.MFER [Hirai] */
#if 0
	MIME_SUBTYPE_VND_, /* vnd.mfmp [Ikeda] */
	MIME_SUBTYPE_VND_, /* vnd.micrografx.flo [Prevo] */
	MIME_SUBTYPE_VND_, /* vnd.micrografx.igx [Prevo] */
	MIME_SUBTYPE_VND_, /* vnd.mif [Wexler] */
	MIME_SUBTYPE_VND_, /* vnd.minisoft-hp3000-save [Bartram] */
	MIME_SUBTYPE_VND_, /* vnd.mitsubishi.misty-guard.trustweb 	[Tanaka] */
	MIME_SUBTYPE_VND_, /* vnd.Mobius.DAF 	[Kabayama] */
	MIME_SUBTYPE_VND_, /* vnd.Mobius.DIS 	[Kabayama] */
	MIME_SUBTYPE_VND_, /* vnd.Mobius.MBK 	[Devasia] */
	MIME_SUBTYPE_VND_, /* vnd.Mobius.MQY 	[Devasia] */
	MIME_SUBTYPE_VND_, /* vnd.Mobius.MSL 	[Kabayama] */
	MIME_SUBTYPE_VND_, /* vnd.Mobius.PLC 	[Kabayama] */
	MIME_SUBTYPE_VND_, /* vnd.Mobius.TXF 	[Kabayama] */
	MIME_SUBTYPE_VND_, /* vnd.mophun.application 	[Wennerstrom] */
	MIME_SUBTYPE_VND_, /* vnd.mophun.certificate 	[Wennerstrom] */
	MIME_SUBTYPE_VND_, /* vnd.motorola.flexsuite 	[Patton] */
	MIME_SUBTYPE_VND_, /* vnd.motorola.flexsuite.adsi 	[Patton] */
	MIME_SUBTYPE_VND_, /* vnd.motorola.flexsuite.fis 	[Patton] */
	MIME_SUBTYPE_VND_, /* vnd.motorola.flexsuite.gotap 	[Patton] */
	MIME_SUBTYPE_VND_, /* vnd.motorola.flexsuite.kmr 	[Patton] */
	MIME_SUBTYPE_VND_, /* vnd.motorola.flexsuite.ttc 	[Patton] */
	MIME_SUBTYPE_VND_, /* vnd.motorola.flexsuite.wem 	[Patton] */
	MIME_SUBTYPE_VND_, /* vnd.motorola.iprm 	[Shamsaasef] */
	MIME_SUBTYPE_VND_, /* vnd.mozilla.xul+xml 	[McDaniel] */
	MIME_SUBTYPE_VND_, /* vnd.ms-artgalry 	[Slawson] */
	MIME_SUBTYPE_VND_, /* vnd.ms-asf 	[Fleischman] */
	MIME_SUBTYPE_VND_, /* vnd.ms-cab-compressed 	[Scarborough] */
	MIME_SUBTYPE_VND_, /* vnd.mseq 	[Le Bodic] */
	MIME_SUBTYPE_VND_, /* vnd.ms-excel 	[Gill] */
	MIME_SUBTYPE_VND_, /* vnd.ms-excel.addin.macroEnabled.12 	[Rae] */
	MIME_SUBTYPE_VND_, /* vnd.ms-excel.sheet.binary.macroEnabled.12 	[Rae] */
	MIME_SUBTYPE_VND_, /* vnd.ms-excel.sheet.macroEnabled.12 	[Rae] */
	MIME_SUBTYPE_VND_, /* vnd.ms-excel.template.macroEnabled.12 	[Rae] */
	MIME_SUBTYPE_VND_, /* vnd.ms-fontobject 	[Scarborough] */
	MIME_SUBTYPE_VND_, /* vnd.ms-htmlhelp 	[Techtonik] */
	MIME_SUBTYPE_VND_, /* vnd.ms-ims 	[Ledoux] */
	MIME_SUBTYPE_VND_, /* vnd.ms-lrm 	[Ledoux] */
	MIME_SUBTYPE_VND_, /* vnd.ms-office.activeX+xml 	[Rae] */
	MIME_SUBTYPE_VND_, /* vnd.ms-officetheme 	[Rae] */
	MIME_SUBTYPE_VND_, /* vnd.ms-playready.initiator+xml 	[Schneider] */
	MIME_SUBTYPE_VND_, /* vnd.ms-powerpoint 	[Gill] */
	MIME_SUBTYPE_VND_, /* vnd.ms-powerpoint.addin.macroEnabled.12 	[Rae] */
	MIME_SUBTYPE_VND_, /* vnd.ms-powerpoint.presentation.macroEnabled.12 	[Rae] */
	MIME_SUBTYPE_VND_, /* vnd.ms-powerpoint.slide.macroEnabled.12 	[Rae] */
	MIME_SUBTYPE_VND_, /* vnd.ms-powerpoint.slideshow.macroEnabled.12 	[Rae] */
	MIME_SUBTYPE_VND_, /* vnd.ms-powerpoint.template.macroEnabled.12 	[Rae] */
	MIME_SUBTYPE_VND_, /* vnd.ms-project 	[Gill] */
	MIME_SUBTYPE_VND_, /* vnd.ms-tnef 	[Gill] */
	MIME_SUBTYPE_VND_, /* vnd.ms-wmdrm.lic-chlg-req 	[Lau] */
	MIME_SUBTYPE_VND_, /* vnd.ms-wmdrm.lic-resp 	[Lau] */
	MIME_SUBTYPE_VND_, /* vnd.ms-wmdrm.meter-chlg-req 	[Lau] */
	MIME_SUBTYPE_VND_, /* vnd.ms-wmdrm.meter-resp 	[Lau] */
	MIME_SUBTYPE_VND_, /* vnd.ms-word.document.macroEnabled.12 	[Rae] */
	MIME_SUBTYPE_VND_, /* vnd.ms-word.template.macroEnabled.12 	[Rae] */
	MIME_SUBTYPE_VND_, /* vnd.ms-works 	[Gill] */
	MIME_SUBTYPE_VND_, /* vnd.ms-wpl 	[Plastina] */
	MIME_SUBTYPE_VND_, /* vnd.ms-xpsdocument 	[McGatha] */
	MIME_SUBTYPE_VND_, /* vnd.msign 	[Borcherding] */
	MIME_SUBTYPE_VND_, /* vnd.multiad.creator 	[Mills] */
	MIME_SUBTYPE_VND_, /* vnd.multiad.creator.cif 	[Mills] */
	MIME_SUBTYPE_VND_, /* vnd.musician 	[Adams] */
	MIME_SUBTYPE_VND_, /* vnd.music-niff 	[Butler] */
	MIME_SUBTYPE_VND_, /* vnd.muvee.style 	[Anantharamu] */
	MIME_SUBTYPE_VND_, /* vnd.ncd.control 	[Tarkkala] */
	MIME_SUBTYPE_VND_, /* vnd.ncd.reference 	[Tarkkala] */
	MIME_SUBTYPE_VND_, /* vnd.nervana 	[Judkins] */
	MIME_SUBTYPE_VND_, /* vnd.netfpx 	[Mutz] */
	MIME_SUBTYPE_VND_, /* vnd.neurolanguage.nlu 	[DuFeu] */
	MIME_SUBTYPE_VND_, /* vnd.noblenet-directory 	[Solomon] */
	MIME_SUBTYPE_VND_, /* vnd.noblenet-sealer 	[Solomon] */
	MIME_SUBTYPE_VND_, /* vnd.noblenet-web 	[Solomon] */
	MIME_SUBTYPE_VND_, /* vnd.nokia.catalogs 	[Nokia] */
	MIME_SUBTYPE_VND_, /* vnd.nokia.conml+wbxml 	[Nokia] */
	MIME_SUBTYPE_VND_, /* vnd.nokia.conml+xml 	[Nokia] */
	MIME_SUBTYPE_VND_, /* vnd.nokia.iptv.config+xml 	[Nokia] */
	MIME_SUBTYPE_VND_, /* vnd.nokia.iSDS-radio-presets 	[Nokia] */
	MIME_SUBTYPE_VND_, /* vnd.nokia.landmark+wbxml 	[Nokia] */
	MIME_SUBTYPE_VND_, /* vnd.nokia.landmark+xml 	[Nokia] */
	MIME_SUBTYPE_VND_, /* vnd.nokia.landmarkcollection+xml 	[Nokia] */
	MIME_SUBTYPE_VND_, /* vnd.nokia.ncd 	[Nokia] */
	MIME_SUBTYPE_VND_, /* vnd.nokia.n-gage.ac+xml 	[Nokia] */
	MIME_SUBTYPE_VND_, /* vnd.nokia.n-gage.data 	[Nokia] */
	MIME_SUBTYPE_VND_, /* vnd.nokia.n-gage.symbian.install 	[Nokia] */
	MIME_SUBTYPE_VND_, /* vnd.nokia.pcd+wbxml 	[Nokia] */
	MIME_SUBTYPE_VND_, /* vnd.nokia.pcd+xml 	[Nokia] */
	MIME_SUBTYPE_VND_, /* vnd.nokia.radio-preset 	[Nokia] */
	MIME_SUBTYPE_VND_, /* vnd.nokia.radio-presets 	[Nokia] */
	MIME_SUBTYPE_VND_, /* vnd.novadigm.EDM 	[Swenson] */
	MIME_SUBTYPE_VND_, /* vnd.novadigm.EDX 	[Swenson] */
	MIME_SUBTYPE_VND_, /* vnd.novadigm.EXT 	[Swenson] */
	MIME_SUBTYPE_VND_, /* vnd.ntt-local.file-transfer 	[NTT-local] */
	MIME_SUBTYPE_VND_, /* vnd.ntt-local.sip-ta_remote 	[NTT-local] */
	MIME_SUBTYPE_VND_, /* vnd.ntt-local.sip-ta_tcp_stream 	[NTT-local] */
#endif
	MIME_SUBTYPE_VND_OASIS_OPENDOCUMENT_CHART,		/* vnd.oasis.opendocument.chart [Schubert][OASIS] */
	MIME_SUBTYPE_VND_OASIS_OPENDOCUMENT_CHART_TEMPLATE,	/* vnd.oasis.opendocument.chart-template [Schubert][OASIS] */
	MIME_SUBTYPE_VND_OASIS_OPENDOCUMENT_DATABASE,		/* vnd.oasis.opendocument.database [Schubert][OASIS] */
	MIME_SUBTYPE_VND_OASIS_OPENDOCUMENT_FORMULA,		/* vnd.oasis.opendocument.formula [Schubert][OASIS] */
	MIME_SUBTYPE_VND_OASIS_OPENDOCUMENT_FORMULA_TEMPLATE,	/* vnd.oasis.opendocument.formula-template [Schubert][OASIS] */
	MIME_SUBTYPE_VND_OASIS_OPENDOCUMENT_GRAPHICS,		/* vnd.oasis.opendocument.graphics [Schubert][OASIS] */
	MIME_SUBTYPE_VND_OASIS_OPENDOCUMENT_GRAPHICS_TEMPLATE,	/* vnd.oasis.opendocument.graphics-template [Schubert][OASIS] */
	MIME_SUBTYPE_VND_OASIS_OPENDOCUMENT_IMAGE,		/* vnd.oasis.opendocument.image [Schubert][OASIS] */
	MIME_SUBTYPE_VND_OASIS_OPENDOCUMENT_IMAGE_TEMPLATE,	/* vnd.oasis.opendocument.image-template [Schubert][OASIS] */
	MIME_SUBTYPE_VND_OASIS_OPENDOCUMENT_PRESENTATION,		/* vnd.oasis.opendocument.presentation [Schubert][OASIS] */
	MIME_SUBTYPE_VND_OASIS_OPENDOCUMENT_PRESENTATION_TEMPLATE,	/* vnd.oasis.opendocument.presentation-template [Schubert][OASIS] */
	MIME_SUBTYPE_VND_OASIS_OPENDOCUMENT_SPREADSHEET,		/* vnd.oasis.opendocument.spreadsheet [Schubert][OASIS] */
	MIME_SUBTYPE_VND_OASIS_OPENDOCUMENT_SPREADSHEET_TEMPLATE,	/* vnd.oasis.opendocument.spreadsheet-template [Schubert][OASIS] */
	MIME_SUBTYPE_VND_OASIS_OPENDOCUMENT_TEXT,			/* vnd.oasis.opendocument.text [Schubert][OASIS] */
	MIME_SUBTYPE_VND_OASIS_OPENDOCUMENT_TEXT_MASTER,		/* vnd.oasis.opendocument.text-master [Schubert][OASIS] */
	MIME_SUBTYPE_VND_OASIS_OPENDOCUMENT_TEXT_TEMPLATE,		/* vnd.oasis.opendocument.text-template [Schubert][OASIS] */
	MIME_SUBTYPE_VND_OASIS_OPENDOCUMENT_TEXT_WEB,			/* vnd.oasis.opendocument.text-web [Schubert][OASIS] */
	MIME_SUBTYPE_VND_OBN,						/* vnd.obn [Hessling] */
	MIME_SUBTYPE_VND_OIPF_CONTENTACCESSDOWNLOAD_XML,		/* vnd.oipf.contentaccessdownload+xml [D'Esclercs] */
	MIME_SUBTYPE_VND_OIPF_CONTENTACCESSSTREAMING_XML,		/* vnd.oipf.contentaccessstreaming+xml [D'Esclercs] */
	MIME_SUBTYPE_VND_OIPF_CSPG_HEXBINARY,		/* vnd.oipf.cspg-hexbinary [D'Esclercs] */
	MIME_SUBTYPE_VND_OIPF_DAE_SVG_XML,		/* vnd.oipf.dae.svg+xml [D'Esclercs] */
	MIME_SUBTYPE_VND_OIPF_DAE_XHTML_XML,		/* vnd.oipf.dae.xhtml+xml [D'Esclercs] */
	MIME_SUBTYPE_VND_OIPF_MIPPVCONTROLMESSAGE_XML,	/* vnd.oipf.mippvcontrolmessage+xml [D'Esclercs] */
	MIME_SUBTYPE_VND_OIPF_PAE_GEM,			/* vnd.oipf.pae.gem [D'Esclercs] */
	MIME_SUBTYPE_VND_OIPF_SPDISCOVERY_XML,		/* vnd.oipf.spdiscovery+xml [D'Esclercs] */
	MIME_SUBTYPE_VND_OIPF_SPDLIST_XML,		/* vnd.oipf.spdlist+xml [D'Esclercs] */
	MIME_SUBTYPE_VND_OIPF_UEPROFILE_XML,		/* vnd.oipf.ueprofile+xml [D'Esclercs] */
	MIME_SUBTYPE_VND_OIPF_USERPROFILE_XML,		/* vnd.oipf.userprofile+xml [D'Esclercs] */
	MIME_SUBTYPE_VND_OLPC_SUGAR,			/* vnd.olpc-sugar [Palmieri] */
#if 0
	MIME_SUBTYPE_VND_,		  /* vnd.oma.bcast.associated-procedure-parameter+xml 	[Rauschenbach][OMNA - Open Mobile Naming Authority] */
	MIME_SUBTYPE_VND_,		  /* vnd.oma.bcast.drm-trigger+xml 	[Rauschenbach][OMNA - Open Mobile Naming Authority] */
	MIME_SUBTYPE_VND_,		  /* vnd.oma.bcast.imd+xml 	[Rauschenbach][OMNA - Open Mobile Naming Authority] */
	MIME_SUBTYPE_VND_,		  /* vnd.oma.bcast.ltkm 	[Rauschenbach][OMNA - Open Mobile Naming Authority] */
	MIME_SUBTYPE_VND_,		  /* vnd.oma.bcast.notification+xml 	[Rauschenbach][OMNA - Open Mobile Naming Authority] */
	MIME_SUBTYPE_VND_,		  /* vnd.oma.bcast.provisioningtrigger 	[Rauschenbach][OMNA - Open Mobile Naming Authority] */
	MIME_SUBTYPE_VND_,		  /* vnd.oma.bcast.sgboot 	[Rauschenbach][OMNA - Open Mobile Naming Authority] */
	MIME_SUBTYPE_VND_,		  /* vnd.oma.bcast.sgdd+xml 	[Rauschenbach][OMNA - Open Mobile Naming Authority] */
	MIME_SUBTYPE_VND_,		  /* vnd.oma.bcast.sgdu 	[Rauschenbach][OMNA - Open Mobile Naming Authority] */
	MIME_SUBTYPE_VND_,		  /* vnd.oma.bcast.simple-symbol-container 	[Rauschenbach][OMNA - Open Mobile Naming Authority] */
	MIME_SUBTYPE_VND_,		  /* vnd.oma.bcast.smartcard-trigger+xml 	[Rauschenbach][OMNA - Open Mobile Naming Authority] */
	MIME_SUBTYPE_VND_,		  /* vnd.oma.bcast.sprov+xml 	[Rauschenbach][OMNA - Open Mobile Naming Authority] */
	MIME_SUBTYPE_VND_,		  /* vnd.oma.bcast.stkm 	[Rauschenbach][OMNA - Open Mobile Naming Authority] */
	MIME_SUBTYPE_VND_,		  /* vnd.oma.cab-address-book+xml 	[Wang][OMA] */
	MIME_SUBTYPE_VND_,		  /* vnd.oma.cab-feature-handler+xml 	[Wang][OMA] */
	MIME_SUBTYPE_VND_,		  /* vnd.oma.cab-pcc+xml 	[Wang][OMA] */
	MIME_SUBTYPE_VND_,		  /* vnd.oma.cab-user-prefs+xml 	[Wang][OMA] */
	MIME_SUBTYPE_VND_,		  /* vnd.oma.dcd 	[Primo] [OMNA - Open Mobile Naming Authority] */
	MIME_SUBTYPE_VND_,		  /* vnd.oma.dcdc 	[Primo] [OMNA - Open Mobile Naming Authority] */
	MIME_SUBTYPE_VND_,		  /* vnd.oma.dd2+xml 	[Sato][Open Mobile Alliance's BAC DLDRM Working Group] */
	MIME_SUBTYPE_VND_,		  /* vnd.oma.drm.risd+xml 	[Rauschenbach][OMNA - Open Mobile Naming Authority] */
	MIME_SUBTYPE_VND_,		  /* vnd.oma.group-usage-list+xml 	[Kelley][OMA Presence and Availability (PAG) Working Group] */
	MIME_SUBTYPE_VND_,		  /* vnd.oma.poc.detailed-progress-report+xml 	[OMA Push to Talk over Cellular (POC) Working Group] */
	MIME_SUBTYPE_VND_,		  /* vnd.oma.poc.final-report+xml 	[OMA Push to Talk over Cellular (POC) Working Group] */
	MIME_SUBTYPE_VND_,		  /* vnd.oma.poc.groups+xml 	[Kelley][OMA Push to Talk over Cellular (POC) Working Group] */
	MIME_SUBTYPE_VND_,		  /* vnd.oma.poc.invocation-descriptor+xml 	[OMA Push to Talk over Cellular (POC) Working Group] */
	MIME_SUBTYPE_VND_,		  /* vnd.oma.poc.optimized-progress-report+xml 	[OMA Push to Talk over Cellular (POC) Working Group] */
	MIME_SUBTYPE_VND_,		  /* vnd.oma.push 	[Sullivan][OMA] */
	MIME_SUBTYPE_VND_,		  /* vnd.oma.scidm.messages+xml 	[Zeng][OMNA - Open Mobile Naming Authority] */
	MIME_SUBTYPE_VND_,		  /* vnd.oma.xcap-directory+xml 	[Kelley][OMA Presence and Availability (PAG) Working Group] */
	MIME_SUBTYPE_VND_,		  /* vnd.omads-email+xml 	[OMA Data Synchronization Working Group] */
	MIME_SUBTYPE_VND_,		  /* vnd.omads-file+xml 	[OMA Data Synchronization Working Group] */
	MIME_SUBTYPE_VND_,		  /* vnd.omads-folder+xml 	[OMA Data Synchronization Working Group] */
	MIME_SUBTYPE_VND_,		  /* vnd.omaloc-supl-init 	[Grange] */
	MIME_SUBTYPE_VND_,		  /* vnd.oma-scws-config 	[Mahalal] */
	MIME_SUBTYPE_VND_,		  /* vnd.oma-scws-http-request 	[Mahalal] */
	MIME_SUBTYPE_VND_,		  /* vnd.oma-scws-http-response 	[Mahalal] */
	MIME_SUBTYPE_VND_,		  /* vnd.openofficeorg.extension 	[Lingner] */
	MIME_SUBTYPE_VND_,		  /* vnd.openxmlformats-officedocument.custom-properties+xml 	[Murata] */
	MIME_SUBTYPE_VND_,		  /* vnd.openxmlformats-officedocument.customXmlProperties+xml 	[Murata] */
	MIME_SUBTYPE_VND_,		  /* vnd.openxmlformats-officedocument.drawing+xml 	[Murata] */
	MIME_SUBTYPE_VND_,		  /* vnd.openxmlformats-officedocument.drawingml.chart+xml 	[Murata] */
	MIME_SUBTYPE_VND_,		  /* vnd.openxmlformats-officedocument.drawingml.chartshapes+xml 	[Murata] */
	MIME_SUBTYPE_VND_,		  /* vnd.openxmlformats-officedocument.drawingml.diagramColors+xml 	[Murata] */
	MIME_SUBTYPE_VND_,		  /* vnd.openxmlformats-officedocument.drawingml.diagramData+xml 	[Murata] */
	MIME_SUBTYPE_VND_,		  /* vnd.openxmlformats-officedocument.drawingml.diagramLayout+xml 	[Murata] */
	MIME_SUBTYPE_VND_,		  /* vnd.openxmlformats-officedocument.drawingml.diagramStyle+xml 	[Murata] */
	MIME_SUBTYPE_VND_,		  /* vnd.openxmlformats-officedocument.extended-properties+xml 	[Murata] */
	MIME_SUBTYPE_VND_,		  /* vnd.openxmlformats-officedocument.presentationml.commentAuthors+xml 	[Murata] */
	MIME_SUBTYPE_VND_,		  /* vnd.openxmlformats-officedocument.presentationml.comments+xml 	[Murata] */
	MIME_SUBTYPE_VND_,		  /* vnd.openxmlformats-officedocument.presentationml.handoutMaster+xml 	[Murata] */
	MIME_SUBTYPE_VND_,		  /* vnd.openxmlformats-officedocument.presentationml.notesMaster+xml 	[Murata] */
	MIME_SUBTYPE_VND_,		  /* vnd.openxmlformats-officedocument.presentationml.notesSlide+xml 	[Murata] */
	MIME_SUBTYPE_VND_,		  /* vnd.openxmlformats-officedocument.presentationml.presentation 	[Murata] */
	MIME_SUBTYPE_VND_,		  /* vnd.openxmlformats-officedocument.presentationml.presentation.main+xml 	[Murata] */
	MIME_SUBTYPE_VND_,		  /* vnd.openxmlformats-officedocument.presentationml.presProps+xml 	[Murata] */
	MIME_SUBTYPE_VND_,		  /* vnd.openxmlformats-officedocument.presentationml.slide 	[Murata] */
	MIME_SUBTYPE_VND_,		  /* vnd.openxmlformats-officedocument.presentationml.slide+xml 	[Murata] */
	MIME_SUBTYPE_VND_,		  /* vnd.openxmlformats-officedocument.presentationml.slideLayout+xml 	[Murata] */
	MIME_SUBTYPE_VND_,		  /* vnd.openxmlformats-officedocument.presentationml.slideMaster+xml 	[Murata] */
	MIME_SUBTYPE_VND_,		  /* vnd.openxmlformats-officedocument.presentationml.slideshow 	[Murata] */
	MIME_SUBTYPE_VND_,		  /* vnd.openxmlformats-officedocument.presentationml.slideshow.main+xml 	[Murata] */
	MIME_SUBTYPE_VND_,		  /* vnd.openxmlformats-officedocument.presentationml.slideUpdateInfo+xml 	[Murata] */
	MIME_SUBTYPE_VND_,		  /* vnd.openxmlformats-officedocument.presentationml.tableStyles+xml 	[Murata] */
	MIME_SUBTYPE_VND_,		  /* vnd.openxmlformats-officedocument.presentationml.tags+xml 	[Murata] */
	MIME_SUBTYPE_VND_,		  /* vnd.openxmlformats-officedocument.presentationml.template 	[Murata] */
	MIME_SUBTYPE_VND_,		  /* vnd.openxmlformats-officedocument.presentationml.template.main+xml 	[Murata] */
	MIME_SUBTYPE_VND_,		  /* vnd.openxmlformats-officedocument.presentationml.viewProps+xml 	[Murata] */
	MIME_SUBTYPE_VND_,		  /* vnd.openxmlformats-officedocument.spreadsheetml.calcChain+xml 	[Murata] */
	MIME_SUBTYPE_VND_,		  /* vnd.openxmlformats-officedocument.spreadsheetml.chartsheet+xml 	[Murata] */
	MIME_SUBTYPE_VND_,		  /* vnd.openxmlformats-officedocument.spreadsheetml.comments+xml 	[Murata] */
	MIME_SUBTYPE_VND_,		  /* vnd.openxmlformats-officedocument.spreadsheetml.connections+xml 	[Murata] */
	MIME_SUBTYPE_VND_,		  /* vnd.openxmlformats-officedocument.spreadsheetml.dialogsheet+xml 	[Murata] */
	MIME_SUBTYPE_VND_,		  /* vnd.openxmlformats-officedocument.spreadsheetml.externalLink+xml 	[Murata] */
	MIME_SUBTYPE_VND_,		  /* vnd.openxmlformats-officedocument.spreadsheetml.pivotCacheDefinition+xml 	[Murata] */
	MIME_SUBTYPE_VND_,		  /* vnd.openxmlformats-officedocument.spreadsheetml.pivotCacheRecords+xml 	[Murata] */
	MIME_SUBTYPE_VND_,		  /* vnd.openxmlformats-officedocument.spreadsheetml.pivotTable+xml 	[Murata] */
	MIME_SUBTYPE_VND_,		  /* vnd.openxmlformats-officedocument.spreadsheetml.queryTable+xml 	[Murata] */
	MIME_SUBTYPE_VND_,		  /* vnd.openxmlformats-officedocument.spreadsheetml.revisionHeaders+xml 	[Murata] */
	MIME_SUBTYPE_VND_,		  /* vnd.openxmlformats-officedocument.spreadsheetml.revisionLog+xml 	[Murata] */
	MIME_SUBTYPE_VND_,		  /* vnd.openxmlformats-officedocument.spreadsheetml.sharedStrings+xml 	[Murata] */
	MIME_SUBTYPE_VND_,		  /* vnd.openxmlformats-officedocument.spreadsheetml.sheet 	[Murata] */
	MIME_SUBTYPE_VND_,		  /* vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml 	[Murata] */
	MIME_SUBTYPE_VND_,		  /* vnd.openxmlformats-officedocument.spreadsheetml.sheetMetadata+xml 	[Murata] */
	MIME_SUBTYPE_VND_,		  /* vnd.openxmlformats-officedocument.spreadsheetml.styles+xml 	[Murata] */
	MIME_SUBTYPE_VND_,		  /* vnd.openxmlformats-officedocument.spreadsheetml.table+xml 	[Murata] */
	MIME_SUBTYPE_VND_,		  /* vnd.openxmlformats-officedocument.spreadsheetml.tableSingleCells+xml 	[Murata] */
	MIME_SUBTYPE_VND_,		  /* vnd.openxmlformats-officedocument.spreadsheetml.template 	[Murata] */
	MIME_SUBTYPE_VND_,		  /* vnd.openxmlformats-officedocument.spreadsheetml.template.main+xml 	[Murata] */
	MIME_SUBTYPE_VND_,		  /* vnd.openxmlformats-officedocument.spreadsheetml.userNames+xml 	[Murata] */
	MIME_SUBTYPE_VND_,		  /* vnd.openxmlformats-officedocument.spreadsheetml.volatileDependencies+xml 	[Murata] */
	MIME_SUBTYPE_VND_,		  /* vnd.openxmlformats-officedocument.spreadsheetml.worksheet+xml 	[Murata] */
	MIME_SUBTYPE_VND_,		  /* vnd.openxmlformats-officedocument.theme+xml 	[Murata] */
	MIME_SUBTYPE_VND_,		  /* vnd.openxmlformats-officedocument.themeOverride+xml 	[Murata] */
	MIME_SUBTYPE_VND_,		  /* vnd.openxmlformats-officedocument.vmlDrawing 	[Murata] */
	MIME_SUBTYPE_VND_,		  /* vnd.openxmlformats-officedocument.wordprocessingml.comments+xml 	[Murata] */
	MIME_SUBTYPE_VND_,		  /* vnd.openxmlformats-officedocument.wordprocessingml.document 	[Murata] */
	MIME_SUBTYPE_VND_,		  /* vnd.openxmlformats-officedocument.wordprocessingml.document.glossary+xml 	[Murata] */
	MIME_SUBTYPE_VND_,		  /* vnd.openxmlformats-officedocument.wordprocessingml.document.main+xml 	[Murata] */
	MIME_SUBTYPE_VND_,		  /* vnd.openxmlformats-officedocument.wordprocessingml.endnotes+xml 	[Murata] */
	MIME_SUBTYPE_VND_,		  /* vnd.openxmlformats-officedocument.wordprocessingml.fontTable+xml 	[Murata] */
	MIME_SUBTYPE_VND_,		  /* vnd.openxmlformats-officedocument.wordprocessingml.footer+xml 	[Murata] */
	MIME_SUBTYPE_VND_,		  /* vnd.openxmlformats-officedocument.wordprocessingml.footnotes+xml 	[Murata] */
	MIME_SUBTYPE_VND_,		  /* vnd.openxmlformats-officedocument.wordprocessingml.numbering+xml 	[Murata] */
	MIME_SUBTYPE_VND_,		  /* vnd.openxmlformats-officedocument.wordprocessingml.settings+xml 	[Murata] */
	MIME_SUBTYPE_VND_,		  /* vnd.openxmlformats-officedocument.wordprocessingml.styles+xml 	[Murata] */
	MIME_SUBTYPE_VND_,		  /* vnd.openxmlformats-officedocument.wordprocessingml.template 	[Murata] */
	MIME_SUBTYPE_VND_,		  /* vnd.openxmlformats-officedocument.wordprocessingml.template.main+xml 	[Murata] */
	MIME_SUBTYPE_VND_,		  /* vnd.openxmlformats-officedocument.wordprocessingml.webSettings+xml 	[Murata] */
	MIME_SUBTYPE_VND_,		  /* vnd.openxmlformats-package.core-properties+xml 	[Murata] */
	MIME_SUBTYPE_VND_,		  /* vnd.openxmlformats-package.digital-signature-xmlsignature+xml 	[Murata] */
	MIME_SUBTYPE_VND_,		  /* vnd.openxmlformats-package.relationships+xml 	[Murata] */
	MIME_SUBTYPE_VND_,		  /* vnd.osa.netdeploy 	[Klos] */
	MIME_SUBTYPE_VND_,		  /* vnd.osgeo.mapguide.package 	[Birch] */
	MIME_SUBTYPE_VND_,		  /* vnd.osgi.bundle 	[Kriens] */
	MIME_SUBTYPE_VND_,		  /* vnd.osgi.dp 	[Kriens] */
	MIME_SUBTYPE_VND_,		  /* vnd.otps.ct-kip+xml */
	MIME_SUBTYPE_VND_PALM,		/* vnd.palm [Peacock] */
	MIME_SUBTYPE_VND_,		  /* vnd.paos.xml 	[Kemp] */
	MIME_SUBTYPE_VND_,		  /* vnd.pawaafile 	[Baskaran] */
	MIME_SUBTYPE_VND_,		  /* vnd.pg.format 	[Gandert] */
	MIME_SUBTYPE_VND_,		  /* vnd.pg.osasli 	[Gandert] */
	MIME_SUBTYPE_VND_,		  /* vnd.piaccess.application-licence 	[Maneos] */
	MIME_SUBTYPE_VND_,		  /* vnd.picsel 	[Naccarato] */
	MIME_SUBTYPE_VND_,		  /* vnd.pmi.widget 	[Lewis] */
	MIME_SUBTYPE_VND_,		  /* vnd.poc.group-advertisement+xml 	[Kelley][OMA Push to Talk over Cellular (POC) Working Group] */
	MIME_SUBTYPE_VND_,		  /* vnd.pocketlearn 	[Pando] */
	MIME_SUBTYPE_VND_,		  /* vnd.powerbuilder6 	[Guy] */
	MIME_SUBTYPE_VND_,		  /* vnd.powerbuilder6-s 	[Guy] */
	MIME_SUBTYPE_VND_,		  /* vnd.powerbuilder7 	[Shilts] */
	MIME_SUBTYPE_VND_,		  /* vnd.powerbuilder75 	[Shilts] */
	MIME_SUBTYPE_VND_,		  /* vnd.powerbuilder75-s 	[Shilts] */
	MIME_SUBTYPE_VND_,		  /* vnd.powerbuilder7-s 	[Shilts] */
	MIME_SUBTYPE_VND_,		  /* vnd.preminet 	[Tenhunen] */
	MIME_SUBTYPE_VND_,		  /* vnd.previewsystems.box 	[Smolgovsky] */
	MIME_SUBTYPE_VND_,		  /* vnd.proteus.magazine 	[Hoch] */
	MIME_SUBTYPE_VND_,		  /* vnd.publishare-delta-tree 	[Ben-Kiki] */
	MIME_SUBTYPE_VND_,		  /* vnd.pvi.ptid1 	[Lamb] */
	MIME_SUBTYPE_VND_,		  /* vnd.pwg-multiplexed 	[RFC3391] */
	MIME_SUBTYPE_VND_,		  /* vnd.pwg-xhtml-print+xml 	[Wright] */
	MIME_SUBTYPE_VND_,		  /* vnd.qualcomm.brew-app-res 	[Forrester] */
	MIME_SUBTYPE_VND_,		  /* vnd.Quark.QuarkXPress 	[Scheidler] */
	MIME_SUBTYPE_VND_,		  /* vnd.quobject-quoxdocument 	[Ludwig] */
	MIME_SUBTYPE_VND_,		  /* vnd.radisys.moml+xml 	[RFC5707] */
	MIME_SUBTYPE_VND_,		  /* vnd.radisys.msml-audit-conf+xml 	[RFC5707] */
	MIME_SUBTYPE_VND_,		  /* vnd.radisys.msml-audit-conn+xml 	[RFC5707] */
	MIME_SUBTYPE_VND_,		  /* vnd.radisys.msml-audit-dialog+xml 	[RFC5707] */
	MIME_SUBTYPE_VND_,		  /* vnd.radisys.msml-audit-stream+xml 	[RFC5707] */
	MIME_SUBTYPE_VND_,		  /* vnd.radisys.msml-audit+xml 	[RFC5707] */
	MIME_SUBTYPE_VND_,		  /* vnd.radisys.msml-conf+xml 	[RFC5707] */
	MIME_SUBTYPE_VND_,		  /* vnd.radisys.msml-dialog-base+xml 	[RFC5707] */
	MIME_SUBTYPE_VND_,		  /* vnd.radisys.msml-dialog-fax-detect+xml 	[RFC5707] */
	MIME_SUBTYPE_VND_,		  /* vnd.radisys.msml-dialog-fax-sendrecv+xml 	[RFC5707] */
	MIME_SUBTYPE_VND_,		  /* vnd.radisys.msml-dialog-group+xml 	[RFC5707] */
	MIME_SUBTYPE_VND_,		  /* vnd.radisys.msml-dialog-speech+xml 	[RFC5707] */
	MIME_SUBTYPE_VND_,		  /* vnd.radisys.msml-dialog-transform+xml 	[RFC5707] */
	MIME_SUBTYPE_VND_,		  /* vnd.radisys.msml-dialog+xml 	[RFC5707] */
	MIME_SUBTYPE_VND_,		  /* vnd.radisys.msml+xml 	[RFC5707] */
	MIME_SUBTYPE_VND_,		  /* vnd.rainstor.data 	[Crook] */
	MIME_SUBTYPE_VND_,		  /* vnd.rapid 	[Szekely] */
	MIME_SUBTYPE_VND_,		  /* vnd.realvnc.bed 	[Reeves] */
	MIME_SUBTYPE_VND_,		  /* vnd.recordare.musicxml 	[Good] */
	MIME_SUBTYPE_VND_,		  /* vnd.recordare.musicxml+xml 	[Good] */
	MIME_SUBTYPE_VND_,		  /* vnd.RenLearn.rlprint 	[Wick] */
	MIME_SUBTYPE_VND_,		  /* vnd.rig.cryptonote 	[Jibiki] */
	MIME_SUBTYPE_VND_,		  /* vnd.route66.link66+xml 	[Kikstra] */
	MIME_SUBTYPE_VND_,		  /* vnd.ruckus.download 	[Harris] */
	MIME_SUBTYPE_VND_,		  /* vnd.s3sms 	[Tarkkala] */
	MIME_SUBTYPE_VND_,		  /* vnd.sailingtracker.track 	[Vesalainen] */
	MIME_SUBTYPE_VND_,		  /* vnd.sbm.cid [Kusakari] */
	MIME_SUBTYPE_VND_,		  /* vnd.sbm.mid2 [Murai] */
	MIME_SUBTYPE_VND_,		  /* vnd.scribus [Bradney] */
	MIME_SUBTYPE_VND_,		  /* vnd.sealed.3df [Kwan] */
	MIME_SUBTYPE_VND_,		  /* vnd.sealed.csf [Kwan] */
	MIME_SUBTYPE_VND_,		  /* vnd.sealed.doc [Petersen] */
	MIME_SUBTYPE_VND_,		  /* vnd.sealed.eml [Petersen] */
	MIME_SUBTYPE_VND_,		  /* vnd.sealed.mht [Petersen] */
	MIME_SUBTYPE_VND_,		  /* vnd.sealed.net [Lambert] */
	MIME_SUBTYPE_VND_,		  /* vnd.sealed.ppt [Petersen] */
	MIME_SUBTYPE_VND_,		  /* vnd.sealed.tiff [Kwan][Lambert] */
	MIME_SUBTYPE_VND_,		  /* vnd.sealed.xls [Petersen] */
	MIME_SUBTYPE_VND_,		  /* vnd.sealedmedia.softseal.html 	[Petersen] */
	MIME_SUBTYPE_VND_,		  /* vnd.sealedmedia.softseal.pdf 	[Petersen] */
	MIME_SUBTYPE_VND_,		  /* vnd.seemail 	[Webb] */
	MIME_SUBTYPE_VND_,		  /* vnd.sema 	[Hansson] */
	MIME_SUBTYPE_VND_,		  /* vnd.semd 	[Hansson] */
	MIME_SUBTYPE_VND_,		  /* vnd.semf 	[Hansson] */
	MIME_SUBTYPE_VND_,		  /* vnd.shana.informed.formdata 	[Selzler] */
	MIME_SUBTYPE_VND_,		  /* vnd.shana.informed.formtemplate 	[Selzler] */
	MIME_SUBTYPE_VND_,		  /* vnd.shana.informed.interchange 	[Selzler] */
	MIME_SUBTYPE_VND_,		  /* vnd.shana.informed.package 	[Selzler] */
	MIME_SUBTYPE_VND_,		  /* vnd.SimTech-MindMapper 	[Koh] */
	MIME_SUBTYPE_VND_,		  /* vnd.smaf 	[Takahashi] */
	MIME_SUBTYPE_VND_,		  /* vnd.smart.notebook 	[Neitz] */
	MIME_SUBTYPE_VND_,		  /* vnd.smart.teacher 	[Boyle] */
	MIME_SUBTYPE_VND_,		  /* vnd.software602.filler.form+xml 	[Hytka][Vondrous] */
	MIME_SUBTYPE_VND_,		  /* vnd.software602.filler.form-xml-zip 	[Hytka][Vondrous] */
	MIME_SUBTYPE_VND_,		  /* vnd.solent.sdkm+xml 	[Gauntlett] */
	MIME_SUBTYPE_VND_,		  /* vnd.spotfire.dxp 	[Jernberg] */
	MIME_SUBTYPE_VND_,		  /* vnd.spotfire.sfs 	[Jernberg] */
	MIME_SUBTYPE_VND_,		  /* vnd.sss-cod 	[Dani] */
	MIME_SUBTYPE_VND_,		  /* vnd.sss-dtf 	[Bruno] */
	MIME_SUBTYPE_VND_,		  /* vnd.sss-ntf 	[Bruno] */
	MIME_SUBTYPE_VND_,		  /* vnd.stepmania.stepchart 	[Andersson] */
	MIME_SUBTYPE_VND_,		  /* vnd.street-stream 	[Levitt] */
	MIME_SUBTYPE_VND_,		  /* vnd.sun.wadl+xml 	[Hadley] */
	MIME_SUBTYPE_VND_,		  /* vnd.sus-calendar 	[Niedfeldt] */
	MIME_SUBTYPE_VND_,		  /* vnd.svd 	[Becker] */
	MIME_SUBTYPE_VND_,		  /* vnd.swiftview-ics 	[Widener] */
	MIME_SUBTYPE_VND_,		  /* vnd.syncml.dm.notification 	[Thompson][OMA-DM Work Group] */
	MIME_SUBTYPE_VND_,		  /* vnd.syncml.dm+wbxml 	[OMA-DM Work Group] */
	MIME_SUBTYPE_VND_,		  /* vnd.syncml.dm+xml 	[Rao][OMA-DM Work Group] */
	MIME_SUBTYPE_VND_,		  /* vnd.syncml.ds.notification 	[OMA Data Synchronization Working Group] */
	MIME_SUBTYPE_VND_,		  /* vnd.syncml+xml 	[OMA Data Synchronization Working Group] */
	MIME_SUBTYPE_VND_,		  /* vnd.tao.intent-module-archive 	[Shelton] */
	MIME_SUBTYPE_VND_,		  /* vnd.tcpdump.pcap 	[Harris][Turner] */
	MIME_SUBTYPE_VND_,		  /* vnd.tmobile-livetv 	[Helin] */
	MIME_SUBTYPE_VND_,		  /* vnd.trid.tpt 	[Cusack] */
	MIME_SUBTYPE_VND_,		  /* vnd.triscape.mxs 	[Simonoff] */
	MIME_SUBTYPE_VND_,		  /* vnd.trueapp 	[Hepler] */
	MIME_SUBTYPE_VND_,		  /* vnd.truedoc 	[Chase] */
	MIME_SUBTYPE_VND_,		  /* vnd.ubisoft.webplayer 	[Talbot] */
	MIME_SUBTYPE_VND_,		  /* vnd.ufdl 	[Manning] */
	MIME_SUBTYPE_VND_,		  /* vnd.uiq.theme 	[Ocock] */
	MIME_SUBTYPE_VND_,		  /* vnd.umajin 	[Riden] */
	MIME_SUBTYPE_VND_,		  /* vnd.unity 	[Unity3d] */
	MIME_SUBTYPE_VND_,		  /* vnd.uoml+xml 	[Gerdes] */
	MIME_SUBTYPE_VND_,		  /* vnd.uplanet.alert 	[Martin] */
	MIME_SUBTYPE_VND_,		  /* vnd.uplanet.alert-wbxml 	[Martin] */
	MIME_SUBTYPE_VND_,		  /* vnd.uplanet.bearer-choice 	[Martin] */
	MIME_SUBTYPE_VND_,		  /* vnd.uplanet.bearer-choice-wbxml 	[Martin] */
	MIME_SUBTYPE_VND_,		  /* vnd.uplanet.cacheop 	[Martin] */
	MIME_SUBTYPE_VND_,		  /* vnd.uplanet.cacheop-wbxml 	[Martin] */
	MIME_SUBTYPE_VND_,		  /* vnd.uplanet.channel 	[Martin] */
	MIME_SUBTYPE_VND_,		  /* vnd.uplanet.channel-wbxml 	[Martin] */
	MIME_SUBTYPE_VND_,		  /* vnd.uplanet.list 	[Martin] */
	MIME_SUBTYPE_VND_,		  /* vnd.uplanet.listcmd 	[Martin] */
	MIME_SUBTYPE_VND_,		  /* vnd.uplanet.listcmd-wbxml 	[Martin] */
	MIME_SUBTYPE_VND_,		  /* vnd.uplanet.list-wbxml 	[Martin] */
	MIME_SUBTYPE_VND_,		  /* vnd.uplanet.signal 	[Martin] */
	MIME_SUBTYPE_VND_,		  /* vnd.vcx 	[T.Sugimoto] */
	MIME_SUBTYPE_VND_,		  /* vnd.vd-study [Rogge] */
	MIME_SUBTYPE_VND_,		  /* vnd.vectorworks [Ferguson][Sarkar] */
	MIME_SUBTYPE_VND_,		  /* vnd.verimatrix.vcas [Peterka] */
	MIME_SUBTYPE_VND_,		  /* vnd.vidsoft.vidconference [Hess] */
	MIME_SUBTYPE_VND_,		  /* vnd.visio [Sandal] */
	MIME_SUBTYPE_VND_,		  /* vnd.visionary [Aravindakumar] */
	MIME_SUBTYPE_VND_,		  /* vnd.vividence.scriptfile [Risher] */
	MIME_SUBTYPE_VND_,		  /* vnd.vsf [Rowe] */
	MIME_SUBTYPE_VND_,		  /* vnd.wap.sic [WAP-Forum] */
	MIME_SUBTYPE_VND_,		  /* vnd.wap.slc [WAP-Forum] */
	MIME_SUBTYPE_VND_,		  /* vnd.wap.wbxml [Stark] */
	MIME_SUBTYPE_VND_,		  /* vnd.wap.wmlc [Stark] */
	MIME_SUBTYPE_VND_,		  /* vnd.wap.wmlscriptc [Stark] */
	MIME_SUBTYPE_VND_,		  /* vnd.webturbo [Rehem] */
	MIME_SUBTYPE_VND_,		  /* vnd.wfa.wsc [Wi-Fi Alliance] */
	MIME_SUBTYPE_VND_,		  /* vnd.wmc */
	MIME_SUBTYPE_VND_,		  /* vnd.wmf.bootstrap [Nguyenphu] [Iyer] */
	MIME_SUBTYPE_VND_,		  /* vnd.wolfram.mathematica [Wolfram] */
	MIME_SUBTYPE_VND_,		  /* vnd.wolfram.mathematica.package [Wolfram] */
	MIME_SUBTYPE_VND_,		  /* vnd.wolfram.player [Wolfram] */
	MIME_SUBTYPE_VND_,		  /* vnd.wordperfect [Scarborough] */
	MIME_SUBTYPE_VND_,		  /* vnd.wqd [Bostrom] */
	MIME_SUBTYPE_VND_,		  /* vnd.wrq-hp3000-labelled [Bartram] */
	MIME_SUBTYPE_VND_,		  /* vnd.wt.stf [Wohler] */
	MIME_SUBTYPE_VND_,		  /* vnd.wv.csp+xml [Ingimundarson] */
	MIME_SUBTYPE_VND_,		  /* vnd.wv.csp+wbxml [Salmi] */
	MIME_SUBTYPE_VND_,		  /* vnd.wv.ssp+xml [Ingimundarson] */
	MIME_SUBTYPE_VND_,		  /* vnd.xara [Matthewman] */
	MIME_SUBTYPE_VND_,		  /* vnd.xfdl [Manning] */
	MIME_SUBTYPE_VND_,		  /* vnd.xfdl.webform [Mansell] */
	MIME_SUBTYPE_VND_,		  /* vnd.xmi+xml [Waskiewicz] */
	MIME_SUBTYPE_VND_,		  /* vnd.xmpie.cpkg [Sherwin] */
	MIME_SUBTYPE_VND_,		  /* vnd.xmpie.dpkg [Sherwin] */
	MIME_SUBTYPE_VND_,		  /* vnd.xmpie.plan [Sherwin] */
	MIME_SUBTYPE_VND_,		  /* vnd.xmpie.ppkg [Sherwin] */
	MIME_SUBTYPE_VND_,		  /* vnd.xmpie.xlim [Sherwin] */
	MIME_SUBTYPE_VND_,		  /* vnd.yamaha.hv-dic [Yamamoto] */
	MIME_SUBTYPE_VND_,		  /* vnd.yamaha.hv-script [Yamamoto] */
	MIME_SUBTYPE_VND_,		  /* vnd.yamaha.hv-voice [Yamamoto] */
	MIME_SUBTYPE_VND_,		  /* vnd.yamaha.openscoreformat.osfpvg+xml [Olleson] */
	MIME_SUBTYPE_VND_,		  /* vnd.yamaha.openscoreformat [Olleson] */
	MIME_SUBTYPE_VND_,		  /* vnd.yamaha.remote-setup [Sukizaki] */
	MIME_SUBTYPE_VND_,		  /* vnd.yamaha.smaf-audio [Shinoda] */
	MIME_SUBTYPE_VND_,		  /* vnd.yamaha.smaf-phrase [Shinoda] */
	MIME_SUBTYPE_VND_,		  /* vnd.yamaha.tunnel-udpencap [Sukizaki] */
	MIME_SUBTYPE_VND_,		  /* vnd.yellowriver-custom-menu [Yellow] */
	MIME_SUBTYPE_VND_,		  /* vnd.zul [Grothmann] */
	MIME_SUBTYPE_VND_,		  /* vnd.zzazz.deck+xml [Hewett] */
#endif
	MIME_SUBTYPE_VOICEXML_XML,	/* voicexml+xml [RFC4267] */
	MIME_SUBTYPE_VQ_RTCPXR,		/* vq-rtcpxr [RFC6035] */
	MIME_SUBTYPE_WATCHERINFO_XML,	/* watcherinfo+xml [RFC3858] */
	MIME_SUBTYPE_WHOISPP_QUERY,	/* whoispp-query [RFC2957] */
	MIME_SUBTYPE_WHOISPP_RESPONSE,	/* whoispp-response [RFC2958] */
	MIME_SUBTYPE_WIDGET,		/* widget [W3C][Pemberton] */
	MIME_SUBTYPE_WITA,		/* wita [Campbell] */
	MIME_SUBTYPE_WORDPERFECT5_1,	/* wordperfect5.1 */
	MIME_SUBTYPE_WSDL_XML,				/* wsdl+xml [W3C] */
	MIME_SUBTYPE_WSPOLICY_XML,			/* wspolicy+xml [W3C] */
	MIME_SUBTYPE_X400_BP,				/* x400-bp [RFC1494] */
	MIME_SUBTYPE_XCAP_ATT_XML,			/* xcap-att+xml [RFC4825] */
	MIME_SUBTYPE_XCAP_CAPS_XML,			/* xcap-caps+xml [RFC4825] */
	MIME_SUBTYPE_XCAP_DIFF_XML,			/* xcap-diff+xml [RFC5874] */
	MIME_SUBTYPE_XCAP_EL_XML,			/* xcap-el+xml [RFC4825] */
	MIME_SUBTYPE_XCAP_ERROR_XML,			/* xcap-error+xml [RFC4825] */
	MIME_SUBTYPE_XCAP_NS_XML,			/* xcap-ns+xml [RFC4825] */
	MIME_SUBTYPE_XCON_CONFERENCE_INFO_DIFF_XML,	/* xcon-conference-info-diff+xml [RFC-ietf-xcon-event-package-01.txt] */
	MIME_SUBTYPE_XCON_CONFERENCE_INFO_XML,		/* xcon-conference-info+xml [RFC-ietf-xcon-event-package-01.txt] */
	MIME_SUBTYPE_XENC_XML,				/* xenc+xml [Reagle][XENC Working Group] */
	MIME_SUBTYPE_XHTML_VOICE_XML,			/* xhtml-voice+xml (Obsolete) [RFC-mccobb-xplusv-media-type-04.txt] */
	MIME_SUBTYPE_XHTML_XML,				/* xhtml+xml [RFC3236] */
	MIME_SUBTYPE_XML_DTD,				/* xml-dtd [RFC3023] */
	MIME_SUBTYPE_XMPP_XML,				/* xmpp+xml [RFC3923] */
	MIME_SUBTYPE_XOP_XML,		/* xop+xml [Nottingham] */
	MIME_SUBTYPE_XSLT_XML,		/* xslt+xml [W3C] */
	MIME_SUBTYPE_XV_XML,		/* xv+xml [RFC4374] */
	MIME_SUBTYPE_YANG,		/* yang [RFC6020] */
	MIME_SUBTYPE_YIN_XML,		/* yin+xml [RFC6020] */
	MIME_SUBTYPE_ZIP,		/* zip */

	/* video */
	MIME_SUBTYPE_3GPP_TT,		/* 3gpp-tt [RFC4396] */
	MIME_SUBTYPE_BMPEG,		/* BMPEG [RFC3555] */
	MIME_SUBTYPE_BT656,		/* BT656 [RFC3555] */
	MIME_SUBTYPE_CELB,		/* CelB [RFC3555] */
	MIME_SUBTYPE_DV,		/* DV [RFC3189] */
	MIME_SUBTYPE_H261,		/* H261 [RFC4587] */
	MIME_SUBTYPE_H263,		/* H263 [RFC3555] */
	MIME_SUBTYPE_H263_1998,		/* H263-1998 [RFC4629] */
	MIME_SUBTYPE_H263_2000,		/* H263-2000 [RFC4629] */
	MIME_SUBTYPE_H264,		/* H264 [RFC-ietf-avt-rtp-rfc3984bis-12] */
	MIME_SUBTYPE_H264_RCDO,		/* H264-RCDO [RFC-ietf-avt-rtp-h264-rcdo-08.txt] */
	MIME_SUBTYPE_H264_SVC,		/* H264-SVC [RFC-ietf-avt-rtp-svc-27.txt] */
	MIME_SUBTYPE_JPEG2000,		/* jpeg2000 [RFC5371][RFC5372] */
	MIME_SUBTYPE_MJ2,		/* MJ2 [RFC3745] */
	MIME_SUBTYPE_MP1S,		/* MP1S [RFC3555] */
	MIME_SUBTYPE_MP2P,		/* MP2P [RFC3555] */
	MIME_SUBTYPE_MP2T,		/* MP2T [RFC3555] */
	MIME_SUBTYPE_MP4V_ES,		/* MP4V-ES [RFC3016] */
	MIME_SUBTYPE_MPV,		/* MPV [RFC3555] */
	MIME_SUBTYPE_NV,		/* nv [RFC4856] */
	MIME_SUBTYPE_POINTER,		/* pointer [RFC2862] */
	MIME_SUBTYPE_QUICKTIME,		/* quicktime [Lindner] */
	MIME_SUBTYPE_RAW,		/* raw [RFC4175] */
	MIME_SUBTYPE_SMPTE292M,		/* SMPTE292M [RFC3497] */
	MIME_SUBTYPE_VC1,		/* vc1 [RFC4425] */
	MIME_SUBTYPE_VND_CCTV,		/* vnd.CCTV [Rottmann] */
	MIME_SUBTYPE_VND_DECE_HD,	/* vnd.dece.hd [Dolan] */
	MIME_SUBTYPE_VND_DECE_MOBILE,	/* vnd.dece.mobile [Dolan] */
	MIME_SUBTYPE_VND_DECE_MP4,	/* vnd.dece.mp4 [Dolan] */
	MIME_SUBTYPE_VND_DECE_PD,	/* vnd.dece.pd [Dolan] */
	MIME_SUBTYPE_VND_DECE_SD,			/* vnd.dece.sd [Dolan] */
	MIME_SUBTYPE_VND_DECE_VIDEO,			/* vnd.dece.video [Dolan] */
	MIME_SUBTYPE_VND_DIRECTV_MPEG,			/* vnd.directv.mpeg [Zerbe] */
	MIME_SUBTYPE_VND_DIRECTV_MPEG_TTS,		/* vnd.directv.mpeg-tts [Zerbe] */
	MIME_SUBTYPE_VND_DLNA_MPEG_TTS,			/* vnd.dlna.mpeg-tts [Heredia] */
	MIME_SUBTYPE_VND_FVT,				/* vnd.fvt [Fuldseth] */
	MIME_SUBTYPE_VND_HNS_VIDEO,			/* vnd.hns.video [Swaminathan] */
	MIME_SUBTYPE_VND_IPTVFORUM_1DPARITYFEC_1010,	/* vnd.iptvforum.1dparityfec-1010 [Nakamura] */
	MIME_SUBTYPE_VND_IPTVFORUM_1DPARITYFEC_2005,	/* vnd.iptvforum.1dparityfec-2005 [Nakamura] */
	MIME_SUBTYPE_VND_IPTVFORUM_2DPARITYFEC_1010,	/* vnd.iptvforum.2dparityfec-1010 [Nakamura] */
	MIME_SUBTYPE_VND_IPTVFORUM_2DPARITYFEC_2005,	/* vnd.iptvforum.2dparityfec-2005 [Nakamura] */
	MIME_SUBTYPE_VND_IPTVFORUM_TTSAVC,		/* vnd.iptvforum.ttsavc [Nakamura] */
	MIME_SUBTYPE_VND_IPTVFORUM_TTSMPEG2,		/* vnd.iptvforum.ttsmpeg2 [Nakamura] */
	MIME_SUBTYPE_VND_MOTOROLA_VIDEO,		/* vnd.motorola.video [McGinty] */
	MIME_SUBTYPE_VND_MOTOROLA_VIDEOP,		/* vnd.motorola.videop [McGinty] */
	MIME_SUBTYPE_VND_MPEGURL,			/* vnd.mpegurl [Recktenwald] */
	MIME_SUBTYPE_VND_MS_PLAYREADY_MEDIA_PYV,	/* vnd.ms-playready.media.pyv [DiAcetis] */
	MIME_SUBTYPE_VND_NOKIA_INTERLEAVED_MULTIMEDIA,	/* vnd.nokia.interleaved-multimedia [Kangaslampi] */
	MIME_SUBTYPE_VND_NOKIA_VIDEOVOIP,		/* vnd.nokia.videovoip [Nokia] */
	MIME_SUBTYPE_VND_OBJECTVIDEO,			/* vnd.objectvideo [Clark] */
	MIME_SUBTYPE_VND_SEALED_MPEG1,			/* vnd.sealed.mpeg1 [Petersen] */
	MIME_SUBTYPE_VND_SEALED_MPEG4,			/* vnd.sealed.mpeg4 [Petersen] */
	MIME_SUBTYPE_VND_SEALED_SWF,			/* vnd.sealed.swf [Petersen] */
	MIME_SUBTYPE_VND_SEALEDMEDIA_SOFTSEAL_MOV,	/* vnd.sealedmedia.softseal.mov [Petersen] */
	MIME_SUBTYPE_VND_UVVU_MP4,			/* vnd.uvvu.mp4 [Dolan] */
	MIME_SUBTYPE_VND_VIVO				/* vnd.vivo [Wolfe] */
};

#endif /* MIME_SUBTYPES_H */
