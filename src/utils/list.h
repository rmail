#ifndef LIST_H
#define LIST_H

#include "mail.h"

#define LIST_LIMIT	512

struct mail_entry {
	struct mail_header hdr;
};

struct mail_list {
	size_t n;			/* Number of elements */
	size_t nfree;			/* Index ot the 1st free element */
	struct mail_entry list[1];	/* Ptr to mail entries */
};



struct mail_list *list_create(size_t n_elem, size_t limit);

void list_free(struct mail_list *lst);

int list_add_entry(struct mail_list *lst, struct mail_entry *entry);

void list_print(struct mail_list *lst);

#endif	/* LIST_H */
