#ifndef MIME_H
#define MIME_H

/* http://www.iana.org/assignments/media-types/index.html */

#include "mime_subtypes.h"

/* MIME Content-Type */
enum mime_type {
	MIME_TYPE_UNKNOWN = 0,
	MIME_TYPE_APPLICATION,
	MIME_TYPE_AUDIO,
	MIME_TYPE_EXAMPLE,
	MIME_TYPE_IMAGE,
	MIME_TYPE_MESSAGE,
	MIME_TYPE_MODEL,
	MIME_TYPE_MULTIPART,
	MIME_TYPE_TEXT,
	MIME_TYPE_VIDEO
};

/* MIME Content-Type charset for text media type [RFC 2045] */
enum mime_type_charset {
	MIME_TYPE_CHARSET_UNKNOWN = 0,
	MIME_TYPE_CHARSET_US_ASCII,
	MIME_TYPE_CHARSET_ISO_8859_1,
	MIME_TYPE_CHARSET_UTF_8,
	MIME_TYPE_CHARSET_WINDOWS_1252
};


/* MIME Content-Disposition */
enum mime_disposition {
	MIME_DISPOSITION_UNKNOWN = 0,
	MIME_DISPOSITION_INLINE,
	MIME_DISPOSITION_ATTACHMENT
};

/* MIME Content-Transfer-Encoding */
enum mime_encoding {
	MIME_ENCODING_UNKNOWN = 0,
	MIME_ENCODING_7BIT,
	MIME_ENCODING_QUOTED_PRINTABLE,
	MIME_ENCODING_BASE64,
	MIME_ENCODING_8BIT,
	MIME_ENCODING_BINARY
};

struct mime_header {
	enum mime_type         type;               /* Content-Type */
	enum mime_type_charset type_charset;       /* Content-Type Charset */
	enum mime_subtype      subtype;            /* Content Subtypes */
	enum mime_disposition  disposition;        /* Content-Disposition */
	enum mime_encoding     transfer_encoding;  /* Content-Transfer-Encoding */

	/* Multipart boundary */
	char boundary[1024];
};

void mime_content_type(const char *buf, struct mime_header *hdr);
void mime_content_type_parameter(const char *buf, struct mime_header *hdr);

void mime_content_disposition(const char *buf, struct mime_header *hdr);

void mime_content_transfer_encoding(const char *buf, struct mime_header *hdr);

void debug_mime_header(const struct mime_header *hdr);

#endif /* MIME_H */
