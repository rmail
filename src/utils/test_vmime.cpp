#include <iostream>
#include <fstream>	// for ifstream

// VMIME headers
#include "vmime/vmime.hpp"
#include "vmime/platforms/posix/posixHandler.hpp"
#include "vmime/security/digest/messageDigestFactory.hpp"

int main(int argc, char **argv)
{

	if (argc < 2) {
		std::cout << "Usage: " << argv[0] << "<file>" << std::endl;
		return -1;
	}

	// Read data from file
	std::ifstream file;

	file.open(argv[1], std::ios::in | std::ios::binary);
	if (!file.good())
	{
		std::cout << "Cannot open input file.\n";
		return 1;
	}
	// VMime initialization
	vmime::platform::setHandler<vmime::platforms::posix::posixHandler>();


	vmime::utility::inputStreamAdapter is(file);
	vmime::string data;
	vmime::utility::outputStreamStringAdapter os(data);
	vmime::utility::bufferedStreamCopy(is, os);

	// Actually parse the message
	vmime::ref<vmime::message> msg = vmime::create<vmime::message>();
	msg->parse(data);

	vmime::ref<vmime::header> hdr = msg->getHeader();

	// Obtains a reference to the body contents
	vmime::ref<vmime::body> bdy = msg->getBody();

	vmime::messageParser mp(msg);

	// Now, you can extract some of its components
	vmime::charset ch(vmime::charsets::UTF_8);

	std::cout << "Date   : " << hdr->Date()->getValue()->generate(); //.dynamicCast<vmime::text>()->getEmail();
	std::cout << std::endl;
//	std::cout << "From   : " << hdr->From()->getValue().dynamicCast<vmime::mailbox>()->getName().getConvertedText(ch) << std::endl;
	std::cout << "From   : " << hdr->From()->getValue().dynamicCast<vmime::mailbox>()->getEmail();

	// Calc MD5 of address
	vmime::ref <vmime::security::digest::messageDigest> var;

	var = vmime::security::digest::messageDigestFactory::getInstance()->create("md5");
	var->update(hdr->From()->getValue().dynamicCast<vmime::mailbox>()->getEmail());
	var->finalize();
	std::cout << " (MD5: " << var->getHexDigest() << ")" << std::endl;


	// Prints Subjects
	std::cout << "Subject: " << hdr->Subject()->getValue().dynamicCast<vmime::text>()->getConvertedText(ch) << std::endl;

	// Prints Header Content-Type
	std::cout << "Content-Type: " << hdr->ContentType()->getValue()->generate() << std::endl;


        const std::vector<const vmime::attachment> atm = findAttachmentsInMessage(msg);
	std::vector<ref<const vmime::attachment>>::const_iterator i;
	for (i = atm->begin(); i < atm->end(); i++) {
		std::cout << "Attachment is: "
			  << att.getName().getBuffer()
			  << " ("  << att.getType().getType() << ")"
			  << std::endl;
	}

	// Enumerate attachments
//	std::cout << "Attachment count" << mp.getAttachmentCount() << std::endl;
	for (int i = 0 ; i < mp.getAttachmentCount() ; ++i) {
		const vmime::attachment& att = *mp.getAttachmentAt(i);
		// Media type (content type) is in "att.getType()"
		// Name is in "att.getName()"
		// Description is in "att.getDescription()"
		// Data is in "att.getData()"



		std::cout << "Attachment is: "
			  << att.getName().getBuffer()
			  << " ("  << att.getType().getType() << ")"
			  << std::endl;
	}


	std::cout << "Body encoding : " << bdy->getEncoding().getName() << std::endl;

	// Prints Body
	vmime::ref <const vmime::contentHandler> cts = bdy->getContents();

	vmime::utility::outputStreamAdapter out(std::cout);
	cts->extract(out);

	//std::cout << "cts.getLength() = " << cts->getLength(); //->getConvertedText(ch) << std::endl;


	return 0;
}
