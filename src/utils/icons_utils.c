#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>

/* Default Pixmap path */
#ifndef PIXMAP_PATH
#define PIXMAP_PATH	"/usr/share/applets/pixmaps/"
#define PIXMAP_DEFAULT	"/usr/share/applets/pixmaps/mail.png"
#endif

/*#define DEBUG*/

static char *make_pixmap_path(char *dest, char *src, size_t n)
{
	char *icon = NULL;
	char md5_str[16 * 2 + 1];
	char *p;
	size_t j;

	/* PIXMAP_PATH + MD5_STR + '.png' + '\0' */
	const size_t i = strlen(PIXMAP_PATH) + (2 * 16) + 4 + 1;

	if (dest) {
		if (n < i) {
			fprintf(stderr, "%s - Buffer too short! [%lu/%lu]\n",
				__func__, (unsigned long) n, (unsigned long) i);
			return NULL;
		}
		icon = dest;
	} else {
		icon = malloc(i);
		if (!icon) {
			fprintf(stderr, "%s - MALLOC %s\n", __func__, strerror(errno));
			return NULL;
		}
	}

	memset(md5_str, 0, 16*2 +1);
	p = md5_str;
	for (j = 0; j < 16; j++) {
		snprintf(p, 3, "%02x", (unsigned char) src[j]);
		p += 2;
	}
	snprintf(icon, i, "%s%s.png", PIXMAP_PATH, md5_str);
#ifdef DEBUG
	fprintf(stderr, "%s - PIXMAP_PATH:\"%s\"\n", __func__, PIXMAP_PATH);
#endif
	return icon;
}

char *icon_get_path(char *filename)
{
	char *icon = NULL;

	icon = make_pixmap_path(NULL, filename, 0);
	if (icon) {
		if (access(icon, F_OK | R_OK) != 0) {
#ifdef DEBUG
			fprintf(stderr, "%s - ACCESS:%s %s\n", __func__, icon, strerror(errno));
#endif
			free(icon);
			icon = PIXMAP_DEFAULT;
		}
	}

#ifdef DEBUG
	fprintf(stderr, "%s - icon:\"%s\"\n", __func__, icon);
#endif
	return icon;
}

void icon_free_path(char *icon)
{
	if ((icon) && (icon != (char *) &PIXMAP_DEFAULT)) {
		free(icon);
		icon = NULL;
	}
}
