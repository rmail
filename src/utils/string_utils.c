/*
 * string_utils.c
 */
#include <stdio.h>
#include <ctype.h>

char *substrncpy(char *dest, const char *start, const char *end, size_t n)
{
	size_t i;
	const char *src;

	if ((!dest) || (!start) || (!end) || (n == 0))
		return NULL;

	if (start > end)
		return NULL;

	src = start;
        for (i = 0 ; i < n && src <= end; i++, src++) {
		dest[i] = *src;
	}
	for ( ; i < n ; i++) {
		dest[i] = '\0';
	}

	return dest;
}

const char *trim_spaces(const char *line)
{
	const char *p = line;

	while (*p != '\0' && isspace(*p)) {
		if (*p == '\n' || *p == '\r') {
			break;
		}
		p++;
	}
	return p;
}
