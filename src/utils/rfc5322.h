#ifndef RFC5322_UTILS_H
#define RFC5322_UTILS_H

int rfc5322_extract_address(char *from, const char *buf, size_t n);
void rfc5322_extract_subject(char *subject, const char *buf, size_t n);

#endif	/* RFC5322_UTILS_H */
