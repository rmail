#define  _BSD_SOURCE

#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <dirent.h>
#include <errno.h>
#include <fcntl.h>

#include <sys/types.h>
#include <sys/stat.h>

/**
 * file_type
 *
 * @param[in] file	file name
 *
 * @retval 0 file is "regular", 1 otherwise, -1 on error
 */
int file_type(const char *file)
{
	int fd;
	struct stat fsb;

	if (!file)
		return -1;

	fd = open(file, O_RDONLY);
	if (fd < 0) {
		fprintf(stderr, "%s - fstat %s: %s\n",
				 __func__, file, strerror(errno));
		return -1;
	}

	if (fstat(fd, &fsb)) {
		fprintf(stderr, "%s - fstat %s: %s\n",
				 __func__, file, strerror(errno));
		close(fd);
		return -1;
	}

	if (!S_ISREG(fsb.st_mode)) {
		close(fd);
		return 1;
	}

	close(fd);
	return 0;
}

int scan_dir(const char *path, void (*file_callback)(const char *))
{
	int rc = 0;
	DIR *dir;
	struct dirent *de;

	if (!path)
		return -1;

	dir = opendir(path);
	if (!dir) {
		fprintf(stderr, "%s - %s: %s\n", __func__, path, strerror(errno));
		return -1;
	}

	do {
		de = readdir(dir);
		if (de) {
			file_callback(de->d_name);
			continue;
		}

		/* de is null on error or EOD */
		if (errno) {
			rc = -1;
			fprintf(stderr, "%s - %s!\n", __func__, strerror(errno));
			break;
		}
	} while (de);

	closedir(dir);

	return rc;
}

size_t dir_entries(const char *path)
{
	DIR *dir;
	struct dirent *de;
	size_t count = 0;

	if (!path)
		return 0;

	dir = opendir(path);
	if (!dir) {
		fprintf(stderr, "%s - %s: %s\n", __func__, path, strerror(errno));
		return 0;
	}

	do {
		de = readdir(dir);
		if (de) {
			if (file_type(de->d_name) == 0)
				count++;
			continue;
		}

		/* de is null on error or EOD */
		if (errno) {
			fprintf(stderr, "%s - %s!\n", __func__, strerror(errno));
			break;
		}
	} while (de);

	closedir(dir);

	return count;
}

