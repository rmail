#define  _BSD_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <dirent.h>
#include <errno.h>
#include <fcntl.h>

#include <sys/types.h>
#include <sys/stat.h>

#include "md5_utils.h"
#include "string_utils.h"
#include "file_utils.h"
#include "rfc5322.h"

#include "list.h"
#include "pop3.h"
#include "pop3_config.h"

static struct mail_list *mlist;

void list_add_from_file(const char *filename)
{
	FILE *fh;
	int done = 0;

	size_t n = 0;

	char buf[POP3_BUFSIZE];
	char from[POP3_BUFSIZE];
	char subject[POP3_BUFSIZE];	/* 998 + CR + LF */

	/* Check if is a regular file */
	if (file_type(filename)) {
		return;
	}

	fh = fopen(filename, "r");
	if (!fh)
		return;

	while (fgets(buf, sizeof(buf), fh) && done != 3) {
		if (!strncmp(buf, "From:", 5)) {
			rfc5322_extract_address(from, buf, sizeof(from));
			done |= 1;
		} else if (!strncmp(buf, "Subject:", 5)) {
			rfc5322_extract_subject(subject, buf, sizeof(subject));
			done |= 2;
		} else {
#if 0
			printf("[SKIP] ------- %s", buf);
#endif
		}
	}
	fclose(fh);

	if (done && mlist) {
		if (mlist->nfree < LIST_LIMIT) {
			n = mlist->nfree;
			/* Update List */
			fprintf(stderr, "%s from file %s to slot %lu\n", __func__, filename, (unsigned long) n);
			strncpy(mlist->list[n].hdr.filename, "name", 1024);
			strncpy(mlist->list[n].hdr.from, from, 1024);
			strncpy(mlist->list[n].hdr.subject, subject, 1024);
			md5_calc_sum(from, mlist->list[n].hdr.addr_md5);
			mlist->nfree += 1;
		} else {
			fprintf(stderr, "%s - No free slot for %s\n", __func__, filename);
		}
	}
}

int main(__attribute__((unused)) int argc,
	 __attribute__((unused)) char **argv)
{
	int rc = -1;

#if 0
	char old_path[PATH_MAX];
	const char *path;
#endif

	struct pop3_mailbox mb;
	unsigned int n;

	mb.host.name = POP3_DEFAULT_HOST;
	mb.host.port = POP3_DEFAULT_PORT;
	mb.auth.user = POP3_DEFAULT_USER;
	mb.auth.pass = POP3_DEFAULT_PASSWORD;

	fprintf(stderr, "Connecting to POP3 server %s...\n", mb.host.name);
	if (pop3_connect(&mb) < 0)
		return -1;

	if (pop3_authenticate(&mb) < 0) {
		fprintf(stderr, "POP3 Authentication failed\n");
		goto app_out;
	}

	/* Retrieve the number of messages present on the mailbox */
	if (pop3_stat(&mb) < 0)
		fprintf(stderr, "Error fetch messages\n");
	else
		fprintf(stderr, "%u Message in maildrop\n", mb.messages);

	if (mb.messages == 0) {
		return 0;
	}

	/* Init mail list */
	mlist = list_create(mb.messages, LIST_LIMIT);
	if (!mlist) {
		goto app_out;
	}

	fprintf(stderr, "Try to process %lu messages\n", (unsigned long) mlist->n);

	if (mb.messages > 0) {
		for (n = 1; n <= mb.messages; n++) {
			rc = pop3_top(&mb, n);
			if (rc < 0) {
				fprintf(stderr, "%s - TOP err n=%u\n", __func__, n);
				break;
			}
		}
	} else {
		fprintf(stderr, "%s - mbox.messages = %u ", __func__, mb.messages);

	}
#if 0
	rc = scan_dir(path, list_add_from_file);
#endif

	list_print(mlist);
	list_free(mlist);

app_out:
	pop3_disconnect(&mb);
	return rc;
}
