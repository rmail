
#include "Poco/Net/MailMessage.h"
#include "Poco/Net/MailStream.h"
#include "Poco/Net/MailRecipient.h"
#include "Poco/Timestamp.h"
#include "Poco/Net/PartHandler.h"
#include "Poco/Net/StringPartSource.h"
#include "Poco/Path.h"
#include "Poco/Exception.h"
#include <iostream>
#include <fstream>	// for ifstream


using Poco::Net::MailMessage;
using Poco::Net::PartHandler;
using Poco::Net::MailRecipient;
using Poco::Net::StringPartSource;
using Poco::Path;
using Poco::Exception;


int main(int argc, char **argv)
{
	if (argc < 2) {
		Path p(argv[0]);
		std::cerr << "usage: " << p.getBaseName() << " <file>" << std::endl;
		return 1;
	}

	// Read data from file
	std::ifstream file;
//	std::istringstream file;
	file.open(argv[1], std::ios::in /*| std::ios::binary*/);
	if (!file.good()) {
		std::cerr << "Cannot open input file.\n";
		return 1;
	}

	try {
		MailMessage message;
//		PartHandler ph = PartHandler();

//		message.read(file, ph);
//		message.readHeader(file);
		message.read(file);

//		std::cout << "Date     : " << message.getDate() << std::endl;
		std::cout << "From     : " << message.getSender() << std::endl;
		std::cout << "Subject  : " << message.getSubject() << std::endl;

		if (!message.isMultipart()) {
			std::cout << std::endl << message.getContent();
		} else {
			std::cout << "Multipart: " << message.isMultipart() << std::endl;
		}
	} catch (Exception& exc) {
		std::cerr << exc.displayText() << std::endl;
		return 1;
	}
	return 0;
}
