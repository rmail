#ifndef ICONS_UTILS_H
#define ICONS_UTILS_H

char *icon_get_path(char *filename);
void icon_free_path(char *icon);

#endif	/* ICONS_UTILS_H */
