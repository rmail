/* MIME types
 *
 * http://www.iana.org/assignments/media-types/index.html
 *
 */

#include <stdio.h>
#include <string.h>
#include <ctype.h>

#include "string_utils.h"
#include "mime.h"

/* XXX Only for text/plain check RFCs ??? */
/**
 * Parse text parameters
 *
 * @param[in]      buf	Header line
 * @param[in|out]  hdr  Mime header struct
 */
static void parse_text_params(const char *buf, struct mime_header *hdr)
{
	const char *p;

	p = trim_spaces(buf);

#if 0
	fprintf(stderr, "%s - %s\n", __func__, p);
#endif

	if (!strncasecmp(p, "charset=", 8)) {
		p += 8;
		/* Check if the charset is double-quoted */
		if (*p == '"') {
			/* skip the first quote-character */
			p++;
		}

		if (!strncasecmp(p, "us-ascii", 8)) {
			hdr->type_charset = MIME_TYPE_CHARSET_US_ASCII;
		} else if (!strncasecmp(p, "iso-8859-1", 10)) {
			hdr->type_charset = MIME_TYPE_CHARSET_ISO_8859_1;
		} else if (!strncasecmp(p, "utf-8", 5)) {
			hdr->type_charset = MIME_TYPE_CHARSET_UTF_8;
		} else if (!strncasecmp(p, "windows-1252", 12)) {
			hdr->type_charset = MIME_TYPE_CHARSET_WINDOWS_1252;
		} else {
			hdr->type_charset = MIME_TYPE_CHARSET_UNKNOWN;
		}
	}
}

/**
 * Parse multipart parameters
 *
 * @param[in]      buf	Header line
 * @param[in|out]  hdr  Mime header struct
 */
static void parse_multipart_params(const char *buf, struct mime_header *hdr)
{
	const char *p;
	const char *endp;

	p = trim_spaces(buf);

#if 0
	fprintf(stderr, "%s - %s\n", __func__, p);
#endif
	if (!strncasecmp(p, "boundary=", 9)) {
		p += 9;

		/* Skip the first quote-character */
		if (*p == '"') {
			p++;
			endp = strchr(p, '"');
		} else {
			endp = strchr(p, ';');
		}

		if (!endp) {
			endp = strchr(p, '\n');
		}

		/* Skip the lasst char e.g: ";\n */
		if (endp) {
			endp--;
		}

		if (!substrncpy(hdr->boundary, p, endp, 1024)) {
			*hdr->boundary = '\0';
		}
	}
}

static void mime_content_subtype_text(const char *buf, struct mime_header *hdr)
{
	const char *p;

	p = buf;
	if (*p != '/') {
		hdr->subtype = MIME_SUBTYPE_UNKNOWN;
		return;
	}

	p++;

	if (!strncasecmp(p, "plain", 5)) {
		hdr->subtype = MIME_SUBTYPE_PLAIN;
		p += 5;
	} else if (!strncasecmp(p, "html", 4)) {
		hdr->subtype = MIME_SUBTYPE_HTML;
		p += 4;
	} else {
		hdr->subtype = MIME_SUBTYPE_UNKNOWN;
		return;
	}

	/* Check Parameters */
	if (*p == ';') {
		p++;
		parse_text_params(p, hdr);
	}
}

/*
 * multipart/mixed:       MIME E-mail; Defined in RFC 2045 and RFC 2046
 * multipart/alternative: MIME E-mail; Defined in RFC 2045 and RFC 2046
 * multipart/related:     MIME E-mail; Defined in RFC 2387 and used by MHTML (HTML mail)
 * multipart/form-data:   MIME Webform; Defined in RFC 2388
 * multipart/signed:      Defined in RFC 1847
 * multipart/encrypted:   Defined in RFC 1847
 */

static void mime_content_subtype_multipart(const char *buf, struct mime_header *hdr)
{
	if (*buf != '/') {
		hdr->subtype = MIME_SUBTYPE_UNKNOWN;
		return;
	}

	buf++;

	if (!strncasecmp(buf, "alternative", 11))
		hdr->subtype = MIME_SUBTYPE_ALTERNATIVE;
	else if (!strncasecmp(buf, "mixed", 5))
		hdr->subtype = MIME_SUBTYPE_MIXED;
	else if (!strncasecmp(buf, "related", 7))
		hdr->subtype = MIME_SUBTYPE_RELATED;
	else if (!strncasecmp(buf, "form-data", 9))
		hdr->subtype = MIME_SUBTYPE_FORM_DATA;
	else if (!strncasecmp(buf, "signed", 6))
		hdr->subtype = MIME_SUBTYPE_SIGNED;
	else if (!strncasecmp(buf, "encrypted", 9))
		hdr->subtype = MIME_SUBTYPE_ENCRYPTED;

	else if (!strncasecmp(buf, "byteranges", 10))
		hdr->subtype = MIME_SUBTYPE_BYTERANGES;
	else if (!strncasecmp(buf, "digest", 6))
		hdr->subtype = MIME_SUBTYPE_DIGEST;
	else if (!strncasecmp(buf, "header-set", 10))
		hdr->subtype = MIME_SUBTYPE_HEADER_SET;
	else if (!strncasecmp(buf, "parallel", 8))
		hdr->subtype = MIME_SUBTYPE_PARALLEL;
	else if (!strncasecmp(buf, "report", 6))
		hdr->subtype = MIME_SUBTYPE_REPORT;
	else if (!strncasecmp(buf, "appledouble", 11))
		hdr->subtype = MIME_SUBTYPE_APPLEDOUBLE;
	else if (!strncasecmp(buf, "voice-message", 13))
		hdr->subtype = MIME_SUBTYPE_VOICE_MESSAGE;
	else
		hdr->subtype = MIME_SUBTYPE_UNKNOWN;
}

/* Parse media type */
void mime_content_type(const char *buf, struct mime_header *hdr)
{
	const char *p;

	p = buf;

	while (*p != '\0' && isspace(*p)) {
		if (*p == '\n' || *p == '\r') {
			hdr->type = MIME_TYPE_UNKNOWN;
			return;
		}
		p++;
	}

	/* Check MIME type */
	/* text/plain is the default for e-mail */
	if (!strncasecmp(p, "text", 4)) {
		hdr->type = MIME_TYPE_TEXT;
		p += 4;
		mime_content_subtype_text(p, hdr);
	} else if (!strncasecmp(p, "multipart", 9)) {
		hdr->type = MIME_TYPE_MULTIPART;
		p += 9;
		mime_content_subtype_multipart(p, hdr);
	} else if (!strncasecmp(p, "application", 11)) {
		hdr->type = MIME_TYPE_APPLICATION;
		p += 11;
	} else if (!strncasecmp(p, "audio", 5)) {
		hdr->type = MIME_TYPE_AUDIO;
		p += 5;
	} else if (!strncasecmp(p, "example", 7)) {
		hdr->type = MIME_TYPE_EXAMPLE;
		p += 7;
	} else if (!strncasecmp(p, "image", 5)) {
		hdr->type = MIME_TYPE_IMAGE;
		p += 5;
	} else if (!strncasecmp(p, "message", 7)) {
		hdr->type = MIME_TYPE_MESSAGE;
		p += 7;
	} else if (!strncasecmp(p, "model", 5)) {
		hdr->type = MIME_TYPE_MODEL;
		p += 5;
	} else if (!strncasecmp(p, "video", 5)) {
		hdr->type = MIME_TYPE_VIDEO;
		p += 5;
	} else {
		hdr->type = MIME_TYPE_UNKNOWN;
	}
}

/* XXX Only for text/plain check RFCs ??? */
void mime_content_type_parameter(const char *buf, struct mime_header *hdr)
{
	switch (hdr->type) {
	case MIME_TYPE_TEXT:
		parse_text_params(buf, hdr);
		break;
	case MIME_TYPE_MULTIPART:
		parse_multipart_params(buf, hdr);
		break;
	default:
		break;
	}
}

void mime_content_disposition(const char *buf, struct mime_header *hdr)
{
	const char *p;

	p = buf;

	while (*p != '\0' && isspace(*p)) {
		if (*p == '\n' || *p == '\r') {
			hdr->disposition = MIME_DISPOSITION_UNKNOWN;
			return;
		}
		p++;
	}

	/* text/plain is the default */
	if (!strncasecmp(p, "inline", 6)) {
		hdr->disposition = MIME_DISPOSITION_INLINE;
		p += 6;
	} else if (!strncasecmp(p, "attachment", 10)) {
		hdr->disposition = MIME_DISPOSITION_ATTACHMENT;
		p += 10;
	} else {
		hdr->disposition = MIME_DISPOSITION_UNKNOWN;
	}

	/* Check Parameters */
	if (*p == ';') {
		fprintf(stderr, "%s - ; found\n", __func__);
	}
}

void mime_content_transfer_encoding(const char *buf, struct mime_header *hdr)
{
	const char *p;

	p = buf;

	while (*p != '\0' && isspace(*p)) {
		if (*p == '\n' || *p == '\r') {
			hdr->transfer_encoding = MIME_ENCODING_7BIT;
			return;
		}
		p++;
	}

	if (!strncasecmp(p, "7bit", 4)) {
		hdr->transfer_encoding = MIME_ENCODING_7BIT;
	} else if (!strncasecmp(p, "quoted-printable", 16)) {
		hdr->transfer_encoding = MIME_ENCODING_QUOTED_PRINTABLE;
	} else if (!strncasecmp(p, "base64", 6)) {
		hdr->transfer_encoding = MIME_ENCODING_BASE64;
	} else if (!strncasecmp(p, "8bit", 4)) {
		hdr->transfer_encoding = MIME_ENCODING_8BIT;
	} else if (!strncasecmp(p, "binary", 6)) {
		hdr->transfer_encoding = MIME_ENCODING_BINARY;
	} else {
		hdr->transfer_encoding = MIME_ENCODING_UNKNOWN;
		return;
	}

	/* Check Parameters */
	if (*p == ';') {
		fprintf(stderr, "%s - ; found\n", __func__);
	}
}

/* DEBUG */
static void debug_mime_text_charset(const struct mime_header *hdr)
{
	fprintf(stderr, "; charset=");

	switch (hdr->type_charset) {
	case MIME_TYPE_CHARSET_US_ASCII:
		fprintf(stderr, "\"us-ascii\"");
		break;
	case MIME_TYPE_CHARSET_ISO_8859_1:
		fprintf(stderr, "\"iso-8859-1\"");
		break;
	case MIME_TYPE_CHARSET_UTF_8:
		fprintf(stderr, "\"utf-8\"");
		break;
	case MIME_TYPE_CHARSET_WINDOWS_1252:
		fprintf(stderr, "\"windows-1252\"");
		break;
	case MIME_SUBTYPE_UNKNOWN:
		fprintf(stderr, "UNKNOWN");
		break;
	default:
		fprintf(stderr, "%d", hdr->type_charset);
		break;
	}
}

static void debug_mime_text(const struct mime_header *hdr)
{
	fprintf(stderr, "text/");

	switch (hdr->subtype) {
	case MIME_SUBTYPE_PLAIN:
		fprintf(stderr, "plain");
		break;
	case MIME_SUBTYPE_HTML:
		fprintf(stderr, "html");
		break;
	case MIME_SUBTYPE_UNKNOWN:
		fprintf(stderr, "UNKNOWN");
		break;
	default:
		fprintf(stderr, "%d", hdr->subtype);
		break;
	}

	debug_mime_text_charset(hdr);
}

static void debug_mime_multipart(const struct mime_header *hdr)
{
	fprintf(stderr, "multipart/");

	switch (hdr->subtype) {
	case MIME_SUBTYPE_ALTERNATIVE:
		fprintf(stderr, "alternative");
		break;
	case MIME_SUBTYPE_MIXED:
		fprintf(stderr, "mixed");
		break;
	case MIME_SUBTYPE_RELATED:
		fprintf(stderr, "related");
		break;
	case MIME_SUBTYPE_FORM_DATA:
		fprintf(stderr, "form-data");
		break;
	case MIME_SUBTYPE_SIGNED:
		fprintf(stderr, "signed");
		break;
	case MIME_SUBTYPE_ENCRYPTED:
		fprintf(stderr, "encrypted");
		break;
	case MIME_SUBTYPE_UNKNOWN:
		fprintf(stderr, "UNKNOWN");
		break;
	default:
		fprintf(stderr, "%d", hdr->subtype);
		break;
	}

	fprintf(stderr, "; boundary=\"%s\"\n", hdr->boundary);
}

static void debug_mime_application(const struct mime_header *hdr)
{
	fprintf(stderr, "application/");

	switch (hdr->subtype) {
	case MIME_SUBTYPE_UNKNOWN:
		fprintf(stderr, "UNKNOWN");
		break;
	default:
		fprintf(stderr, "%d", hdr->subtype);
		break;
	}
	fprintf(stderr, "\n");
}

static void debug_mime_audio(const struct mime_header *hdr)
{
	fprintf(stderr, "audio/");
	switch (hdr->subtype) {
	case MIME_SUBTYPE_UNKNOWN:
		fprintf(stderr, "UNKNOWN");
		break;
	default:
		fprintf(stderr, "%d", hdr->subtype);
		break;
	}
	fprintf(stderr, "\n");

}

static void debug_mime_image(const struct mime_header *hdr)
{
	fprintf(stderr, "image/");
	switch (hdr->subtype) {
	case MIME_SUBTYPE_UNKNOWN:
		fprintf(stderr, "UNKNOWN");
		break;
	default:
		fprintf(stderr, "%d", hdr->subtype);
		break;
	}
	fprintf(stderr, "\n");

}

static void debug_mime_message(const struct mime_header *hdr)
{
	fprintf(stderr, "message/");
	switch (hdr->subtype) {
	case MIME_SUBTYPE_UNKNOWN:
		fprintf(stderr, "UNKNOWN");
		break;
	default:
		fprintf(stderr, "%d", hdr->subtype);
		break;
	}
	fprintf(stderr, "\n");
}

static void debug_mime_model(const struct mime_header *hdr)
{
	fprintf(stderr, "model/");
	switch (hdr->subtype) {
	case MIME_SUBTYPE_UNKNOWN:
		fprintf(stderr, "UNKNOWN");
		break;
	default:
		fprintf(stderr, "%d", hdr->subtype);
		break;
	}
	fprintf(stderr, "\n");

}
static void debug_mime_video(const struct mime_header *hdr)
{
	fprintf(stderr, "video/");
	switch (hdr->subtype) {
	case MIME_SUBTYPE_UNKNOWN:
		fprintf(stderr, "UNKNOWN");
		break;
	default:
		fprintf(stderr, "%d", hdr->subtype);
		break;
	}
	fprintf(stderr, "\n");
}

void debug_mime_header(const struct mime_header *hdr)
{
	fprintf(stderr, "MIME Header:\n");

	/* MIME Content-Type */
	fprintf(stderr, "  Content-Type.............: ");
	switch (hdr->type) {
	case MIME_TYPE_TEXT:
		debug_mime_text(hdr);
		fprintf(stderr, "\n");
		break;
	case MIME_TYPE_MULTIPART:
		debug_mime_multipart(hdr);
		break;
	case MIME_TYPE_APPLICATION:
		debug_mime_application(hdr);
		break;
	case MIME_TYPE_AUDIO:
		debug_mime_audio(hdr);
		break;
	case MIME_TYPE_EXAMPLE:
		fprintf(stderr, "example\n");
		break;
	case MIME_TYPE_IMAGE:
		debug_mime_image(hdr);
		break;
	case MIME_TYPE_MESSAGE:
		debug_mime_message(hdr);
		break;
	case MIME_TYPE_MODEL:
		debug_mime_model(hdr);
		break;
	case MIME_TYPE_VIDEO:
		debug_mime_video(hdr);
		break;
	case MIME_TYPE_UNKNOWN:
	default:
		fprintf(stderr, "Unknown\n");
		break;
	}

	/* MIME Content-Disposition */
	fprintf(stderr, "  Content-Disposition......: ");
	switch (hdr->disposition) {
	case MIME_DISPOSITION_INLINE:
		fprintf(stderr, "inline\n");
		break;
	case MIME_DISPOSITION_ATTACHMENT:
		fprintf(stderr, "attachment\n");
		break;
	case MIME_DISPOSITION_UNKNOWN:
	default:
		fprintf(stderr, "Unknown\n");
		break;
	}

	/* MIME Content-Transfer-Encoding */
	fprintf(stderr, "  Content-Transfer-Encoding: ");
	switch (hdr->transfer_encoding) {
	case MIME_ENCODING_7BIT:
		fprintf(stderr, "7bit\n");
		break;
	case MIME_ENCODING_QUOTED_PRINTABLE:
		fprintf(stderr, "quoted-printable\n");
		break;
	case MIME_ENCODING_BASE64:
		fprintf(stderr, "base64\n");
		break;
	case MIME_ENCODING_8BIT:
		fprintf(stderr, "8bit\n");
		break;
	case MIME_ENCODING_BINARY:
		fprintf(stderr, "binary\n");
		break;
	case MIME_ENCODING_UNKNOWN:
	default:
		fprintf(stderr, "Unknown\n");
		break;
	}
}


