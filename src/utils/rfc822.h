#ifndef RFC822_UTILS_H
#define RFC822_UTILS_H

int rfc822_check_header_end(const char *line);
unsigned int rfc822_check_field_folding(const char *line);


/* Date Time Functions */

/* XXX */
time_t rfc822_parse_date(char *date_string);


/* Alternative From curier project */
time_t rfc822_parsedt(const char *rfcdt);
const char *rfc822_mkdt(time_t t);



#endif	/* RFC822_UTILS_H */
