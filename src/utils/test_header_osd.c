#define  _BSD_SOURCE

#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>

#include <sys/types.h>
#include <sys/stat.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xatom.h>
#include <X11/Xft/Xft.h>

#include "string_utils.h"
#include "file_utils.h"
#include "rfc5322.h"
#include "list.h"
#include "md5_utils.h"
#include "osd.h"
#include "icons_utils.h"
#include "mail.h"

static struct mail_entry mail;

void parse_file(const char *filename)
{
	FILE *fh;

	fh = fopen(filename, "r");
	if (!fh)
		return;

	parse_mail_header(fh, &mail.hdr);

	fclose(fh);
}

static void print_mail(struct mail_entry *mail)
{
	if (!mail)
		return;

	print_mail_header(&mail->hdr);
}

void display_osd(int argc, char **argv)
{
	struct notify_object no;

	char *icon = NULL;

	Display *dpy;
	XEvent e;

	int running = 1;

	/* Open connection with the server */
	dpy = XOpenDisplay(NULL);
	if (dpy == NULL) {
		fprintf(stderr, "Cannot open display\n");
		return;
	}

	icon = icon_get_path((char *) mail.hdr.addr_md5);

	osd_init(dpy, &no, mail.hdr.from, mail.hdr.subject, icon);

	/* Create OSD object */
	if (osd_new_notify_object(argc, argv, &no)) {
		goto out;
	}

	/* Select kind of events we are interested in */
	XSelectInput(no.frame.display, no.frame.win,
			ExposureMask | KeyPressMask | ButtonPressMask);

	/* Map (show) the window */
	XMapWindow(no.frame.display, no.frame.win);

	/* event loop */
	while (running) {
		XNextEvent(no.frame.display, &e);

		switch (e.type) {
		case Expose:
			/* Draw or redraw the window */
			osd_show_notify_object(&no);
			break;

		/* Exit on key press or mouse button press */
		case KeyPress:
		case ButtonPress:
			running = 0;
			break;
		}
	}

	osd_delete_notify_object(&no);

out:
	icon_free_path(icon);

	/* Close connection to server */
	XCloseDisplay(no.frame.display);
}

int main(int argc, char **argv)
{
	const char *file;

	if (argc < 2) {
		fprintf(stderr, "%s <file>\n", argv[0]);
		return -1;
	}

	file = argv[1];

	if (file_type(file))
		return -1;

	parse_file(file);

	print_mail(&mail);

	display_osd(argc, argv);

	return 0;
}

