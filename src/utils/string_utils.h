#ifndef STRING_UTILS_H
#define STRING_UTILS_H

char *substrncpy(char *dest, const char *start, const char *end, size_t n) __attribute__((nonnull (1, 2, 3)));
const char *trim_spaces(const char *line);

#endif	/* STRING_UTILS_H */
