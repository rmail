#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "list.h"

/*#define DEBUG_LIST*/

void list_print(struct mail_list *lst)
{
	size_t n;

	if (!lst)
		return;

	for (n = 0; n < lst->n; n++) {
		fprintf(stderr, "[%u] ", n);
		print_mail_header(&lst->list[n].hdr);
	}
}

/**
 * Allocate a list via malloc of n_elem elements
 *
 * @param[in|out] n_elem	number of elements in the list
 * @param[in]     limit		max number of elements
 *
 * @retval Pointer to the new allocated list or NULL on error
 */
struct mail_list *list_create(size_t n_elem, size_t limit)
{
	struct mail_list *lst;

	if (n_elem == 0)
		return NULL;

	if (n_elem > limit)
		n_elem = limit;

	lst = malloc(sizeof(struct mail_list) + n_elem * sizeof(struct mail_entry));
	if (!lst) {
		fprintf(stderr, "%s - MALLOC: %s", __func__, strerror(errno));
		return NULL;
	}

#ifdef DEBUG_LIST
	fprintf(stderr, "%s - MALLOC: item %lu [%lu]\n", __func__,
			(unsigned long) n_elem,
			(unsigned long) (sizeof(struct mail_list) + n_elem * sizeof(struct mail_entry)));
#endif

	lst->n = n_elem;
	lst->nfree = 0;

	return lst;
}

void list_free(struct mail_list *lst)
{
	if (lst) {
		free(lst);
		lst = NULL;
#ifdef DEBUG_LIST
		fprintf(stderr, "%s - FREE: list\n", __func__);
#endif
	}
}

int list_add_entry(struct mail_list *lst, struct mail_entry *entry)
{
	size_t n = 0;

	if (lst->nfree < LIST_LIMIT) {
		n = lst->nfree;
		/* Update List */
		memcpy(&lst->list[n], entry, sizeof(struct mail_entry));

		/* Update 1st free slot index */
		lst->nfree += 1;
		return 0;
	}

	/* No free slot */
	return -1;
}
