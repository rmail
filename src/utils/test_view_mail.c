#define  _BSD_SOURCE

#include <stdio.h>
#include <string.h>
#include <limits.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <time.h>
#include <ctype.h>

#include <sys/types.h>
#include <sys/stat.h>

#include "string_utils.h"
#include "file_utils.h"
#include "rfc5322.h"
#include "rfc2047.h"
#include "rfc822.h"
#include "mime.h"
#include "md5_utils.h"
#include "mail.h"

static int hdr_only = 0;
static int mime_only = 0;

static void plain_body(char *line)
{
	printf("%s", line);
}


static void qp_body(char *line)
{
	char buf[1024];

	rfc2047_decode_quoted_printable(buf, line, sizeof(buf) - 1);
	printf("%s", buf);
}

static void parse_mail_body(FILE *fh, struct mail_header *hdr)
{
	char buf[1024];
	void (*decode_body)(char *line);

	switch (hdr->mime.transfer_encoding) {
	case MIME_ENCODING_QUOTED_PRINTABLE:
		decode_body = qp_body;
		break;
	default:
		decode_body = plain_body;
		break;
	}

	(void) fseek(fh, hdr->body_offset, SEEK_SET);

	while (fgets(buf, sizeof(buf), fh)) {
		decode_body(buf);
	}
}

void usage(const char *appname)
{
	fprintf(stderr, "Usage:\n%s options\n", appname);
	fprintf(stderr, "Options:\n");
	fprintf(stderr,	" -h Parse header only\n"
			" -m Parse MIME only\n"
			" -f <file> Parse file\n"
			" -d <maildir> Parse maildir\n");
}

static void parse_file(const char *file)
{
	FILE *fh;
	struct mail_header mail_hdr;

	if (file_type(file))
		return;

	fh = fopen(file, "r");
	if (!fh) {
		return;
	}

	init_mail_header(&mail_hdr);

	strncpy(mail_hdr.filename, file, 1024);

	parse_mail_header(fh, &mail_hdr);

	fprintf(stderr, "-------------------------------------------\n");
	fprintf(stderr, "Test file: %s\n", mail_hdr.filename);
	fprintf(stderr, "-------------------------------------------\n");
	if (!mime_only) {
		fprintf(stderr, "Date   : %s\n", rfc822_mkdt(mail_hdr.date));
		fprintf(stderr, "From   : %s\n", mail_hdr.from);
		fprintf(stderr, "Subject: %s\n", mail_hdr.subject);
	}

	debug_mime_header(&mail_hdr.mime);


	if (!hdr_only) {
		fprintf(stderr, "-------------------------------------------\n");
		/* Message Body */
		parse_mail_body(fh, &mail_hdr);
	}

	fclose(fh);
}


int main(int argc, char **argv)
{
	int opt;

	char *file = NULL;
	char *maildir_path = NULL;

	char old_path[PATH_MAX];
	int rc;

	while ((opt = getopt(argc, argv, "d:f:hm")) != -1) {
		switch (opt) {
		case 'd':
			maildir_path = optarg;
			break;
		case 'f':
			file = optarg;
			break;
		case 'h':
			hdr_only = 1;
			break;
		case 'm':
			mime_only = 1;
			break;
		default:
			fprintf(stderr, "Unknown parameter.\n");
			usage(argv[0]);
			return -1;
                }
        }

	if ((file == NULL) && (maildir_path == NULL)) {
		usage(argv[0]);
		return -1;
	}
	if ((file != NULL) && (maildir_path != NULL)) {
		fprintf(stderr, "-f and -d are mutually esclusive!.\n");
		usage(argv[0]);
	}

	if (file != NULL) {
		parse_file(file);
		return 0;
	}

	/* Parse all maildir */
	if (!getcwd(old_path, PATH_MAX)) {
		fprintf(stderr, "%s - GETCWD: %s\n", __func__, strerror(errno));
		return -1;
	}

	if (chdir(maildir_path) < 0) {
		fprintf(stderr, "%s - CHDIR: %s\n", __func__, strerror(errno));
		return -1;
	}

	rc = scan_dir(maildir_path, parse_file);

	if (chdir(old_path) < 0) {
		fprintf(stderr, "%s - Cannot restore path %s!\n", __func__, strerror(errno));
	}

	return rc;
}

