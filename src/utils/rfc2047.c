#include <stdio.h>
#include <string.h>

#include "string_utils.h"

/*
 * Index_hex and Index_64 imported from Mutt:handler.c
 * decode_quotedprintable() and decode_base64() as well
 */
static const int index_hex[128] = {
/*       0   1   2   3   4   5   6   7   8   9   A   B   C   D   E   F */
	-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
	 0,  1,  2,  3,  4,  5,  6,  7,  8,  9, -1, -1, -1, -1, -1, -1,
	-1, 10, 11, 12, 13, 14, 15, -1, -1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
	-1, 10, 11, 12, 13, 14, 15, -1, -1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1
};

static const int index_64[128] = {
/*       0   1   2   3   4   5   6   7   8   9   A   B   C   D   E   F */
	-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 62, -1, -1, -1, 63,
	52, 53, 54, 55, 56, 57, 58, 59, 60, 61, -1, -1, -1, -1, -1, -1,
	-1,  0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14,
	15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, -1, -1, -1, -1, -1,
	-1, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40,
	41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, -1, -1, -1, -1, -1
};

#define HEX_VAL(c) index_hex[(unsigned int)(c)]
#define BASE64_VAL(c) index_64[(unsigned int)(c)]

/* Decode a string from base64 */
void rfc2047_decode_base64(char *dest, const char *src, size_t n)
{
	const char *end;

	int c;
	int b = 0;
	int k = 0;

	if (!dest || !src || (n == 0)) {
		return;
	}

	end = src + n;

	for ( ; src < end; src++) {
		if (*src == '=')
			break;
		if ((*src & 0x80) || (c = BASE64_VAL(*src)) == -1)
			continue;
		if (k + 6 >= 8) {
			k -= 2;
			*(dest++) = b | (c >> k);
			b = c << (8 - k);
		} else {
			b |= c << (k + 2);
			k += 6;
		}
		*dest = '\0';
	}
}

void rfc2047_decode_quoted_printable(char *dest, const char *src, size_t n)
{
	const char *end;

	if (!dest || !src || (n == 0)) {
		return;
	}

	end = src + n;

	while (src < end) {
		if (*src == '_') {
			*dest = ' ';
		} else if ((*src == '=') && (src + 2 < end) &&
			  (!(*(src + 1) & 0x80) && HEX_VAL(*(src + 1)) != -1) &&
			  (!(*(src + 2) & 0x80) && HEX_VAL(*(src + 2)) != -1))
		{
			*dest = (HEX_VAL(*(src + 1)) << 4) |
				 HEX_VAL(*(src + 2));
			src += 2;
		} else {
			*dest = *src;
		}
		src++;
		dest++;
	}

	*dest = '\0';
}

/*
 * encoded-word = "=?" charset "?" encoding "?" encoded-text "?="
*/
void rfc2047_decode_word(char *dest, const char *src, size_t n)
{
	char *start = NULL;
	char *end = NULL;

	char *encoding;
	char *charset;

	start = strstr(src, "=?");
	end = strstr(src, "?=");

	if (!start || !end) {
		strncpy(dest, src, n);
		return;
	}

	/* Adjust pointers */
	start += 2;

	charset = start;
#if 0
	printf("charset @%d\n", charset - src);
#endif

	encoding = strchr(start, '?');
	if (encoding == end) {
		/* Something wrong */
	}
	encoding++;

#if 0
	printf("encoding @%d\n", encoding - src);
#endif

	start = encoding + 2;

	if (*encoding == 'Q') {
		rfc2047_decode_quoted_printable(dest, start, end - start);
	} else if (*encoding == 'B') {
		rfc2047_decode_base64(dest, start, end - start);
	} else {
		fprintf(stderr, "Unkonwn encoding '%c'\n", *encoding);
	}
}
