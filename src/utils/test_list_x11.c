#define  _BSD_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <dirent.h>
#include <errno.h>
#include <fcntl.h>

#include <sys/types.h>
#include <sys/stat.h>

#include <X11/Intrinsic.h>
#include <X11/StringDefs.h>
#include <X11/Shell.h>

#include <X11/Xaw/Form.h>
#include <X11/Xaw/Viewport.h>
#include <X11/Xaw/Command.h>
#include <X11/Xaw/AsciiText.h>
#include <X11/Xaw/Label.h>
#include <X11/Xaw/Box.h>

#include "md5_utils.h"
#include "string_utils.h"
#include "file_utils.h"
#include "rfc5322.h"
#include "rfc822.h"
#include "icons_utils.h"

#include "iv_widget.h"

#include "list.h"

struct applet {
	XtAppContext	ctx;	/* Application context */
	Widget		shell;	/* Application shell widget */
	Widget		form;	/* Application top Form widget */
};

static struct mail_list *mlist;

void list_add_from_file(const char *filename)
{
	FILE *fh;
	struct mail_entry entry;

	/* Check if is a regular file */
	if (file_type(filename)) {
		return;
	}

	fh = fopen(filename, "r");
	if (!fh)
		return;

	strncpy(entry.hdr.filename, filename, LINE_LENGTH);

	parse_mail_header(fh, &entry.hdr);

	fclose(fh);

	if (mlist) {
		if (list_add_entry(mlist, &entry)) {
			fprintf(stderr, "%s - No free slot for %s\n", __func__, filename);
		} else {
			/* fprintf(stderr, "%s - File %s added to list\n", __func__, filename); */
		}
	}
}


/*======================================================================
 * X11 CODE
 *======================================================================*/

/* The callback function for the quit button. */
void quit_callback(Widget w,
		   __attribute__((unused)) XtPointer client_data,
		   __attribute__((unused)) XtPointer call_data)
{
	list_free(mlist);

	XtDestroyApplicationContext(XtWidgetToApplicationContext(w));

	exit(0);
}

static void fix_scrollbar_translations(Widget w, String name)
{
	Widget sb = NULL; /* scrollbar widget */

	/* New scrollbar translation */
	String trans = "#override "
	"<Btn1Down>: StartScroll(Forward)\n"		/* Left button */
	"<Btn2Down>: \n"				/* Middle button */
	"<Btn3Down>: StartScroll(Backward)\n"		/* Right button */
	"<Btn4Down>: StartScroll(Backward)\n"		/* Wheel down */
	"<Btn5Down>: StartScroll(Forward)\n"		/* Wheel up */
	"<Btn1Motion>: MoveThumb() NotifyThumb()\n"
	"<BtnUp>: NotifyScroll(Proportional) EndScroll()";

	if (!name)
		return;

	sb = XtNameToWidget(w, name);
	if (sb)
		XtOverrideTranslations(sb, XtParseTranslationTable(trans));
	else
		fprintf(stderr, "%s: WARNING cannon set %s translation!\n", __func__, name);
}


/* destroy_popup: Destroys the popup box widget.
 * Arguments:
 *	w - *** UNUSED ***.
 *	box - the box widget.  This widget is a direct child of the popup shell to destroy.
 *	p - *** UNUSED **.
 */
static void destroy_popup(__attribute__((unused)) Widget w,
			  XtPointer box,
			  __attribute__((unused)) XtPointer p)
{
	Widget popup;

	popup = XtParent((Widget) box);

	XtDestroyWidget(popup);
}

static void create_popup(Widget w, XtPointer client_data,
			 __attribute__((unused)) XtPointer call_data)

{
	Widget popup;
	Widget box;
	Widget address;
	Widget subj;
	Widget date;
	Widget text;
	Widget ok_btn;

	struct mail_entry *me = (struct mail_entry *) client_data;

	popup = XtVaCreatePopupShell("PopupText", transientShellWidgetClass, w,
				     XtNborderWidth, 0,
				     XtNx, 100,
				     XtNy, 100,
				     /*XtNwidth, 400, */
				     NULL);

	/* PopupBox contains Label and Slider */
	box = XtVaCreateManagedWidget("PopupForm", formWidgetClass, popup,
				      XtNborderWidth, 0,
				      NULL);

	address = XtVaCreateManagedWidget("Address", labelWidgetClass, box,
					XtNborderWidth,	0,
					XtNwidth,	400,
					XtNlabel,	me->hdr.from,
					XtNjustify,	XtJustifyLeft,
					NULL);

	date = XtVaCreateManagedWidget("date", labelWidgetClass, box,
					XtNfromHoriz,	address,
					XtNborderWidth,	0,
					XtNwidth,	400,
					XtNlabel,	rfc822_mkdt(me->hdr.date),
					XtNjustify,	XtJustifyLeft,
					NULL);

	subj = XtVaCreateManagedWidget("subj", labelWidgetClass, box,
					XtNfromVert,	date,
					XtNborderWidth,	0,
					XtNwidth,	400,
					XtNlabel,	me->hdr.subject,
					XtNjustify,	XtJustifyLeft,
					NULL);

	text = XtVaCreateManagedWidget("Text", asciiTextWidgetClass, box,
					XtNfromVert,	subj,
					XtNresizable,	True,
					XtNtype,	XawAsciiFile,
					XtNstring,	me->hdr.filename,
					XtNwidth,	800,
					XtNheight,	500,
					XtNdisplayCaret, False,
					XtNscrollHorizontal, XawtextScrollWhenNeeded,
					XtNscrollVertical,   XawtextScrollAlways,
					XtNeditType,	XawtextRead,
					XtNtop,		XtChainTop,
					NULL);

	ok_btn = XtVaCreateManagedWidget("Ok", commandWidgetClass, box,
					XtNfromVert,  text,
					XtNleft,      XtChainLeft,
					XtNright,     XtChainLeft,
					XtNtop,       XtChainBottom,
					XtNbottom,    XtChainBottom,
					XtNresizable, False,
					NULL);

	XtAddCallback(ok_btn, XtNcallback, destroy_popup, (XtPointer) box);

	/* Popup widget */
	XtPopup(popup, XtGrabNone);
#if 0
	fprintf(stderr, "%s box=%p popup=%p\n", __func__, (void *) box, (void *) popup);
#endif
}

static int create_applet(struct applet *app, int argc, char **argv)
{
	XtSetLanguageProc(NULL, (XtLanguageProc) NULL, NULL);

	/* Open Xt application shell widget */
	app->shell = XtVaOpenApplication(&app->ctx, "TestListX11", NULL, 0,
					&argc, argv, NULL,
					applicationShellWidgetClass,
					NULL);

	/* Create top form widget */
	app->form = XtVaCreateManagedWidget("AppForm", formWidgetClass,
					app->shell, NULL);
	return 0;
}

/* The callback function for the quit button. */
void item_callback(Widget w, XtPointer item_string, XtPointer call_data)
{
	create_popup(w, item_string, call_data);
}

static int list_add_item(Widget parent, const struct mail_entry *item)
{
	Display *d;
	Visual  *visual;
	int screen;

	/* Depth of the root window */
	int depth;

	char   *icon = NULL;
	XImage *pix = NULL;

	Widget box_v;
	Widget box_h;
	Widget addr;
	Widget subj;
	Widget iv;

	d = XtDisplay(parent);

	screen = DefaultScreen(d);
	depth  = DisplayPlanes(d, screen);
	visual = XDefaultVisual(d, screen);

	/* Allocated icon pathname */
	icon = icon_get_path((char *) item->hdr.addr_md5);

	/* Create XImage from PNG file */
	pix = create_ximage_from_PNG(d, visual, depth, icon, NULL);

	/* Free allocated icon pathname with 'icon_get_path()' */
	icon_free_path(icon);

	box_h = XtVaCreateManagedWidget("box_h", boxWidgetClass, parent,
					XtNorientation, XtorientHorizontal,
					NULL);

	iv = XtVaCreateManagedWidget("IV", imageViewWidgetClass, box_h,
					     XtNximage,    pix,
					     XtNimageType, 0,
					     NULL);

	box_v = XtVaCreateManagedWidget("box_v", boxWidgetClass, box_h,
					XtNborderWidth,	0,
					XtNorientation, XtorientVertical,
					NULL);

	addr = XtVaCreateManagedWidget("from", labelWidgetClass, box_v,
					XtNborderWidth,	0,
					XtNwidth,	400,
					XtNlabel,	item->hdr.from,
					XtNjustify,	XtJustifyLeft,
					NULL);

	subj = XtVaCreateManagedWidget("subj", labelWidgetClass, box_v,
					XtNborderWidth,	0,
					XtNwidth,	400,
					XtNlabel,	item->hdr.subject,
					XtNjustify,	XtJustifyLeft,
					NULL);

	/* Set handler for mouse button press on widgets
	 * Pass the file to the callbacks
	 * Button1Mask
	 * Button2Mask
	 * Button3Mask
	 * Button4Mask
	 * Button5Mask
	 * All the above ButtonPressMask
	 */
#if 0
	XtAddEventHandler(addr, Button1Mask, False, (XtEventHandler) item_callback, (XtPointer) item);
	XtAddEventHandler(subj, Button1Mask, False, (XtEventHandler) item_callback, (XtPointer) item);
	XtAddEventHandler(iv,   Button1Mask, False, (XtEventHandler) item_callback, (XtPointer) item);
#else
	XtAddEventHandler(addr, ButtonPressMask, False, (XtEventHandler) item_callback, (XtPointer) item);
	XtAddEventHandler(subj, ButtonPressMask, False, (XtEventHandler) item_callback, (XtPointer) item);
	XtAddEventHandler(iv,   ButtonPressMask, False, (XtEventHandler) item_callback, (XtPointer) item);
#endif

	return 0;
}

int xutil_main(struct mail_list *lst, int argc, char **argv)
{
	size_t n;
	int rc;
	struct applet app;

	Widget vport;		/* ViewPort (list container) */
	Widget button;
	Widget box;

	rc = create_applet(&app, argc, argv);

	vport = XtVaCreateManagedWidget("vport", viewportWidgetClass,
					app.form,
					XtNresizable,	False,
					XtNwidth,	450,
					XtNheight,	400,
					XtNallowVert,	True,	/* Display Vertical scrollbar */
					XtNallowHoriz,	True,	/* Display Horizontal scrollbar */
					XtNuseBottom,	True,	/* Horizontal scrollbar at bottom */
					XtNtop,		XtChainTop,
					NULL);
	if (vport == NULL)
		return -1;

	box = XtVaCreateManagedWidget("box", boxWidgetClass, vport,
					XtNorientation, XtorientVertical,
					NULL);

	if (lst) {
		for (n = 0; n < lst->n; n++) {
			list_add_item(box, &lst->list[n]);
		}
	}

	button = XtVaCreateManagedWidget("quit", commandWidgetClass,
					app.form,
					XtNfromVert,  vport,
					XtNleft,      XtChainLeft,
					XtNright,     XtChainLeft,
					XtNtop,       XtChainBottom,
					XtNbottom,    XtChainBottom,
					XtNresizable, False,
					NULL);

	XtAddCallback(button, XtNcallback, quit_callback, NULL);

	XtRealizeWidget(app.shell);

	/* Override default translations (A.k.a. bind actions to widgets) */
	fix_scrollbar_translations(vport, "horizontal");
	fix_scrollbar_translations(vport, "vertical");

	XtAppMainLoop(app.ctx);

	return 0;
}

/*======================================================================*/
/* END X11 CODE								*/
/*======================================================================*/

int main(int argc, char **argv)
{
	int rc;
	char old_path[PATH_MAX];

	const char *path;

	size_t n;

	if (argc < 2) {
		path  = "/home/roberto/Maildir/cur/";
	} else {
		path = argv[1];
	}

	if (!getcwd(old_path, PATH_MAX)) {
		fprintf(stderr, "%s - GETCWD: %s\n", __func__, strerror(errno));
		return -1;
	}

	if (chdir(path) < 0) {
		fprintf(stderr, "%s - CHDIR: %s\n", __func__, strerror(errno));
		return -1;
	}

	n = dir_entries(path);
	if (n < 1) {
		fprintf(stderr, "%s is empty!\n", path);
		rc = -1;
		goto out_1;
	}

	/* Init mail list */
	mlist = list_create(n, LIST_LIMIT);
	if (!mlist) {
		rc = -1;
		goto out_1;
	}

	fprintf(stderr, "Try to process %lu files\n", (unsigned long) mlist->n);

	rc = scan_dir(path, list_add_from_file);

	/*int*/ xutil_main(mlist, argc, argv);

	list_free(mlist);

out_1:
	if (chdir(old_path) < 0) {
		fprintf(stderr, "%s - Cannot restore path %s!\n", __func__, strerror(errno));
	}

	return rc;
}
