#ifndef FILE_UTILS_H
#define FILE_UTILS_H

int file_type(const char *file);
size_t dir_entries(const char *path);
int scan_dir(const char *path, void (*file_callback)(const char *));

#endif	/* FILE_UTILS_H */
