#include <string.h>
#include <time.h>

#include "rfc822.h"
#include "rfc5322.h"

#include "md5_utils.h"

#include "mime.h"

#include "mail.h"


void print_mail_header(struct mail_header *hdr)
{
	int i;
	for (i = 0; i < 16; i++) {
			fprintf(stderr, "%02x", hdr->addr_md5[i]);
	}
	fprintf(stderr, "File: %s -- %s -- %s\n",
				hdr->filename,
				hdr->from,
				hdr->subject);
}

void init_mail_header(struct mail_header *hdr)
{
	memset(hdr->from, 0, sizeof(hdr->from));
	memset(hdr->subject, 0, sizeof(hdr->subject));

	hdr->body_offset = 0L;

	/* MIME */
	/* [RFC2045]
	 * If no Content-Type header field is specified, default to:
	 * Content-type: text/plain; charset=us-ascii
	 */
	hdr->mime.type = MIME_TYPE_TEXT;
	hdr->mime.type_charset = MIME_TYPE_CHARSET_US_ASCII;
	hdr->mime.subtype = MIME_SUBTYPE_PLAIN;
	hdr->mime.disposition = MIME_DISPOSITION_UNKNOWN;

	/* [RFC2045] "Content-Transfer-Encoding: 7BIT" is assumed if the
	 *  Content-Transfer-Encoding header field is not present.
	 *  ... If a Content-Transfer-Encoding header field appears as part
	 *  of a message header, it applies to the entire body of that message.
	 *  If a Content-Transfer-Encoding header field appears as part of an
	 *  entity's headers, it applies only to the body of that entity.
	 *  If an entity is of type "multipart" the Content-Transfer-Encoding
	 *  is not permitted to have any value other than "7bit", "8bit" or
	 *  "binary".
	 */
	hdr->mime.transfer_encoding = MIME_ENCODING_7BIT;
}

void parse_mail_header(FILE *fh, struct mail_header *hdr)
{
	char buf[1024];
	char *crlf;
	unsigned int fld_cont;	/* RFC822 Header field folding flag */
	unsigned int fa_valid;	/* Valid From Address flag */

	crlf = NULL;
	fld_cont = 0;
	fa_valid = 0;

	/* Scan all lines */
	do {
		/*
		 * Don't get a new line if we are coming from a folding
		 * field
		 */
		if (!fld_cont) {
			if (fgets(buf, sizeof(buf), fh) == NULL) {
				break;
			}
		}

		fld_cont = 0;

		if (!strncasecmp(buf, "From:", 5)) {
			rfc5322_extract_address(hdr->from, buf, sizeof(hdr->from));
			fa_valid = 1;
		} else if (!strncasecmp(buf, "Reply-to:", 9)) {
			if (fa_valid)
				continue;

			if (!rfc5322_extract_address(hdr->from, buf, sizeof(hdr->from))) {
				fa_valid = 1;
			}

		} else if (!strncasecmp(buf, "Subject:", 8)) {
			rfc5322_extract_subject(hdr->subject, buf, sizeof(hdr->subject));
		} else if (!strncasecmp(buf, "Date:", 5)) {
			hdr->date = rfc822_parsedt(buf);
		} else if (!strncasecmp(buf, "Content-Type:", 13)) {
			mime_content_type(buf + 14, &hdr->mime);

			while (fgets(buf, sizeof(buf), fh)) {
				if (rfc822_check_field_folding(buf)) {
					/* Decode folded line */
					mime_content_type_parameter(buf, &hdr->mime);
				} else {
					fld_cont = 1;
					break;
				}
			}
		} else if (!strncasecmp(buf, "Content-Disposition:", 20)) {
			mime_content_disposition(buf + 21, &hdr->mime);
		} else if (!strncasecmp(buf, "Content-Transfer-Encoding:", 26)) {
			mime_content_transfer_encoding(buf + 27, &hdr->mime);
		} else {
			/*printf("[SKIP] ------- %s", buf);*/
		}
	} while (!rfc822_check_header_end(buf));

	/* If a valid 'from' address was found calc the MD5 of this address */
	if (fa_valid) {
		crlf = strrchr(hdr->from, '\n');
		if (crlf)
			*crlf = '\0';

		md5_calc_sum(hdr->from, hdr->addr_md5);
	}

	/* Get file offset of the message body */
	hdr->body_offset = ftell(fh);
}
