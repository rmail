#define  _BSD_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <dirent.h>
#include <errno.h>
#include <fcntl.h>

#include <sys/types.h>
#include <sys/stat.h>

#include "md5_utils.h"
#include "string_utils.h"
#include "file_utils.h"
#include "rfc5322.h"

#include "list.h"

static struct mail_list *mlist;

void list_add_from_file(const char *filename)
{
	FILE *fh;
	struct mail_entry entry;

	/* Check if is a regular file */
	if (file_type(filename)) {
		return;
	}

	fh = fopen(filename, "r");
	if (!fh)
		return;

	strncpy(entry.hdr.filename, filename, LINE_LENGTH);

	parse_mail_header(fh, &entry.hdr);

	fclose(fh);

	if (mlist) {
		if (list_add_entry(mlist, &entry)) {
			fprintf(stderr, "%s - No free slot for %s\n", __func__, filename);
		} else {
			/* fprintf(stderr, "%s - File %s added to list\n", __func__, filename); */
		}
	}

}

int main(int argc, char **argv)
{
	int rc;
	char old_path[PATH_MAX];

	const char *path;

	size_t n;

	if (argc < 2) {
		path  = "/home/roberto/Maildir/cur/";
	} else {
		path = argv[1];
	}

	if (!getcwd(old_path, PATH_MAX)) {
		fprintf(stderr, "%s - GETCWD: %s\n", __func__, strerror(errno));
		return -1;
	}

	if (chdir(path) < 0) {
		fprintf(stderr, "%s - CHDIR: %s\n", __func__, strerror(errno));
		return -1;
	}

	n = dir_entries(path);
	if (n < 1) {
		fprintf(stderr, "%s is empty!\n", path);
		rc = -1;
		goto out_1;
	}

	/* Init mail list */
	mlist = list_create(n, LIST_LIMIT);
	if (!mlist) {
		rc = -1;
		goto out_1;
	}

	fprintf(stderr, "Try to process %lu files\n", (unsigned long) mlist->n);


	rc = scan_dir(path, list_add_from_file);

	list_print(mlist);
	list_free(mlist);

out_1:
	if (chdir(old_path) < 0) {
		fprintf(stderr, "%s - Cannot restore path %s!\n", __func__, strerror(errno));
	}

	return rc;
}
