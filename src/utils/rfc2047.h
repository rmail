#ifndef RFC2047_UTILS_H
#define RFC2047_UTILS_H

void rfc2047_decode_quoted_printable(char *dest, const char *src, size_t n);
void rfc2047_decode_base64(char *dest, const char *src, size_t n);
void rfc2047_decode_word(char *dest, const char *src, size_t n);

#endif	/* RFC2047_UTILS_H */
