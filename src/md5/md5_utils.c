#include <string.h>
#include "md5.h"

void md5_calc_sum(const char *str, unsigned char *md5sum)
{
	md5_context ctx;

	md5_starts(&ctx);
	md5_update(&ctx, (unsigned char *) str, strlen(str));
	md5_finish(&ctx, md5sum);
}
