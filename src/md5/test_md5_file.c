#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "md5.h"

int main(int argc, char **argv)
{
	FILE *f;
	int i, j;
	md5_context ctx;
	unsigned char buf[1000];
	unsigned char md5sum[16];

	if (argc < 2) {
	        printf( "Usage: %s <file>\n", argv[0]);
		return -1;
	}

	f = fopen(argv[1], "rb");
	if (!f) {
            perror("fopen");
            return -1;
        }

        md5_starts(&ctx);

	while ((i = fread(buf, 1, sizeof(buf), f)) > 0) {
		md5_update(&ctx, buf, i);
        }

	md5_finish(&ctx, md5sum);

	for (j = 0; j < 16; j++) {
		printf("%02x", md5sum[j]);
	}

	printf("  %s\n", argv[1]);

	return 0;
}
