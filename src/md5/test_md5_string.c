#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "md5.h"

int main(int argc, char **argv)
{
	int i;
	md5_context ctx;
	unsigned char md5sum[16];

	if (argc < 2) {
	        printf("\nmissing string.\n");
		return -1;
	}

	md5_starts(&ctx);
	md5_update(&ctx, (uint8 *) argv[1], strlen(argv[1]));
	md5_finish(&ctx, md5sum);

	for (i = 0; i < 16; i++) {
		printf("%02x", md5sum[i]);
	}

	printf("\n");

	return 0;
}
