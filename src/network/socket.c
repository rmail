/**
 * Copyright (C) 2011 - Roberto Branciforti
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

/* socket.c - socket utilities */

#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <netdb.h>

#include <sys/types.h>
#include <sys/socket.h>


/**
 * Create a socket and return its descriptor.
 *
 * @param[in] ai socket parameters
 *
 * @retval see socket(2)
 */
inline int socket_open(struct addrinfo *ai)
{
	return socket(ai->ai_family, ai->ai_socktype, ai->ai_protocol);
}

/**
 * Close a socket
 *
 * @param[in] sockfd	socket descriptor
 *
 * @retval see close(2)
 */
inline int socket_close(int sockfd)
{
	return close(sockfd);
}

/**
 * Tries to connect to a specified server on a given machine.
 * The function waits until a connection with the server is established and
 * then returns a socket descriptor connected to the server, -1 on error.
 *
 * @param[in] hostname
 * @param[in] port
 *
 * @retval socket descriptor connected to the server, -1 on error.
 */
int connect_to_tcp_server(const char *hostname, const char *port)
{
	struct addrinfo hints;
	struct addrinfo *res;
	struct addrinfo *rp;

	int sockfd;
	int rc;

	memset(&hints, 0, sizeof(struct addrinfo));

#if 0
	/* See RFC 3490, Internationalizing  Domain  Names  in  Applications */
	hints.ai_flags = AI_IDN;
#else
	hints.ai_flags = AI_ADDRCONFIG;
#endif

#if 1	/* IPv4 Internet protocols (cfr. ip(7) */
	hints.ai_family = AF_INET;
#else
	/*  IPv6 Internet protocols (cfr. ipv6(7) */
	hints.ai_family = AF_INET6;
#endif

	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = 6;

	rc = getaddrinfo(hostname, port, &hints, &res);
	if (rc != 0) {
		fprintf(stderr, "%s - GETADDRINFO: %s\n", __func__, gai_strerror(rc));
		return -1;
	}

	sockfd = -1;

	for (rp = res; rp != NULL; rp = rp->ai_next) {
		sockfd = socket_open(rp);
		if (sockfd < 0)
			continue;

		if (connect(sockfd, rp->ai_addr, rp->ai_addrlen) == 0) {
			break;
		}

		if (socket_close(sockfd) != 0) {
			fprintf(stderr, "%s - CLOSE: %s\n", __func__, strerror(errno));
			freeaddrinfo(res);
			return -1;
		}
	}

	freeaddrinfo(res);

	if (!rp) {
		/* Could not connect to server */
		fprintf(stderr, "%s - Could not connect to server.\n", __func__);
		socket_close(sockfd);
		sockfd = -1;
	}

	return sockfd;
}
