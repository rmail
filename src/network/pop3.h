/*
 * Copyright (C) 2011 - Roberto Branciforti
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */

#ifndef POP3_H
#define POP3_H

#include <polarssl/ssl.h>
#include <polarssl/havege.h>

#define POP3_BUFSIZE	1000

struct pop3_host {
	int  sockfd;	/* Socket descriptor */
	char *name;	/* Hostname */
	char *port;	/* Port */
};

#define POP3_AUTH_USERPASS	0
#define POP3_AUTH_SSL		1

struct pop3_auth_ssl {
	ssl_context   ctx;
	ssl_session   ssn;
	havege_state  hs;
	x509_cert     cert;
};

struct pop3_auth {
	unsigned int type;	/* Authentication type */

	char *user;		/* User name */
	char *pass;		/* Password */

	struct pop3_auth_ssl ssl;
};

struct pop3_mailbox {
	struct pop3_host  host;
	struct pop3_auth  auth;
	unsigned int      messages;  /* Number of messages in maildrop */
};

/* Connection */
int pop3_connect(struct pop3_mailbox *mbox);
int pop3_disconnect(struct pop3_mailbox *mbox);

/* Authentication */
int pop3_authenticate(struct pop3_mailbox *mbox);

/* Maildrop commands */
int pop3_stat(struct pop3_mailbox *mbox);
int pop3_list(struct pop3_mailbox *mbox);
int pop3_capa(struct pop3_mailbox *mbox);

int pop3_top(struct pop3_mailbox *mbox, unsigned int n);
int pop3_retr(struct pop3_mailbox *mbox, unsigned int n);

#endif	/* POP3_H */
