#include <stdio.h>
#include <unistd.h>

#include "pop3.h"

/* Contains default for connections */
#include "pop3_config.h"

#define CMD_TOP		0
#define CMD_RETR	1

void usage(const char *appname)
{
	fprintf(stderr, "Usage:\n%s [parameters]\n", appname);
	fprintf(stderr, "Parameters:\n");
	fprintf(stderr,	" -h <host>        Server Hostname or IP address\n"
			" -p <port>        Server Port\n"
			" -u <user>        Username\n"
			" -P <password>    Password\n"
			" -s               Use SSL connection\n"
			" -r               RETR command instead TOP\n");
}

int main(int argc, char **argv)
{
	int opt;
	int rc = -1;

	char *hostname = NULL;
	char *port = NULL;
	char *user = NULL;
	char *pass = NULL;

	unsigned int authentication_type = POP3_AUTH_USERPASS;

	/* POP3 Command used to test */
	unsigned int pop3_command = CMD_TOP;

	struct pop3_mailbox mb;
	unsigned int n;

	while ((opt = getopt(argc, argv, "u:h:p:P:sr")) != -1) {
		switch (opt) {
		case 'h':
			hostname = optarg;
			break;
		case 'p':
			port = optarg;
			break;
		case 'P':
			pass = optarg;
			break;
		case 'r':
			pop3_command = CMD_RETR;
			break;
		case 's':
			authentication_type = POP3_AUTH_SSL;
			break;
		case 'u':
			user = optarg;
			break;
		default:
			fprintf(stderr, "Unknown parameter.\n");
			usage(argv[0]);
			return -1;
                }
        }

	if (!hostname) {
		if (authentication_type != POP3_AUTH_SSL)
			hostname = POP3_DEFAULT_HOST;
		else
			hostname = POP3_SSL_DEFAULT_HOST;
	}

	if (!port) {
		if (authentication_type != POP3_AUTH_SSL)
			port = POP3_DEFAULT_PORT;
		else
			port = POP3_SSL_DEFAULT_PORT;
	}

	if (!user) {
		if (authentication_type != POP3_AUTH_SSL)
			user = POP3_DEFAULT_USER;
		else
			user = POP3_SSL_DEFAULT_USER;
	}

	if (!pass) {
		if (authentication_type != POP3_AUTH_SSL)
			pass = POP3_SSL_DEFAULT_PASSWORD;
		else
			pass = POP3_DEFAULT_PASSWORD;
	}

	mb.host.name = hostname;
	mb.host.port = port;

	mb.auth.type = authentication_type;

	mb.auth.user = user;
	mb.auth.pass = pass;

	fprintf(stderr, "Connecting %s to %s:%s...\n", user, hostname, port);

	if (pop3_connect(&mb) < 0) {
		fprintf(stderr, "Connect error\n");
		return -1;
	}

	if (pop3_authenticate(&mb) < 0) {
		goto app_out;
	}

	/* Retrieve the server capabilities */
#if 10
	if (pop3_capa(&mb) < 0) {
		goto app_out;
	}
#endif

	/* Retrieve the number of messages present on the mailbox */
	if (pop3_stat(&mb) < 0) {
		goto app_out;
	}

	fprintf(stderr, "%u Message in maildrop\n", mb.messages);

	/* Retrieve the message list */
	if (mb.messages > 0) {
		rc = pop3_list(&mb);
		if (rc < 0)
			goto app_out;

		for (n = 1; n <= mb.messages; n++) {
			if (CMD_RETR == pop3_command) {
				rc = pop3_retr(&mb, n);
			} else {
				rc = pop3_top(&mb, n);
			}
			if (rc < 0) {
				fprintf(stderr, "%s - ERROR n=%u\n", __func__, n);
				break;
			}
		}
	}

app_out:
	pop3_disconnect(&mb);
	return rc;
}
