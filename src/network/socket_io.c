/**
 * Copyright (C) 2011 - Roberto Branciforti
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

/* socket_io.c - socket I/O utilities */

#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>

#include <sys/types.h>

ssize_t socket_read(int sockfd, char *buf, size_t count)
{
	ssize_t n;

	n = read(sockfd, buf, count);
	if (n >= 0)
		buf[n] = '\0';

	return n;
}

ssize_t socket_write(int sockfd, const char *buf, size_t n)
{
	return write(sockfd, buf, n);
}

/* Read a maximum of n-1 characters from sd until a newline or
 * a '\0' is received. buf stores the input WITHOUT a terminating newline.
 * Returns number of read characters, without terminating \0.
 * HINT: pass the SIZE of the array as parameter for n.
 */
ssize_t socket_read_line(int sockfd, char *buf, size_t count, const long timeout)
{
	fd_set rfds;
	struct timeval tv;
	char *p;
	int rc;
	ssize_t n;

	p = buf;
	n = count;

	while (n > 0) {
		FD_ZERO(&rfds);
		FD_SET(sockfd, &rfds);

		tv.tv_sec = timeout;
		tv.tv_usec = 0;

		rc = select(sockfd + 1, &rfds, NULL, NULL, &tv);
		if (rc > 0) {
			if (read(sockfd, p, 1) == 1) {
				n--;
				if (*p == '\n') {
					break;
				}
			}
		} else if (rc == 0) {
			/* Time out */
			fprintf(stderr, "%s - SELECT: timeout\n", __func__);
			break;
		} else {
			/* Error */
			fprintf(stderr, "%s - SELECT: %s\n", __func__, strerror(errno));
			return -1;
		}

		p++;
	}

	*p = '\0';

	return count - n;
}
