/**
 * Copyright (C) 2011 - Roberto Branciforti <r.branciforti@seh.it>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

/* socket_io.h - socket I/O utilities */

#ifndef SOCKET_IO_H
#define SOCKET_IO_H

ssize_t socket_read(int sockfd, char *buf, size_t n);
ssize_t socket_write(int sockfd, const char *buf, size_t n);

ssize_t socket_read_line(int sockfd, char *buf, size_t count, const long timeout);

#endif	/* SOCKET_IO_H */
