/**
 * Copyright (C) 2011 - Roberto Branciforti
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

/* POP3 functions (See RFC 1939) */

#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include "socket.h"
#include "socket_io.h"
#include "socket_ssl.h"

#include "pop3.h"

#if 0
#define POP3_DEBUG
#define DEBUG_ERROR	fprintf(stderr, "%s:%d - %s\n", __FILE__, __LINE__, __func__)
#endif

#define POP3_TIMEOUT	3  /* seconds */

static void print_server_response(const char *buf)
{
	size_t i;
	for (i = 0; i < strlen(buf); i++) {
		if (buf[i] == '\r')
			fprintf(stderr, "\\r");
		else if (buf[i] == '\n')
			fprintf(stderr, "\\n");
		else if (buf[i] == '\t')
			fprintf(stderr, "\\t");
		else if ((buf[i] < ' ') || (buf[i] > 0x7E))
			fprintf(stderr, "0x%02X ", buf[i]);
		else
			fprintf(stderr, "%c", buf[i]);
	}
	fprintf(stderr, "\n");
}

/**
 * Test for an OK response
 *
 * @param[in] buf	server response line buffer
 *
 * @retval 1 +OK, 0 on -ERR response
 */
static int pop3_command_success(const char *buf)
{
#ifdef POP3_DEBUG
	print_server_response(buf);
#endif
	if (strstr(buf, "+OK") != NULL)
		return 1;

	return 0;
}

static int pop3_send_command(struct pop3_mailbox *mbox, const char *cmd, size_t cmd_size)
{
	ssize_t rc;

	if (POP3_AUTH_SSL == mbox->auth.type) {
		rc = socket_ssl_write(&mbox->auth.ssl.ctx, cmd, cmd_size);
	} else {
		rc = socket_write(mbox->host.sockfd, cmd, cmd_size);
	}

	if (rc < 0)
	        return -1;

	/* Now wr >= 0 and it's safe to cast it to size_t */
	if (cmd_size != (size_t) rc)
	        return -1;

	return 0;
}

static int pop3_get_reply(struct pop3_mailbox *mbox, char *line, size_t lsize)
{
	if (POP3_AUTH_SSL == mbox->auth.type) {
		return socket_ssl_read_line(&mbox->auth.ssl.ctx, line, lsize);
	}

	return socket_read_line(mbox->host.sockfd, line, lsize, POP3_TIMEOUT);
}

static int pop3_command(struct pop3_mailbox *mbox, const char *cmd, size_t csize)
{
	char line[POP3_BUFSIZE];

	if (pop3_send_command(mbox, cmd, csize) < 0) {
		fprintf(stderr, "%s - line %d\n", __func__, __LINE__);
		return -1;
	}

	if (pop3_get_reply(mbox, line, POP3_BUFSIZE) < 0) {
		fprintf(stderr, "%s - line %d\n", __func__, __LINE__);
		return -1;
	}

	if (strstr(line, "+OK") == NULL) {
		fprintf(stderr, "%s - line %d\n", __func__, __LINE__);
		return -1;
	}

	return 0;
}

/**
 * Test the end of pop3
 *
 * @param[in] buf	server response line buffer
 *
 * @retval 1 if EOS, 0 otherwise
 */
static int pop3_end_scan(const char *buf)
{
	if ((buf[0] == '.') && ((buf[1] == '\r') || (buf[1] == '\n')))
		return 1;

	return 0;
}

int pop3_connect(struct pop3_mailbox *mbox)
{
	struct pop3_auth_ssl *ssl;

	int sockfd;
	int rc;

	char buf[POP3_BUFSIZE];

	sockfd = connect_to_tcp_server(mbox->host.name, mbox->host.port);
	if (sockfd < 0) {
		return -1;
	}

	mbox->host.sockfd = sockfd;

#ifdef POP3_DEBUG
	fprintf(stderr, "%s sockfd %d\n", __func__, sockfd);
#endif

	ssl = &mbox->auth.ssl;

	if (POP3_AUTH_SSL == mbox->auth.type) {
		if (socket_ssl_init(&mbox->host.sockfd, &ssl->ctx, &ssl->ssn, &ssl->hs, &ssl->cert, NULL)) {
			return -1;
		}

		rc = socket_ssl_read_line(&mbox->auth.ssl.ctx, buf, POP3_BUFSIZE);
	} else {
		/* !SSL */
		rc = socket_read_line(sockfd, buf, sizeof(buf), POP3_TIMEOUT);
	}

	/* Once the TCP connection has been opened by a POP3 client,
	 * the POP3 server issues a one line greeting.
	 */
#ifdef POP3_DEBUG
	print_server_response(buf);
#endif
	if (rc < 0) {
		return -1;
	}

	if (!pop3_command_success(buf)) {
		return -1;
	}

	return 0;
}

int pop3_disconnect(struct pop3_mailbox *mbox)
{
	(void) pop3_command(mbox, "QUIT\r\n", 6);

	if (POP3_AUTH_SSL == mbox->auth.type) {
		return socket_ssl_close(mbox->host.sockfd,
					&mbox->auth.ssl.ctx,
					&mbox->auth.ssl.cert);
	}

	return socket_close(mbox->host.sockfd);
}

int pop3_authenticate(struct pop3_mailbox *mbox)
{
	int n;
	char line[POP3_BUFSIZE];

	if ((mbox->auth.type != POP3_AUTH_USERPASS) &&
	    (mbox->auth.type != POP3_AUTH_SSL)) {

		fprintf(stderr, "%s - Invalid POP3_AUTH_TYPE %d\n",
				__func__, mbox->auth.type);
		return -1;
	}

	n = snprintf(line, POP3_BUFSIZE, "USER %s\r\n", mbox->auth.user);
	if (pop3_command(mbox, line, n)) {
		fprintf(stderr, "%s - Invalid user\n", __func__);
		return -1;
	}

	n = snprintf(line, POP3_BUFSIZE, "PASS %s\r\n", mbox->auth.pass);
	if (pop3_command(mbox, line, n)) {
		fprintf(stderr, "%s - Invalid password\n", __func__);
		return -1;
	}

	return 0;
}

int pop3_capa(struct pop3_mailbox *mbox)
{
	char line[POP3_BUFSIZE];
	int scan_end;

	if (pop3_command(mbox, "CAPA\r\n", 6)) {
	        return -1;
	}

	scan_end = 0;
	while (!scan_end) {
		if (pop3_get_reply(mbox, line, POP3_BUFSIZE) < 0) {
			fprintf(stderr, "%s - line %d\n", __func__, __LINE__);
			return -1;
		} else {
			fprintf(stderr, "%s - line %s\n", __func__, line);
		}

		scan_end = pop3_end_scan(line);

		/* Prints the scan-list entry */
		print_server_response(line);
	}

	return 0;
}

int pop3_stat(struct pop3_mailbox *mbox)
{
	int n;
	unsigned int msgs;
	unsigned long octets = 0;

	char line[POP3_BUFSIZE];

	if (pop3_send_command(mbox, "STAT\r\n", 6)) {
		fprintf(stderr, "%s - line %d\n", __func__, __LINE__);
	        return -1;
	}

	if (pop3_get_reply(mbox, line, POP3_BUFSIZE) < 0) {
		fprintf(stderr, "%s - line %d\n", __func__, __LINE__);
		return -1;
	}

	/* Reset message counter */
	mbox->messages = 0;
	/* [RFC 1939]...
	 * The positive response consists of "+OK" followed by a single space,
	 * the number of messages in the maildrop, a single space, and the
	 * size of the maildrop in octets.
	 */
	n = sscanf(line, "+OK %u %lu", &msgs, &octets);
	if (n >= 0) {
		mbox->messages = msgs;
	} else {
		fprintf(stderr, "%s - Error parsing %s\n", __func__, line);
		return -1;
	}

	return 0;
}

int pop3_list(struct pop3_mailbox *mbox)
{
	char line[POP3_BUFSIZE];
	int scan_end;

	if (pop3_command(mbox, "LIST\r\n", 6)) {
	        return -1;
	}

	scan_end = 0;
	while (!scan_end) {
		if (pop3_get_reply(mbox, line, POP3_BUFSIZE) < 0) {
			return -1;
		}

		scan_end = pop3_end_scan(line);

		/* Prints the scan-list entry */
		print_server_response(line);
	}

	return 0;
}

int pop3_top(struct pop3_mailbox *mbox, unsigned int msg_num)
{
	char snd[POP3_BUFSIZE];
	char buf[POP3_BUFSIZE];

	int scan_end;
	int n;

	if (msg_num > mbox->messages) {
		fprintf(stderr, "%s - BUG: msg_num %u > mbox.messages %u",
			 __func__, msg_num, mbox->messages);
		return -1;
	}

	n = snprintf(snd, POP3_BUFSIZE, "TOP %u 0\r\n", msg_num);

	if (pop3_send_command(mbox, snd, n)) {
	        return -1;
	}

	scan_end = 0;
	while (!scan_end) {
		if (socket_read_line(mbox->host.sockfd, buf, POP3_BUFSIZE, POP3_TIMEOUT) < 0)
			return -1;

		scan_end = pop3_end_scan(buf);

		/* Prints the scan-list entry */
		print_server_response(buf);
	}
	return 0;
}

int pop3_retr(struct pop3_mailbox *mbox, unsigned int msg_num)
{
	char snd[POP3_BUFSIZE];
	char buf[POP3_BUFSIZE];
	int scan_end;
	int n;

	if (msg_num > mbox->messages) {
		fprintf(stderr, "%s - BUG: msg_num %u > mbox.messages %u",
			 __func__, msg_num, mbox->messages);
		return -1;
	}

	n = snprintf(snd, POP3_BUFSIZE, "RETR %u\r\n", msg_num);

	if (pop3_send_command(mbox, snd, n)) {
	        return -1;
	}

	scan_end = 0;
	while (!scan_end) {
		if (socket_read_line(mbox->host.sockfd, buf, POP3_BUFSIZE, POP3_TIMEOUT) < 0)
			return -1;

		scan_end = pop3_end_scan(buf);

		/* Prints the scan-list entry */
		print_server_response(buf);
	}
	return 0;
}
