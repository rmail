#include <stdio.h>
#include <unistd.h>

#include "socket.h"
#include "socket_io.h"

void usage(const char *appname)
{
	fprintf(stderr, "Usage:\n%s -h <host> -p <port>\n", appname);
}

int main(int argc, char **argv)
{
	int opt;
	int rc;

	int sockfd;

	char *hostname = NULL;
	char *port = NULL;

	while ((opt = getopt(argc, argv, "h:p:")) != -1) {
		switch (opt) {
		case 'h' :
			hostname = optarg;
			break;
		case 'p':
			port = optarg;
			break;
		default:
			fprintf(stderr, "Unknown parameter.\n");
			usage(argv[0]);
			return -1;
                }
        }

	if (!hostname || !port) {
                usage(argv[0]);
		return -1;
	}

	fprintf(stderr, "Connecting to %s port %s...\n", hostname, port);
	sockfd = connect_to_tcp_server(hostname, port);
	if (sockfd < 0)
		return -1;

	fprintf(stderr, "Connected!\n");

	rc = socket_close(sockfd);
	return rc;
}
