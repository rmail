/* socket_ssl.h - SSL socket utilities */

#include <polarssl/ssl.h>
#include <polarssl/havege.h>

#ifndef SOCKET_SSL_H
#define SOCKET_SSL_H

ssize_t socket_ssl_read(ssl_context *ssl, char *buf, size_t len);
ssize_t socket_ssl_read_line(ssl_context *ssl, char *buf, size_t count);

ssize_t socket_ssl_write(ssl_context *ssl, const char *buf, size_t len);

int socket_ssl_init(int *sockfd, ssl_context *ssl, ssl_session *ssn,
		    havege_state *hs, x509_cert *cert, char *certfile);

int socket_ssl_close(int sockfd, ssl_context *ssl, x509_cert *cert);

#endif	/* SOCKET_SSL_H */
