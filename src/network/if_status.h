#ifndef IF_STATUS_H
#define IF_STATUS_H

#define	IFSTATUS_DOWN	 0
#define	IFSTATUS_UP	 1
#define	IFSTATUS_ERR	-1

extern int net_monitor_interface(const char *if_name, const int wlan);

#endif	/* IF_STATUS_H */
