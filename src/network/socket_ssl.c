#include <unistd.h>

/*-- for read_line */
#include <string.h>
#include <errno.h>
#include <sys/types.h>

#ifdef SOCKET_SSL_DEBUG
#define POLARSSL_DEBUG_MSG
#endif

#include <polarssl/net.h>
#include <polarssl/ssl.h>
#include <polarssl/havege.h>

#if 0 /* We are using the net_send and net_recv defined in "polarssl/net.h" */
/* SSL BIO write callbacks */
static int ps_send(void *ctx, unsigned char *buf, int len)
{
	return write(*(int *) ctx, buf, len);
}

/* SSL BIO read callbacks */
static int ps_recv(void *ctx, unsigned char *buf, int len)
{
	return read(*(int *) ctx, buf, len);
}
#endif

ssize_t socket_ssl_read(ssl_context *ssl, char *buf, size_t len)
{
	int rc;

	rc = ssl_read(ssl, (unsigned char *) buf, len);

#ifdef SOCKET_SSL_DEBUG
	if (rc < 0)
		fprintf(stderr, "%s - ERROR rc=%d\n", __func__, rc);
#endif

	return rc;
}

ssize_t socket_ssl_write(ssl_context *ssl, const char *buf, size_t len)
{
	int rc;

#ifdef SOCKET_SSL_DEBUG
	fprintf(stderr, "%s - buf=%s\n", __func__, buf);
#endif

	do {
		rc = ssl_write(ssl, (const unsigned char *) buf, len);
		if ((rc >= 0) /*&& (rc == len)*/) {
			break;
		}

		if (rc == POLARSSL_ERR_SSL_PEER_CLOSE_NOTIFY ||
		    rc == POLARSSL_ERR_NET_CONN_RESET) {
			rc = 0;
			break;
		}

		if (rc < 0 && rc != POLARSSL_ERR_NET_TRY_AGAIN) {
			fprintf(stderr, "%s - ERROR rc=%d\n", __func__, rc);
			break;
		}
	} while (rc >= 0);

	return rc;
}

int socket_ssl_init(int *sockfd, ssl_context *ssl, ssl_session *ssn,
		    havege_state *hs, x509_cert *cert, char *certfile)
{
	havege_init(hs);

	/* Initialize the SSL context */
	if (ssl_init(ssl))
		return -1;

	ssl_set_endpoint(ssl, SSL_IS_CLIENT);

	memset(cert, 0, sizeof(x509_cert));

	if (certfile) {
		x509parse_crtfile(cert, certfile);
		ssl_set_ca_chain(ssl, cert, NULL, NULL);
		ssl_set_authmode(ssl, SSL_VERIFY_REQUIRED);
	} else {
		ssl_set_authmode(ssl, SSL_VERIFY_NONE);
	}

	ssl_set_rng(ssl, havege_rand, hs);

	/* Set SSL BIO callbacks */
	ssl_set_bio(ssl, net_recv, sockfd, net_send, sockfd);

	ssl_set_ciphers(ssl, ssl_default_ciphers);
	ssl_set_session(ssl, 1, 600, ssn);

	return ssl_handshake(ssl);
}

int socket_ssl_close(int sockfd, ssl_context *ssl, x509_cert *cert)
{
	/* Notify the peer that the connection is being closed */
	ssl_close_notify(ssl);

	/* Unallocate all certificate data */
	x509_free(cert);

	/* Free the SSL context */
	ssl_free(ssl);

	return close(sockfd);
}

static char *check_end_of_line(char *p, const size_t n)
{
	char *nl;

	/* Check for CR first, if not found check the NL */
	nl = memchr(p, '\r', n);
	if (nl == NULL) {
		nl = memchr(p, '\n', n);
	}

	/* If present, remove the EOL character */
	if (nl != NULL) {
#ifdef SOCKET_SSL_DEBUG
		fprintf(stderr, "%s - EOL\n", __func__);
#endif
		*nl = '\0';
	}

	return nl;
}

ssize_t socket_ssl_read_line(ssl_context *ssl, char *buf, size_t count)
{
	char *p;
	ssize_t rc;
	size_t n;

	p = buf;
	n = 0;

	while (n < count) {
		rc = socket_ssl_read(ssl, p, count);
		if (rc > 0) {
			n += rc;
			if (check_end_of_line(p, rc) != NULL) {
				break;
			}
		} else if (rc == 0) {
#ifdef SOCKET_SSL_DEBUG
			fprintf(stderr, "%s - Read None!\n", __func__);
#endif
			break;
		} else {
			/* rc < 0 Error */
			fprintf(stderr, "%s - ERROR\n", __func__);
			return -1;
		}
		p += rc;
	}

#ifdef SOCKET_SSL_DEBUG
	fprintf(stderr, "%s - SSL-READ: %s n:%d\n", __func__, buf, n);
#endif
	return n;
}
