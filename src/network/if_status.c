#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <errno.h>
#include <ctype.h>

#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/socket.h>

#include <netinet/in.h>

#include <linux/sockios.h>
#include <linux/if_ether.h>

#include "if_status.h"

/*#define USE_ETHTOOL_INTERFACE*/

#ifdef USE_ETHTOOL_INTERFACE
#include <linux/ethtool.h>
#endif

#define USE_WIRELESS_INTERFACE

#ifdef USE_WIRELESS_INTERFACE
#include <linux/wireless.h>
#endif

/*#define USE_MII_INTERFACE*/
/*#define USE_PRIV_INTERFACE*/

/*#define DEBUG_IF_STATUS */
/*#define DEBUG_IF_STATUS_ERRORS */

#ifdef DEBUG_IF_STATUS_C
#define PRINT_DEBUG(if_name, msg, err)	\
	do {					\
		printf("%s -- (%s) %s: %s\n", __func__, (if_name), (msg), (err)); \
	} while (0);
#else
#define PRINT_DEBUG(if_name, msg, err)
#endif

#ifdef DEBUG_IF_STATUS
static void net_debug_status(const char *if_name, const int status)
{
	switch (status) {
	case IFSTATUS_UP:
		printf("%s: Link beat detected\n", if_name);
		break;
	case IFSTATUS_DOWN:
		printf("%s: Unplugged\n", if_name);
		break;
	default:
		printf("%s: Not supported%s\n", if_name, getuid() != 0 ? " (Retry as root?)" : "");
		break;
	}
}
#else
#define net_debug_status(if_name, status)
#endif

#if 0
static void interface_up(int fd, const char *iface)
{
	struct ifreq ifr;

	memset(&ifr, 0, sizeof(ifr));
	strncpy(ifr.ifr_name, iface, sizeof(ifr.ifr_name)-1);

	if (ioctl(fd, SIOCGIFFLAGS, &ifr) < 0) {
		PRINT_DEBUG(iface, "Could not get interface flags.", "\n");
		return;
	}

	if ((ifr.ifr_flags & IFF_UP) == IFF_UP)
		return;

	if (ioctl(fd, SIOCGIFADDR, &ifr) < 0) {
		/* Could not get interface address */
		PRINT_DEBUG(iface, "SIOCGIFADDR", strerror(errno));
	} else if (ifr.ifr_addr.sa_family != AF_INET) {
		PRINT_DEBUG(iface, "The interface is not IP-based.", "\n");
	} else {
		((struct sockaddr_in *)(&ifr.ifr_addr))->sin_addr.s_addr = INADDR_ANY;
		if (ioctl(fd, SIOCSIFADDR, &ifr) < 0) {
			/* Could not set interface address */
			PRINT_DEBUG(iface, "SIOCSIFADDR", strerror(errno));
		}
	}

	if (ioctl(fd, SIOCGIFFLAGS, &ifr) < 0) {
		/* Could not get interface flags. */
		PRINT_DEBUG(iface, "SIOCGIFFLAGS", strerror(errno));
		return;
	}

	ifr.ifr_flags |= IFF_UP;

	if (ioctl(fd, SIOCSIFFLAGS, &ifr) < 0) {
		/* Could not set interface flags */
		PRINT_DEBUG(iface, "SIOCSIFFLAGS", strerror(errno));
	}
}
#endif

/*
 *======================================================================
 * CABLE
 *======================================================================
 */
#ifdef USE_MII_INTERFACE
static int interface_detect_beat_mii(int fd, const char *iface)
{
	struct ifreq ifr;

	memset(&ifr, 0, sizeof(ifr));
	strncpy(ifr.ifr_name, iface, sizeof(ifr.ifr_name) - 1);

	if (ioctl(fd, SIOCGMIIPHY, &ifr) == -1) {
		PRINT_DEBUG(iface, "SIOCGMIIPHY", strerror(errno));
		return IFSTATUS_ERR;
	}

	((unsigned short *) &ifr.ifr_data)[1] = 1;

	if (ioctl(fd, SIOCGMIIREG, &ifr) == -1) {
		PRINT_DEBUG(iface, "SIOCGMIIREG", strerror(errno));
		return IFSTATUS_ERR;
	}

	return (((unsigned short*) &ifr.ifr_data)[3] & 0x0004) ? IFSTATUS_UP : IFSTATUS_DOWN;
}
#endif

#ifdef USE_PRIV_INTERFACE
static int interface_detect_beat_priv(int fd, const char *iface)
{
	struct ifreq ifr;

	memset(&ifr, 0, sizeof(ifr));
	strncpy(ifr.ifr_name, iface, sizeof(ifr.ifr_name) - 1);

	if (ioctl(fd, SIOCDEVPRIVATE, &ifr) == -1) {
		PRINT_DEBUG(iface, "SIOCDEVPRIVATE", strerror(errno));
		return IFSTATUS_ERR;
	}

	((unsigned short*) &ifr.ifr_data)[1] = 1;

	if (ioctl(fd, SIOCDEVPRIVATE+1, &ifr) == -1) {
		PRINT_DEBUG(iface, "SIOCDEVPRIVATE+1", strerror(errno));
		return IFSTATUS_ERR;
	}

	return (((unsigned short*) &ifr.ifr_data)[3] & 0x0004) ? IFSTATUS_UP : IFSTATUS_DOWN;
}
#endif

#ifdef USE_ETHTOOL_INTERFACE
static int interface_detect_beat_ethtool(int fd, const char *iface)
{
	struct ifreq ifr;
	struct ethtool_value edata;

	memset(&ifr, 0, sizeof(ifr));
	strncpy(ifr.ifr_name, iface, sizeof(ifr.ifr_name)-1);

	edata.cmd = ETHTOOL_GLINK;
	ifr.ifr_data = (caddr_t) &edata;

	if (ioctl(fd, SIOCETHTOOL, &ifr) == -1) {
		PRINT_DEBUG(iface, "ETHTOOL_GLINK", strerror(errno));
		return IFSTATUS_ERR;
	}

	return (edata.data) ? IFSTATUS_UP : IFSTATUS_DOWN;
}
#endif

static int interface_detect_beat_iff(int fd, const char *iface)
{
	struct ifreq ifr;

	memset(&ifr, 0, sizeof(ifr));
	strncpy(ifr.ifr_name, iface, sizeof(ifr.ifr_name)-1);

	if (ioctl(fd, SIOCGIFFLAGS, &ifr) == -1) {
		PRINT_DEBUG(iface, "SIOCGIFFLAGS", strerror(errno));
		return IFSTATUS_ERR;
	}

	return (ifr.ifr_flags & IFF_RUNNING) ? IFSTATUS_UP : IFSTATUS_DOWN;
}

/*
 *======================================================================
 * WLAN
 *======================================================================
 */
#ifdef USE_WIRELESS_INTERFACE
static int get_wlan_qual_old(const char *iface)
{
	FILE *f;
	char buf[256];
	char *bp;
	int l, q = -1;

	l = strlen(iface);

	f = fopen("/proc/net/wireless", "r");
	if (f == NULL) {
		PRINT_DEBUG(iface, "Open /proc/net/wireless", strerror(errno));
		return -1;
	}

	while (fgets(buf, sizeof(buf)-1, f)) {
		bp = buf;

		while (*bp && isspace(*bp))
			bp++;

		if (!strncmp(bp, iface, l) && bp[l]==':') {
			/* skip device name */
			if (!(bp = strchr(bp, ' ')))
				break;
			bp++;

			/* skip status */
			if (!(bp = strchr(bp, ' ')))
				break;

			q = atoi(bp);
			break;
		}
	}

	fclose(f);

	if (q < 0) {
		PRINT_DEBUG(iface, "Failed to find interface in /proc/net/wireless", "\n");
	}

	return q;
}

static int get_wlan_qual_new(int fd, const char *iface)
{
	struct iwreq req;
	struct iw_statistics q;
	static struct iw_range range;

	memset(&req, 0, sizeof(req));
	strncpy(req.ifr_ifrn.ifrn_name, iface, IFNAMSIZ);

	req.u.data.pointer = (caddr_t) &q;
	req.u.data.length = sizeof(q);
	req.u.data.flags = 1;

	/* Get interface quality */
	if (ioctl(fd, SIOCGIWSTATS, &req) < 0) {
		PRINT_DEBUG(iface, "SIOCGIWSTATS", strerror(errno));
		return -1;
	}

	memset(&req, 0, sizeof(req));
	strncpy(req.ifr_ifrn.ifrn_name, iface, IFNAMSIZ);

	memset(&range, 0, sizeof(struct iw_range));
	req.u.data.pointer = (caddr_t) &range;
	req.u.data.length = sizeof(struct iw_range);
	req.u.data.flags = 0;

	if (ioctl(fd, SIOCGIWRANGE, &req) < 0) {
		PRINT_DEBUG(iface, "SIOCGIWRANGE", strerror(errno));
		return -1;
	}

	/* Test if both qual and level are on their lowest level */
	if (q.qual.qual <= 0 &&
	   (q.qual.level > range.max_qual.level ? q.qual.level <= 156 : q.qual.level <= 0))
		return 0;

	return 1;
}

static int is_assoc_ap(uint8_t mac[ETH_ALEN])
{
	int b, j;
	b = 1;

	for (j = 1; j < ETH_ALEN; j++)
		if (mac[j] != mac[0]) {
			b = 0;
			break;
		}

	return !b || (mac[0] != 0xFF && mac[0] != 0x44 && mac[0] != 0x00);
}

static int interface_detect_beat_wlan(int fd, const char *iface)
{
	uint8_t mac[6];
	int q;
	struct iwreq req;

	memset(&req, 0, sizeof(req));
	strncpy(req.ifr_ifrn.ifrn_name, iface, IFNAMSIZ);

	if (ioctl(fd, SIOCGIWAP, &req) < 0) {
		PRINT_DEBUG(iface, "Get AP address", strerror(errno));
		return IFSTATUS_ERR;
	}

	memcpy(mac, &(req.u.ap_addr.sa_data), ETH_ALEN);

	if (!is_assoc_ap(mac))
		return IFSTATUS_DOWN;

	/* Use new method for get the signal quality */
	q = get_wlan_qual_new(fd, iface);
	if (q < 0) {
		/* Fallback to the old  method for get the signal quality */
		q = get_wlan_qual_old(iface);
		if (q < 0) {
			PRINT_DEBUG(iface, "Failed to get wireless link quality.", "\n");
			return IFSTATUS_ERR;
		}
	}

	return q > 0 ? IFSTATUS_UP : IFSTATUS_DOWN;
}
#endif

/*
 *======================================================================
 * PUBLIC FUNCTIONS
 *======================================================================
 */
int net_monitor_interface(const char *if_name, const int wlan)
{
	int rc;
	int fd;

	if (!if_name) {
		return IFSTATUS_ERR;
	}

	fd = socket(PF_INET, SOCK_DGRAM, 0);
	if (fd < 0) {
		return IFSTATUS_ERR;
	}

#if 0
	if (interface_auto_up)
		interface_up(fd, iface);
#endif

#ifdef USE_ETHTOOL_INTERFACE
	rc = interface_detect_beat_ethtool(fd, if_name);
	if (rc != IFSTATUS_ERR) {
		net_debug_status(if_name, rc);
		return rc;
	}
#endif

#ifdef USE_MII_INTERFACE
	rc = interface_detect_beat_mii(fd, if_name);
	if (rc != IFSTATUS_ERR) {
		net_debug_status(if_name, rc);
		return rc;
	}
#endif

#ifdef USE_PRIV_INTERFACE
	rc = interface_detect_beat_priv(fd, if_name);
	if (rc != IFSTATUS_ERR) {
		net_debug_status(if_name, rc);
		return rc;
	}
#endif

#ifdef USE_WIRELESS_INTERFACE
	if (wlan) {
		rc = interface_detect_beat_wlan(fd, if_name);
		if (rc != IFSTATUS_ERR) {
			net_debug_status(if_name, rc);
			return rc;
		}
	}
#endif

	rc = interface_detect_beat_iff(fd, if_name);

	close(fd);

	net_debug_status(if_name, rc);

	return rc;
}
