#ifndef UUID_UTILS_H
#define UUID_UTILS_H

void make_unique_filename(char *buf);
char *make_path(const char *base, const char *name);
void free_path(char *path);

#endif
