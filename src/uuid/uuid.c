#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <uuid/uuid.h>

void make_unique_filename(char *buf)
{
        uuid_t unique;

        uuid_generate(unique);
        uuid_unparse(unique, buf);
/*
	fprintf(stderr, "%s - buf: %s\n", __func__, buf);
*/
}


char *make_path(const char *base, const char *name)
{
	char *path = NULL;

	size_t base_len;	/* base lenght (in bytes) */
	size_t name_len;	/* name lenght (in bytes) */
	size_t path_len;	/* base lenght + name lenght */

	if ((!base) || (!name))
		return NULL;

	base_len = strlen(base);
	name_len = strlen(name);

	path_len = base_len + name_len + 2;
	if (path_len < 3) {
		return NULL;
	}

	path = malloc(path_len);
	if (!path) {
		return NULL;
	}

	fprintf(stderr, "%s - base: %s (%u) name:%s (%u)\n", __func__, base, base_len, name, name_len);

	memset(path, 0, path_len);

	strncpy(path, base, base_len);
	strncat(path, "/", 1);
	strncat(path, name, name_len);

	return path;
}

void free_path(char *path)
{
	if (path) {
		free(path);
		path = NULL;
	}
}

