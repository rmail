#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>

/* For PATH_MAX and _POSIX_PATH_MAX */
#include <limits.h>

#include <sys/time.h>

int maildir_make_unique(char *uuid, size_t uuid_len)
{
	char hostname[64];
	struct timeval tv;
	pid_t my_pid;

	/* 2nd method gettimeofday() */
	if (gettimeofday(&tv, NULL) < 0) {
		fprintf(stderr, "gettimeofday ERORR: %s\n", strerror(errno));
		return -1;
	}

	if (gethostname(hostname, sizeof(hostname)) < 0) {
		fprintf(stderr, "gethostname ERORR: %s\n", strerror(errno));
		return -1;
	}

	my_pid = getpid();

	snprintf(uuid, uuid_len, "%lu.%06lu.%lu.%s:2,", tv.tv_sec, my_pid, tv.tv_usec, hostname);


	return 0;
}

#if 0
static char *
mkname(time_t t, enum mflag f, const char *pref)
{
        static unsigned long    count;
        static pid_t    mypid;
        char    *cp;
        static char     *node;
        int     size, n, i;

        if (pref == NULL) {
                if (mypid == 0)
                        mypid = getpid();
                if (node == NULL) {
                        cp = nodename(0);
                        n = size = 0;
                        do {
                                if (n < size + 8)
                                        node = srealloc(node, size += 20);
                                switch (*cp) {
                                case '/':
                                        node[n++] = '\\', node[n++] = '0',
                                        node[n++] = '5', node[n++] = '7';
                                        break;
                                case ':':
                                        node[n++] = '\\', node[n++] = '0',
                                        node[n++] = '7', node[n++] = '2';
                                        break;
                                default:
                                        node[n++] = *cp;
                                }
                        } while (*cp++);
                }
                size = 60 + strlen(node);
                cp = salloc(size);
                n = snprintf(cp, size, "%lu.%06lu_%06lu.%s:2,",
                                (unsigned long)t,
                                (unsigned long)mypid, ++count, node);
        } else {
                size = (n = strlen(pref)) + 13;
                cp = salloc(size);
                strcpy(cp, pref);
                for (i = n; i > 3; i--)
                        if (cp[i-1] == ',' && cp[i-2] == '2' && cp[i-3] == ':') {
                                n = i;
                                break;
                        }
                if (i <= 3) {
                        strcpy(&cp[n], ":2,");
                        n += 3;
                }
        }
        if (n < size - 7) {
                if (f & MDRAFTED)
                        cp[n++] = 'D';
                if (f & MFLAGGED)
                        cp[n++] = 'F';
                if (f & MANSWERED)
                        cp[n++] = 'R';
                if (f & MREAD)
                        cp[n++] = 'S';
                if (f & MDELETED)
                        cp[n++] = 'T';
                cp[n] = '\0';
        }
        return cp;
}
#endif


int main(void)
{
	char unique[_POSIX_PATH_MAX];
	char filename[PATH_MAX];


	printf("PATH_MAX:%d _POSIX_PATH_MAX:%d\n", PATH_MAX, _POSIX_PATH_MAX);

	memset(unique, 0, sizeof(unique));

	if (maildir_make_unique(unique, sizeof(unique)) == 0)
		printf("Unique: %s\n", unique);

	return 0;
}
