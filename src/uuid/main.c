#include <stdio.h>

#include <uuid/uuid.h>


int main(void)
{
	uuid_t out;
	int i;

	unsigned char *uuid;

	uuid_generate(out);

	uuid = (unsigned char *) out;

	for (i = 0; i < 16; i++)
		printf("%02x", out[i]);

	printf("\n");
	return 0;
}

