#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/select.h>
#include <sys/inotify.h>

/*
Here are some other tips for inotify:

- If a file or directory under observation is deleted, its watches are removed
  automatically (after a delete event is delivered, if appropriate).

- If you're monitoring a file or directory on a file system that is unmounted,
  your watch receives an unmount event before all affected watches are deleted.

- Add the IN_ONESHOT flag to the watch mask to set a one-time alert. After the
  alert is sent once, it is deleted.

- To modify an event, provide the same pathname but a different mask.
  The new watch replaces the old one.

- For all practical purposes, you're unlikely to run out of watches in any given
  inotify instance. However, you can run out of space in your event queue,
  depending on how often you process events. A queue overflow causes the
  IN_Q_OVERFLOW event.

- The close() method destroys the inotify instance and all associated watches
  and empties all pending events in the queue.

-----------------------------------------------------------------------------
Valid Events
IN_ACCESS         File was read from.
IN_MODIFY         File was written to.
IN_ATTRIB         File's metadata (inode or xattr) was changed.
IN_CLOSE_WRITE    File was closed (and was open for writing).
IN_CLOSE_NOWRITE  File was closed (and was not open for writing).
IN_OPEN           File was opened.
IN_MOVED_FROM     File was moved away from watch.
IN_MOVED_TO       File was moved to watch.
IN_DELETE         File was deleted.
IN_DELETE_SELF    The watch itself was deleted.

Helper Events
-----------------------------------------------------------------------------
IN_CLOSE          IN_CLOSE_WRITE | IN_CLOSE_NOWRITE
IN_MOVE           IN_MOVED_FROM | IN_MOVED_TO
IN_ALL_EVENTS     Bitwise OR of all events.
-----------------------------------------------------------------------------

Events That Cover General Changes
-----------------------------------------------------------------------------
IN_UNMOUNT	The backing filesystem was unmounted.
IN_Q_OVERFLOW	The inotify queue overflowed.
IN_IGNORED	The watch was automatically removed, because the file was
		deleted or its filesystem was unmounted.


Obtaining the Size of the Queue

The size of the pending event queue can be obtained via FIONREAD:

unsigned int queue_len;
int ret;

ret = ioctl (fd, FIONREAD, &queue_len);
if (ret < 0)
        perror ("ioctl");
else
        printf ("%u bytes pending in queue\n", queue_len);


This is useful to implement throttling: reading from the queue only when
the number of events has grown sufficiently large.

Configuring inotify

inotify is configurable via procfs and sysctl.

/proc/sys/filesystem/inotify/max_queued_events is the maximum number of events
that can be queued at once. If the queue reaches this size, new events are
dropped, but the IN_Q_OVERFLOW event is always sent. With a significantly
large queue, overflows are rare even if watching many objects.
The default value is 16,384 events per queue.

"/proc/sys/filesystem/inotify/max_user_instances"
is the maximum number of inotify instances that a given user can instantiate.
The default value is 128 instances, per user.

"/proc/sys/filesystem/inotify/max_user_watches"
is the maximum number of watches per instance.
The default value is 8,192 watches, per instance.

These knobs exist because kernel memory is a precious resource.
Although any user can read these files, only the system administrator can write to them.

*/


#define EVENT_SIZE  (sizeof(struct inotify_event))
#define BUF_LEN     (1024 * (EVENT_SIZE + 16))


int my_inotify_job(int fd)
{
	int i = 0;
	int length;
	char buffer[BUF_LEN];

	length = read(fd, buffer, BUF_LEN);

	if (length < 0) {
		perror("read");
	}

	while (i < length) {
		struct inotify_event *event = (struct inotify_event *) &buffer[i];
		if (event->len) {
			if (event->mask & IN_CREATE) {
				if (event->mask & IN_ISDIR) {
					printf("The directory %s was created.\n", event->name);
				} else {
					printf("The file %s was created.\n", event->name);
				}
			} else if (event->mask & IN_DELETE) {
				if (event->mask & IN_ISDIR) {
					printf("The directory %s was deleted.\n", event->name);
				} else {
					printf("The file %s was deleted.\n", event->name);
				}
			} else if (event->mask & IN_MODIFY) {
				if (event->mask & IN_ISDIR) {
					printf("The directory %s was modified.\n", event->name);
				} else {
					printf("The file %s was modified.\n", event->name);
				}
			}
		}
		i += EVENT_SIZE + event->len;
	}
	return 0;
}



int main(int argc, char **argv)
{
	int fd;
	int wd;
	int rc;

	fd_set fds;

	struct timeval ttw;	/* time to wait */


	/*
	 * Create an instance of the inotify subsystem in the kernel and
	 * returns a file descriptor on success and -1 on failure.
	 */
	fd = inotify_init();

	if (fd < 0) {
		perror("inotify_init");
	}

	wd = inotify_add_watch(fd, "/tmp/myinotify", IN_MODIFY | IN_CREATE | IN_DELETE);

	FD_ZERO(&fds);
	FD_SET(fd, &fds);

	ttw.tv_sec = 10;
	ttw.tv_usec = 0;

	rc = select(fd + 1, &fds, NULL, NULL, &ttw);

	if (rc < 0) {
		/* Error */
		perror("SELECT");
	} else if (rc == 0) {
		/* Timeout */
		printf("Timeout.\n");
	} else if (FD_ISSET(fd, &fds)) {
		/* Process the inotify events */
		my_inotify_job(fd);
	}

	(void) inotify_rm_watch(fd, wd);
	(void) close(fd);

	exit(0);
}
