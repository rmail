/*
 * iNotify daemon
 *
 * To use this code, search for 'TODO' and follow the directions.
 *
 * To compile this file:
 *      gcc -o [daemonname] thisfile.c
 *
 */
#define _GNU_SOURCE

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/select.h>
#include <sys/inotify.h>

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <syslog.h>
#include <string.h>
#include <assert.h>
#include <signal.h>


#define DAEMON_NAME	"inotify_daemon"
#define DAEMON_PID_FILE	"/var/run/inotify_daemon.pid"

static volatile int running = 1;

/**
 * Print the command-line options for this application.
 *
 * @param[in] argc	Standard argument count
 * @param[in] argv	Standard argument array
 */
static void usage(int argc, char **argv)
{
	if (argc >= 1) {
		fprintf(stderr, "Usage: %s [options]\n"
			"  Options:\n"
			"    -d\tfullpath to watch.\n"
			"    -n\tDon't fork off as a daemon.\n"
			"    -h\tShow this help screen.\n",
			basename(argv[0]));

	}
}

/**
 * This function handles select signals that the application may receive.
 * This function is installed as a signal handler in the 'main()' function.
 *
 * @param[in] sig	The received signal number
 */
void signal_handler(int sig)
{
	switch (sig) {
	case SIGHUP:
		syslog(LOG_INFO /*WARNING*/, "Received SIGHUP signal.");
		break;
	case SIGTERM:
		syslog(LOG_INFO /*WARNING*/, "Received SIGTERM signal.");
		running = 0;
		break;
	default:
		syslog(LOG_INFO /*WARNING*/, "Unhandled signal (%d) %s", sig, strsignal(sig));
		break;
	}
}

static void demonizing_process(void)
{
	pid_t pid;	/* Our Process ID */
	pid_t sid;	/* Our Session ID */

	syslog(LOG_INFO, "Starting the daemonizing process");

	/* Fork off the parent process */
	pid = fork();
	if (pid < 0) {
		syslog(LOG_ERR, "%s - FORK: %s", __func__, strerror(errno));
		exit(EXIT_FAILURE);
	}

	/* If we got a good PID, then we can exit the parent process. */
	if (pid > 0) {
		syslog(LOG_DEBUG, "%s - TERM parent process", __func__);
		exit(EXIT_SUCCESS);
	}

	/* Change the file mode mask */
	umask(0);

	/* Create a new SID for the child process */
	sid = setsid();
	if (sid < 0) {
		syslog(LOG_ERR, "%s - SETSID: %s", __func__, strerror(errno));
		exit(EXIT_FAILURE);
	}

	/* Change the current working directory */
	if ((chdir("/")) < 0) {
		syslog(LOG_ERR, "%s - CHDIR: %s", __func__, strerror(errno));
		exit(EXIT_FAILURE);
	}

        /* Close out the standard file descriptors */
        close(STDIN_FILENO);
        close(STDOUT_FILENO);
        close(STDERR_FILENO);
}

#define EVENT_SIZE  (sizeof(struct inotify_event))
#define BUF_LEN     (1024 * (EVENT_SIZE + 16))

static void my_inotify_job(int fd)
{
	int i = 0;
	int length;
	char buffer[BUF_LEN];

	length = read(fd, buffer, BUF_LEN);
	if (length < 0) {
		syslog(LOG_INFO, "Read error. %s", strerror(errno));
		return;
	}

	while (i < length) {
		struct inotify_event *event = (struct inotify_event *) &buffer[i];
		if (event->len) {
			if (event->mask & IN_CREATE) {
				if (event->mask & IN_ISDIR) {
					syslog(LOG_INFO, "The directory %s was created.\n", event->name);
				} else {
					syslog(LOG_INFO, "The file %s was created.\n", event->name);
				}
			} else if (event->mask & IN_DELETE) {
				if (event->mask & IN_ISDIR) {
					syslog(LOG_INFO, "The directory %s was deleted.\n", event->name);
				} else {
					syslog(LOG_INFO, "The file %s was deleted.\n", event->name);
				}
			} else if (event->mask & IN_MODIFY) {
				if (event->mask & IN_ISDIR) {
					syslog(LOG_INFO, "The directory %s was modified.\n", event->name);
				} else {
					syslog(LOG_INFO, "The file %s was modified.\n", event->name);
				}
			}
		}
		i += EVENT_SIZE + event->len;
	}
}

/**************************************************************************/
int main(int argc, char **argv)
{
	int c;
	char *path;
	int daemonize;

	/*--- inotify variables */
	int fd;
	int wd;
	int rc;

	fd_set fds;

	struct timeval ttw;	/* time to wait */

	/*--- end inotify variables */


	/* Setup signal handling before we start */
	signal(SIGHUP,  signal_handler);
	signal(SIGTERM, signal_handler);
	signal(SIGINT,  signal_handler);
	signal(SIGQUIT, signal_handler);

	/* Set defaults */
	path = NULL;
#if defined(DEBUG)
	daemonize = 0;
#else
	daemonize = 1;
#endif

	/* Command line parsing */
	while((c = getopt(argc, argv, "d:nh|help")) != -1) {
		switch (c) {
		case 'd':
			path = optarg;
			break;
		case 'h':
			usage(argc, argv);
			exit(0);
			break;
		case 'n':
			daemonize = 0;
			break;
		default:
			usage(argc, argv);
			exit(0);
			break;
		}
	}

	if (!path) {
		syslog(LOG_INFO, "No path specified. Exit");
		usage(argc, argv);
		exit(-1);
	}

	/* Setup syslog logging - see setlogmask(3) */
#if defined(DEBUG)
	setlogmask(LOG_UPTO(LOG_DEBUG));
	openlog(DAEMON_NAME, LOG_CONS | LOG_NDELAY | LOG_PERROR | LOG_PID, LOG_USER);
#else
	setlogmask(LOG_UPTO(LOG_INFO));
	openlog(DAEMON_NAME, LOG_CONS, LOG_USER);
#endif

	if (daemonize) {
		syslog(LOG_DEBUG, "Daemon mode");
		demonizing_process();
	} else {
		syslog(LOG_INFO, "Monitor mode");
	}

	/*
	 * Start deamon processing code
	 */


	/*
	 * Create an instance of the inotify subsystem in the kernel and
	 * returns a file descriptor on success and -1 on failure.
	 */
	fd = inotify_init();

	if (fd < 0) {
		perror("inotify_init");
	}

	wd = inotify_add_watch(fd, path, IN_MODIFY | IN_CREATE | IN_DELETE);


	syslog(LOG_INFO, "Watch %s", path);

	while (running) {
		FD_ZERO(&fds);
		FD_SET(fd, &fds);

		ttw.tv_sec = 10;
		ttw.tv_usec = 0;

		rc = select(fd + 1, &fds, NULL, NULL, &ttw);
		if (rc < 0) {
			/* Error */
			if (rc != EINTR)
				syslog(LOG_INFO, "Select error watching %s. %s", path, strerror(errno));
		} else if (FD_ISSET(fd, &fds)) {
			/* Process the inotify events */
			my_inotify_job(fd);
		} else if (rc == 0) {
			/* Timeout */
		}
	}

	/*
	 *  Free any allocated resources before exiting
	 */
	(void) inotify_rm_watch(fd, wd);
	(void) close(fd);

	syslog(LOG_INFO, "Exit");

	exit(0);
}
