#!/bin/sh
if [ -z "$1" ]; then
	echo "$0 <filename>"
	echo "The output is <filename>_thumb.png"
	exit
fi

CONVERT="/home/roberto/RNF/ImageMagick/bin/convert"

#-- Source file without extension
SOURCE_NAME=`basename $1 | awk -F. -v OFS=. '{$NF=""; sub("[.]$", ""); print}'`


#-- Output Thumbnail
THUMB_OUT="${SOURCE_NAME}_final.png"

#-- Resized Thumbnail
THUMB_RESIZED="${SOURCE_NAME}_thumb_resized.png"
THUMB_SRC="${SOURCE_NAME}_thumb.png"

#-- Resized Thumbnail Mask
THUMB_MSK="${SOURCE_NAME}_thumb_msk.png"

#-- Resized Thumbnail Lighting Overlay
THUMB_OVR="${SOURCE_NAME}_thumb_ovr.png"

#-- Resize then crop off the parts of the image that overflows the desired size.
#$CONVERT $1 -thumbnail 100x100^ -gravity center -extent 100x100 ${THUMB_SRC}
#$CONVERT $1 -resize 100x100^ -gravity center -extent 100x100 ${THUMB_SRC}
#$CONVERT $1 -resize 48x48^ -gravity center -extent 48x48 ${THUMB_SRC}
$CONVERT $1 -resize 32x32^ -gravity center ${THUMB_RESIZED}

$CONVERT ${THUMB_RESIZED} -gravity center -extent 48x48 ${THUMB_SRC}

#-- For lets generate a rounded corners mask for our thumbnail image.
#$CONVERT ${THUMB_SRC} -alpha off -fill white -colorize 100% \
#     -draw 'fill black polygon 0,0 0,15 15,0 fill white circle 15,15 15,0' \
#     \( +clone -flip \) -compose Multiply -composite \
#     \( +clone -flop \) -compose Multiply -composite \
#     -background Gray50 -alpha Shape ${THUMB_MSK}

#-- For lets generate a rounded corners mask for our thumbnail image.
$CONVERT ${THUMB_SRC} -alpha off -fill white -colorize 100% \
     -draw 'fill black polygon 0,0 0,7 7,0 fill white circle 7,7 7,0' \
     \( +clone -flip \) -compose Multiply -composite \
     \( +clone -flop \) -compose Multiply -composite \
     -background Gray50 -alpha Shape ${THUMB_MSK}

#-- Now we have a 'shape mask' we want to use, I can apply the Aqua Effect
#-- effect to generate a lighting overlay, for this shape.

$CONVERT ${THUMB_MSK} -bordercolor None -border 1x1 \
          -alpha Extract -blur 0x10  -shade 130x30 -alpha On \
          -background gray50 -alpha background -auto-level \
          -function polynomial  3.5,-5.05,2.05,0.3 \
          \( +clone -alpha extract  -blur 0x2 \) \
          -channel RGB -compose multiply -composite \
          +channel +compose -chop 1x1 ${THUMB_OVR}

#-- With a final light/shade overlay image such as the above we can easily
#-- apply it to any thumbnail image of the right size.

$CONVERT ${THUMB_SRC} ${THUMB_OVR} \( -clone 0,1 -compose Hardlight -composite \) \
          -delete 0 -compose In -composite ${THUMB_OUT}

#-- Remove intermediate files
rm $THUMB_SRC
rm $THUMB_MSK
rm $THUMB_OVR



