TOPDIR=$(shell pwd)

TARGETS = md5 network applets utils

.SILENT:

.PHONY: $(TARGETS) clean

all: $(TARGETS)

md5:
	$(MAKE) -C src/md5 all

network:
	$(MAKE) -C src/network all

utils:
	$(MAKE) -C src/utils PIXMAP_PATH=$(TOPDIR)/pixmaps/ all

applets:
	$(MAKE) -C src/applets PIXMAP_PATH=$(TOPDIR)/pixmaps/ all

clean:
	$(MAKE) -C src/utils clean
	$(MAKE) -C src/md5 clean
	$(MAKE) -C src/network clean
	$(MAKE) -C src/applets clean
